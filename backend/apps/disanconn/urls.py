from backend.apps.disanconn import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

router = DefaultRouter()

urlpatterns = [
    url(r'', include(router.urls)), 
]

urlpatterns += format_suffix_patterns([

])
