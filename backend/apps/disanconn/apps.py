# -*- coding: utf-8 -*-
from django.apps import AppConfig

class AppsConfig(AppConfig):
    name = 'backend.apps.disanconn'
