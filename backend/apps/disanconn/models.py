# -*- coding: utf-8 -*-
from django.db import models
import datetime
from backend.library.sendmail import send_mail
import cx_Oracle
from backend.library.datemanager.models import Holiday
from django.conf import settings
import signal
from backend.library.utils import signal_handler

class DBConn(models.Model):
    owner = models.ForeignKey('license.Owner')
    name = models.CharField(max_length=60, verbose_name='Nombre')
    username = models.CharField(max_length=60, verbose_name='Usuario')
    password = models.CharField(max_length=60, verbose_name='Contraseña')
    host = models.CharField(max_length=60, verbose_name='Host')
    port = models.CharField(max_length=60, verbose_name='Puerto')
    scheme = models.CharField(max_length=60, verbose_name='Esquema')
    is_active = models.BooleanField(verbose_name='Está Activo?')
    
    def createConn(self):
        signal.signal(signal.SIGALRM, signal_handler)
        signal.alarm(30)
        conn = None
        try:
            connstr = self.username + '/' + self.password + '@' + self.host + ':' + str(self.port) + '/' + self.scheme 
            conn = cx_Oracle.connect(connstr)
        except:
            pass
        signal.alarm(0)
        return conn

    def __str__(self):
        return u"%s" % (self.name)

    def executeQuery(cursor, package_name, parameter_list, response_variables=[], debug=False):
        if debug:
            import pdb; pdb.set_trace()
        cursor_parameters = parameter_list + response_variables
        cursor.callproc(package_name, cursor_parameters )
        return cursor_parameters
    
    executeQuery = staticmethod(executeQuery)

    class Meta:
        ordering = ['name']
        app_label = 'disanconn'
        verbose_name = 'Conexión Disan'
        verbose_name_plural = 'Conexiones Disan'

class HasRigths(models.Model):
    
    def process(owner, cursor, document_type_id, document, conn_user, debug=False):
        IN_RESPONSE = cursor.var(cx_Oracle.STRING)
        if debug:
            import pdb; pdb.set_trace()
        process_response = {'PATIENT_NUMBER': None}
        package_name = 'FARMACIAEXT.ValidaAfiliacion.TieneDerechos'
        parameter_list = []
        parameter_list.append(document_type_id)
        parameter_list.append(document.ljust(16))
        response_variables = []
        response_variables.append(IN_RESPONSE)
        patient_number = None
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            data_response = HasRigths.translate(IN_RESPONSE.values[0])
            
            process_response['PROCESS'] = data_response['PROCESS']
            process_response['MESSAGE'] = data_response['MESSAGE']

            data_response = SearchPatient.process(owner, cursor, document_type_id, document, debug=debug) 
            patient_number = data_response['PATIENT_NUMBER']

            if process_response['PROCESS'] == 'FALSE':
                if data_response['PATIENT_NUMBER'] is not None and process_response['PATIENT_NUMBER'] != 0.0:
                    process_response['PATIENT_NAME'] = data_response['PATIENT_NAME']
                    try:
                        data_response = JurisdictionUser.process(owner, cursor, patient_number, conn_user, debug=debug)
                        process_response['PROCESS'] = data_response['PROCESS']
                        process_response['MESSAGE'] = data_response['MESSAGE']
                    except:
                        process_response['PROCESS'] = 'FALSE'
                        process_response['MESSAGE'] = 'Se presento un error durante la validacion'
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presento un error durante la validacion'
        process_response['PATIENT_NUMBER'] = patient_number
        return process_response
                    
    process = staticmethod(process)

    def translate(response):
        result = {}
        result['PROCESS'] = 'FALSE'
        result['MESSAGE'] = ''
        if len(response) > 0: 
            if response[0] == '1. Usuario Con Afiliacion Correcta.':
                result['PROCESS'] = 'TRUE'
            elif response[0] == '2. Tipo de Documento No Registrado en <Tipos de Documentos>.':
                result['MESSAGE'] = 'Tipo de Documento Inexistente'
            elif response[0] == '3. Usuario Con Afiliacion No Regularizada, Favor Presentarse A La Oficina Registro Y Control De Usuarios (Afiliaciones).':
                result['MESSAGE'] = 'Validar la Afiliacion'
            elif response[0] == '4. Usuario Con Plan De Atencion No Valido.':
                result['MESSAGE'] = 'Paciente fuera de la Jurisdiccion'
            else:
                result['MESSAGE'] = 'Se presento un error durante la validacion'
        else:
            result['MESSAGE'] = 'Se presento un error durante la validacion'
        
        return result
        
    translate = staticmethod(translate)

    class Meta:
        abstract = True
        app_label = 'disanconn'
    
class SearchPatient(models.Model):
    
    def process(owner, cursor, document_type_id, document, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        process_response = {}
        center_code = None
        SRV_Message = cursor.var(cx_Oracle.STRING)
        SRV_Message.setvalue(0, '1000000'.ljust(4000))
        
        In_PAC_PAC_Numero = cursor.var(cx_Oracle.NUMBER)
        In_PAC_PAC_Numero.setvalue(0, 0)

        In_PAC_CAR_NumerFicha = cursor.var(cx_Oracle.STRING)
        In_PAC_CAR_NumerFicha.setvalue(0, '')

        Out_PAC_PAC_Nombre = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_ApellPater = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_ApellMater = cursor.var(cx_Oracle.STRING)
        Out_Dvp_Pro_Glosa = cursor.var(cx_Oracle.STRING)
        Out_Dvp_Reg_Glosa = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_Fono = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_DireccionGralHabit = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_PoblaHabit = cursor.var(cx_Oracle.STRING)
        Out_xDvp_Pro_Glosa = cursor.var(cx_Oracle.STRING)
        Out_xDvp_Reg_Glosa = cursor.var(cx_Oracle.STRING)
        Out_TAB_TipoIdentCodigo = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_Rut = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_Numero = cursor.var(cx_Oracle.NUMBER)
        Out_FormaPagoCodigo = cursor.var(cx_Oracle.STRING)
        Out_FormaPagoNombre = cursor.var(cx_Oracle.STRING)
        Out_CON_CON_Codigo = cursor.var(cx_Oracle.STRING)
        Out_CON_CON_Descripcio = cursor.var(cx_Oracle.STRING)
        Out_TipoBenefCodigo = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_Cotizante = cursor.var(cx_Oracle.NUMBER)
        Out_xEstado = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_SemanCotiz = cursor.var(cx_Oracle.NUMBER)
        Out_PAC_CAR_NumerFicha = cursor.var(cx_Oracle.STRING)
        Out_CodigoCentroAten1 = cursor.var(cx_Oracle.STRING)
        Out_NombreCentroAten1 = cursor.var(cx_Oracle.STRING)

        package_name = 'ADMSALUD.ATAAGE_BUSCARPAC_PKG.ATAAGE_BuscarPac'
        parameter_list = []
        parameter_list.append(SRV_Message)
        parameter_list.append(document_type_id)
        parameter_list.append(document.ljust(13))
        parameter_list.append(In_PAC_PAC_Numero)
        parameter_list.append(In_PAC_CAR_NumerFicha)
        response_variables= []
        response_variables.append(Out_PAC_PAC_Nombre)
        response_variables.append(Out_PAC_PAC_ApellPater)
        response_variables.append(Out_PAC_PAC_ApellMater)
        response_variables.append(Out_Dvp_Pro_Glosa)
        response_variables.append(Out_Dvp_Reg_Glosa)
        response_variables.append(Out_PAC_PAC_Fono)
        response_variables.append(Out_PAC_PAC_DireccionGralHabit)
        response_variables.append(Out_PAC_PAC_PoblaHabit)
        response_variables.append(Out_xDvp_Pro_Glosa)
        response_variables.append(Out_xDvp_Reg_Glosa)
        response_variables.append(Out_TAB_TipoIdentCodigo)
        response_variables.append(Out_PAC_PAC_Rut)
        response_variables.append(Out_PAC_PAC_Numero)
        response_variables.append(Out_FormaPagoCodigo)
        response_variables.append(Out_FormaPagoNombre)
        response_variables.append(Out_CON_CON_Codigo)
        response_variables.append(Out_CON_CON_Descripcio)
        response_variables.append(Out_TipoBenefCodigo)
        response_variables.append(Out_PAC_PAC_Cotizante)
        response_variables.append(Out_xEstado)
        response_variables.append(Out_PAC_PAC_SemanCotiz)
        response_variables.append(Out_PAC_CAR_NumerFicha)
        response_variables.append(Out_CodigoCentroAten1)
        response_variables.append(Out_NombreCentroAten1)
        patient_number = 0.0
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            patient_number = Out_PAC_PAC_Numero.values[0]
            patient_name = '' 
            if Out_PAC_PAC_Nombre.values[0] is not None and Out_PAC_PAC_Nombre.values[0] != '':
                patient_name += Out_PAC_PAC_Nombre.values[0]
                patient_name += ' '
            if Out_PAC_PAC_ApellPater.values[0] is not None and Out_PAC_PAC_ApellPater.values[0] != '':
                patient_name += Out_PAC_PAC_ApellPater.values[0]
                patient_name += ' '
            if Out_PAC_PAC_ApellMater.values[0] is not None and Out_PAC_PAC_ApellMater.values[0] != '':
                patient_name += Out_PAC_PAC_ApellMater.values[0]
            if Out_CodigoCentroAten1.values[0] is not None and Out_CodigoCentroAten1.values[0] != '':
                center_code = Out_CodigoCentroAten1.values[0]
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = 'Se presento un error durante la validacion'

        if patient_number == 0.0:
            try:
                answer, patient_number, patient_name = GetPatientData.process(owner, cursor, document_type_id, document, debug=debug)
            except:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] = 'Se presento un error durante la validacion'
        if patient_number is not None and patient_number != 0.0:
            process_response['PROCESS'] = 'TRUE'
            process_response['MESSAGE'] = ''
        process_response['PATIENT_NUMBER'] = patient_number
        process_response['PATIENT_NAME'] = patient_name       
        return process_response 
              
    process = staticmethod(process)

    class Meta:
        abstract = True
        app_label = 'disanconn'

class GetPatientData(models.Model):
    
    def process(owner, cursor, document_type_id, document, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        patient_number = 0.0
        patient_name  = None
        response = None
        package_name = 'NEW_CITAS.ADMINISTRADATOSPACIENTE.TRAEDATOSPACIENTE'
        In_Answer = cursor.var(cx_Oracle.STRING)
        In_Answer.setvalue(0, ''.ljust(4000))
        
        
        Out_PAC_PAC_NUMEROW = cursor.var(cx_Oracle.NUMBER)
        Out_TAB_TIPOIDENTGLOSAW = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_RUTW_W = cursor.var(cx_Oracle.STRING)
        Out_NOMBREPACIENTEW = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_DIRECCIONGRALHABITW = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_FONOW = cursor.var(cx_Oracle.STRING)
        Out_PAC_PAC_FECHANACIMW = cursor.var(cx_Oracle.STRING) 
        
        parameter_list = []
        parameter_list.append(document_type_id)
        parameter_list.append(document.ljust(13))
        parameter_list.append(In_Answer)
        
        response_variables= []
        response_variables.append(Out_PAC_PAC_NUMEROW)
        response_variables.append(Out_TAB_TIPOIDENTGLOSAW)
        response_variables.append(Out_PAC_PAC_RUTW_W)
        response_variables.append(Out_NOMBREPACIENTEW)
        response_variables.append(Out_PAC_PAC_DIRECCIONGRALHABITW)
        response_variables.append(Out_PAC_PAC_FONOW)
        response_variables.append(Out_PAC_PAC_FECHANACIMW)

        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if Out_PAC_PAC_NUMEROW.values[0] is not None:
                patient_number = Out_PAC_PAC_NUMEROW.values[0]
            if Out_NOMBREPACIENTEW.values[0] is not None:
                patient_name = Out_NOMBREPACIENTEW.values[0]
            response = In_Answer.values[0]
        except:
            pass
        return response, patient_number, patient_name
              
    process = staticmethod(process)

    class Meta:
        abstract = True
        app_label = 'disanconn'

class JurisdictionUser(models.Model):
    
    def process(owner, cursor, patient_number, conn_user, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        process_response = {}
        IN_RESPONSE = cursor.var(cx_Oracle.STRING)
        RTAVALIDACIONW = cursor.var(cx_Oracle.NUMBER)
        RTAVALIDACIONW.setvalue(0, -1)
        IN_RESPONSE.setvalue(0, ''.ljust(4000))
            
        package_name = 'NEW_CITAS.ADMONJURISESP.VAL_USUARIO_JURISDICCION'
        parameter_list = []
        parameter_list.append(patient_number)
        parameter_list.append(conn_user)
        parameter_list.append(RTAVALIDACIONW)
        parameter_list.append(IN_RESPONSE)
        response_variables= []
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            validation = RTAVALIDACIONW.values[0]
            if validation == 0:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] = 'Paciente fuera de la Jurisdiccion'        
            else:
                try:
                    data_response = HasAbsences.process(owner, cursor, patient_number, debug=debug)
                    process_response['PROCESS'] = data_response['PROCESS']
                    process_response['MESSAGE'] = data_response['MESSAGE']
                except:
                    process_response['PROCESS'] = 'FALSE'
                    process_response['MESSAGE'] = 'Se presento un error durante la validacion'
                
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = 'Se presento un error durante la validacion'
            
        return process_response

              
    process = staticmethod(process)

    class Meta:
        abstract = True
        app_label = 'disanconn'

class HasAbsences(models.Model):
    
    def process(owner, cursor, patient_number, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        process_response = {}
        IN_RESPONSE = cursor.var(cx_Oracle.NUMBER)
        IN_RESPONSE.setvalue(0, -1)

        package_name = 'NEW_CITAS.ADMINISTRAINFCOMPARENDO.TIENEINASISTENCIAS'
        parameter_list = []
        parameter_list.append(patient_number)
        parameter_list.append(IN_RESPONSE)
        response_variables= []
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            validation = IN_RESPONSE.values[0]
            if validation == 0:
                process_response['PROCESS'] = 'TRUE'
                process_response['MESSAGE'] = ''        
            else:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] = 'Usuario Bloqueado'        
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = 'Se presento un error durante la validacion'
            
        return process_response

    process = staticmethod(process)

    class Meta:
        abstract = True
        app_label = 'disanconn'
         
class LoadSchedule(models.Model):
    
    def process(owner, cursor, speciality_id, conn_user, debug=False):
        result_quantity = 3
        if debug:
            import pdb; pdb.set_trace()
            
        #'STELLAVP'
        process_response = LoadSchedule.load_dates(owner, cursor, speciality_id, conn_user, result_quantity, debug=debug)
        return process_response
                    
    process = staticmethod(process)

    def load_dates(owner, cursor, speciality_id, conn_user, result_quantity=3, debug=False):
        from backend.apps.operative.models import SpecialityCode
        if debug:
            import pdb; pdb.set_trace()

        process_response = {}
        speciality_vector = SpecialityCode.generateVector(speciality_id)
        service_code, speciality_code, subspeciality_code = speciality_vector
        
        RESPUESTA = cursor.var(cx_Oracle.STRING)
        RESPUESTA.setvalue(0, ''.ljust(4000))

        FECHA_STR = cursor.var(cx_Oracle.STRING)
        FECHA_STR.setvalue(0, ''.ljust(4000))

        DIA_STR = cursor.var(cx_Oracle.STRING)
        DIA_STR.setvalue(0, ''.ljust(4000))

        RANGO_STR = cursor.var(cx_Oracle.STRING)
        RANGO_STR.setvalue(0, ''.ljust(4000))
        
        CENTRO_NAME_STR = cursor.var(cx_Oracle.STRING)
        CENTRO_NAME_STR.setvalue(0, ''.ljust(4000))
        
        CENTRO_STR = cursor.var(cx_Oracle.STRING)
        CENTRO_STR.setvalue(0, ''.ljust(4000))
        
        PROFESIONAL_NAME_STR = cursor.var(cx_Oracle.STRING)
        PROFESIONAL_NAME_STR.setvalue(0, ''.ljust(4000))
        
        PROFESIONAL_STR = cursor.var(cx_Oracle.STRING)
        PROFESIONAL_STR.setvalue(0, ''.ljust(4000))
        
        OBJECT_STR = cursor.var(cx_Oracle.STRING)
        OBJECT_STR.setvalue(0, ''.ljust(4000))
        
        FLD_USERCODEW = conn_user
        
        parameters_quantity = 2    

        try:
            str_query = """
DECLARE
        RESPUESTA               VARCHAR2(4000);
        SER_SER_CODIGOW         CHAR(8); 
        SER_ESP_CODIGOW         CHAR(8);
        SER_SUB_CODIGOW         CHAR(8);
        PCA_AGE_LUGARW          CHAR(8);
        PCA_AGE_CODIGPROFEW     VARCHAR2(13);
        FLD_USERCODEW           CHAR(8); 
        CANT_PARAMETROS         INTEGER;
        COL_PCA_AGE_FECHACITAC  NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_FECHACITAC_Arr;
        COL_DIA_SEMANA          NEW_CITAS.ADMONCARGAGRILLAS.DIA_SEMANA_Arr;
        COL_RANGO               NEW_CITAS.ADMONCARGAGRILLAS.RANGO_ARR;
        COL_NOMBRECENTROATEN    NEW_CITAS.ADMONCARGAGRILLAS.NOMBRECENTROATEN_Arr;
        COL_SER_OBJ_DESCRIPCIO  NEW_CITAS.ADMONCARGAGRILLAS.SER_OBJ_DESCRIPCIO_ARR;
        COL_SER_SUB_DESCRIPCIO  NEW_CITAS.ADMONCARGAGRILLAS.SER_SUB_DESCRIPCIO_Arr;
        COL_SER_SER_DESCRIPCIO  NEW_CITAS.ADMONCARGAGRILLAS.SER_SER_DESCRIPCIO_Arr;
        COL_DOCTOR              NEW_CITAS.ADMONCARGAGRILLAS.DOCTOR_Arr;
        COL_PCA_AGE_CODIGSERVI  NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_CODIGSERVI_Arr;
        COL_PCA_AGE_CODIGPROFE  NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_CODIGPROFE_Arr;
        COL_PCA_AGE_OBJETO      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_OBJETO_ARR;
        COL_PCA_AGE_LUGAR       NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_LUGAR_ARR;
        I BINARY_INTEGER := 0;
        VAULT VARCHAR2(30);
        J INTEGER;
        FECHA_STR VARCHAR2(4000);
        DIA_STR VARCHAR2(4000);
        RANGO_STR VARCHAR2(4000);
        CENTRO_STR VARCHAR2(4000);
        CENTRO_NAME_STR VARCHAR2(4000);
        PROFESIONAL_STR VARCHAR2(4000);
        PROFESIONAL_NAME_STR VARCHAR2(4000);
        OBJECT_STR VARCHAR2(4000);
        MAX_DATA INTEGER;        
BEGIN
    J := 0;
    FECHA_STR := '';
    DIA_STR := '';
    RANGO_STR := '';
    CENTRO_STR := '';
    CENTRO_NAME_STR := '';
    PROFESIONAL_STR := '';
    PROFESIONAL_NAME_STR := '';
    OBJECT_STR := '';
    MAX_DATA := 30;
    
    RESPUESTA := :P_RESPUESTA;
    SER_SER_CODIGOW := :P_SER_SER_CODIGOW;   
    SER_ESP_CODIGOW := :P_SER_ESP_CODIGOW;
    SER_SUB_CODIGOW := :P_SER_SUB_CODIGOW;
    FLD_USERCODEW :=  :P_FLD_USERCODEW;
    CANT_PARAMETROS := :P_CANT_PARAMETROS;
     
    NEW_CITAS.ADMONCARGAGRILLAS.CARGA_AGENDA_ALL(
        RESPUESTA
        , SER_SER_CODIGOW         
        , SER_ESP_CODIGOW         
        , SER_SUB_CODIGOW         
        , PCA_AGE_LUGARW          
        , PCA_AGE_CODIGPROFEW     
        , FLD_USERCODEW          
        , CANT_PARAMETROS        
        , COL_PCA_AGE_FECHACITAC 
        , COL_DIA_SEMANA         
        , COL_RANGO              
        , COL_NOMBRECENTROATEN   
        , COL_SER_OBJ_DESCRIPCIO 
        , COL_SER_SUB_DESCRIPCIO 
        , COL_SER_SER_DESCRIPCIO 
        , COL_DOCTOR             
        , COL_PCA_AGE_CODIGSERVI 
        , COL_PCA_AGE_CODIGPROFE 
        , COL_PCA_AGE_OBJETO     
        , COL_PCA_AGE_LUGAR      
    );
     :P_RESPUESTA := RESPUESTA;
     
    I := COL_PCA_AGE_FECHACITAC.FIRST();
    while I is not null and J < MAX_DATA loop
        --dbms_output.put_line('fecha.: ' || COL_PCA_AGE_FECHACITAC(i));
        FECHA_STR := FECHA_STR || ',' || COL_PCA_AGE_FECHACITAC(i);
        I := COL_PCA_AGE_FECHACITAC.next(I);
        J := J + 1;
    end loop;    
    -- dbms_output.put_line('FECHA_STR: ' || FECHA_STR);
    
    -- DIAS
    J := 0;
    I := COL_DIA_SEMANA.FIRST();
    while I is not null and J < MAX_DATA loop
        --dbms_output.put_line('vault.: ' || COL_DIA_SEMANA(I));
        DIA_STR := DIA_STR || ',' || COL_DIA_SEMANA(I);
        I := COL_DIA_SEMANA.next(I);
        J := J + 1;
    end loop;
    -- dbms_output.put_line('DIA_STR: ' || DIA_STR);
    
    -- RANGO
    J := 0;
    I := COL_RANGO.FIRST();
    while I is not null and J < MAX_DATA loop
        --dbms_output.put_line('vault.: ' || COL_DIA_SEMANA(I));
        RANGO_STR := RANGO_STR || ',' || COL_RANGO(I);
        I := COL_RANGO.next(I);
        J := J + 1;
    end loop;
    -- dbms_output.put_line('RANGO_STR: ' || RANGO_STR);

    -- NOMBRE CENTRO
    J := 0;
    I := COL_NOMBRECENTROATEN.FIRST();
    while I is not null and J < MAX_DATA loop
        CENTRO_NAME_STR := CENTRO_NAME_STR || ',' || COL_NOMBRECENTROATEN(I);
        I := COL_NOMBRECENTROATEN.next(I);
        J := J + 1;
    end loop;

    -- CENTRO
    J := 0;
    I := COL_PCA_AGE_LUGAR.FIRST();
    while I is not null and J < MAX_DATA loop
        CENTRO_STR := CENTRO_STR || ',' || COL_PCA_AGE_LUGAR(I);
        I := COL_PCA_AGE_LUGAR.next(I);
        J := J + 1;
    end loop;    
    
    -- dbms_output.put_line('CENTRO_STR: ' || CENTRO_STR);
        
    -- NOMBRE PROFESIONAL
    J := 0;
    I := COL_DOCTOR.FIRST();
    while I is not null and J < MAX_DATA loop
        PROFESIONAL_NAME_STR := PROFESIONAL_NAME_STR || ',' || COL_DOCTOR(I);
        I := COL_DOCTOR.next(I);
        J := J + 1;
    end loop;

    -- PROFESIONAL
    J := 0;
    I := COL_PCA_AGE_CODIGPROFE.FIRST();
    while I is not null and J < MAX_DATA loop
        PROFESIONAL_STR := PROFESIONAL_STR || ',' || COL_PCA_AGE_CODIGPROFE(I);
        I := COL_PCA_AGE_CODIGPROFE.next(I);
        J := J + 1;
    end loop;    
    
    -- dbms_output.put_line('PROFESIONAL_STR: ' || PROFESIONAL_STR);
        
    -- OBJECT
    J := 0;
    I := COL_PCA_AGE_OBJETO.FIRST();
    while I is not null and J < MAX_DATA loop
        OBJECT_STR := OBJECT_STR || ',' || COL_PCA_AGE_OBJETO(I);
        I := COL_PCA_AGE_OBJETO.next(I);
        J := J + 1;
    end loop;    
    
    -- dbms_output.put_line('OBJECT_STR: ' || OBJECT_STR);
        
    :P_FECHA_STR := FECHA_STR;
    :P_DIA_STR := DIA_STR;
    :P_RANGO_STR := RANGO_STR;
    :P_CENTRO_STR := CENTRO_STR;
    :P_CENTRO_NAME_STR := CENTRO_NAME_STR;
    :P_PROFESIONAL_STR := PROFESIONAL_STR;
    :P_PROFESIONAL_NAME_STR := PROFESIONAL_NAME_STR;
    :P_OBJECT_STR := OBJECT_STR;
END; 
                    """
            cursor.execute(str_query, P_RESPUESTA = RESPUESTA, P_SER_SER_CODIGOW = service_code.ljust(8),
                                           P_SER_ESP_CODIGOW = speciality_code.ljust(8), P_SER_SUB_CODIGOW = subspeciality_code.ljust(8),
                                           P_FLD_USERCODEW = FLD_USERCODEW, P_CANT_PARAMETROS =  parameters_quantity, 
                                           P_FECHA_STR = FECHA_STR,
                                           P_DIA_STR = DIA_STR,
                                           P_RANGO_STR = RANGO_STR,
                                           P_CENTRO_NAME_STR = CENTRO_NAME_STR,
                                           P_CENTRO_STR = CENTRO_STR,
                                           P_PROFESIONAL_NAME_STR = PROFESIONAL_NAME_STR,
                                           P_PROFESIONAL_STR = PROFESIONAL_STR,
                                           P_OBJECT_STR = OBJECT_STR
                                    )
            
            if RESPUESTA.values[0] == '0. No hay registros para mostrar':
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'No hay Agenda'
            else:
                center_str_list = CENTRO_STR.values[0].split(',')
                center_code_set = set(center_str_list)
                fecha_str_list = FECHA_STR.values[0].split(',')
                day_str_list = DIA_STR.values[0].split(',')
                center_name_str_list = CENTRO_NAME_STR.values[0].split(',')
                professional_str_list = PROFESIONAL_STR.values[0].split(',')
                professional_name_str_list = PROFESIONAL_NAME_STR.values[0].split(',')
                object_str_list = OBJECT_STR.values[0].split(',')
                date_result_list = []
                i = 0
                for center_code in center_code_set:
                    if center_code != '':
                        start = 0
                        date_end = False
                        date_result = {}
                        
                        while not date_end:
                            try:
                                index = center_str_list.index(center_code, start)
                            except:
                                date_end = True
                            try:
                                date_str = fecha_str_list[index].strip()
                                date = Holiday.texttodate(date_str)
                                if date is not None:
                                    date_text = Holiday.verbose_date(date, False)
                                    professional_code = professional_str_list[index].strip()
                                    date_result['DATE'] = date.strftime('%d/%m/%y')
                                    date_result['DATE_VERBOSE'] = date_text
                                    date_result['DAY_NAME'] = day_str_list[index]
                                    date_result['CENTER_CODE'] = center_code.strip()
                                    date_result['CENTER_NAME'] = center_name_str_list[index]
                                    date_result['MEDIC_CODE'] = professional_code
                                    date_result['MEDIC'] = professional_name_str_list[index]
                                    date_result['SPECIALITY_ID'] = speciality_id
                                    object_w = object_str_list[index]
                                    time_result = LoadSchedule.load_schedules(owner, cursor, service_code, professional_code, date, center_code.strip(), object_w, conn_user, debug=debug)
                                    if time_result['PROCESS'] == 'TRUE':
                                        date_end = True
                                        date_result['TIME'] = time_result['TIME'] 
                                        date_result['CORR_TIME'] = time_result['CORR_TIME'] 
                                        date_result['BLOCK_TIME'] = time_result['BLOCK_TIME']
                                        date_result['TIMESTAMP'] = time_result['TIMESTAMP']
            
                                        date_result_list.append(date_result)
                                    else:
                                        start = index
                            except:
                                pass
                        i += 1
                        if i >= result_quantity:
                            break
                process_response['OPTION_LIST'] =  date_result_list
                process_response['PROCESS'] = 'TRUE'
                process_response['MESSAGE'] =  'Hay Agenda'
                
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presento un error durante la validacion'

        return process_response
                    
    load_dates = staticmethod(load_dates)

    def load_schedules(owner, cursor, service_code, professional_code, date, center_code, object_w, conn_user, debug=False):
        if debug:
            import pdb; pdb.set_trace()
            
        process_response = {}
        process_response['PROCESS'] = 'FALSE'
        process_response['TIME'] = None
        process_response['CORR_TIME'] = None
        
        RESPUESTA = cursor.var(cx_Oracle.STRING)
        RESPUESTA.setvalue(0, ''.ljust(4000))

        HORA_STR = cursor.var(cx_Oracle.STRING)
        HORA_STR.setvalue(0, ''.ljust(4000))

        CORRELHORA_STR = cursor.var(cx_Oracle.STRING)
        CORRELHORA_STR.setvalue(0, ''.ljust(4000))

        ESTADO_STR = cursor.var(cx_Oracle.STRING)
        ESTADO_STR.setvalue(0, ''.ljust(4000))
        
        BLOQUEADO_STR = cursor.var(cx_Oracle.STRING)
        BLOQUEADO_STR .setvalue(0, ''.ljust(4000))
  
        NUMPACIENT_STR = cursor.var(cx_Oracle.STRING)
        NUMPACIENT_STR.setvalue(0, ''.ljust(4000))

        TIMEBLOQUE_STR = cursor.var(cx_Oracle.STRING)
        TIMEBLOQUE_STR.setvalue(0, ''.ljust(4000))
        
        TIMESTAMP_STR = cursor.var(cx_Oracle.STRING)
        TIMESTAMP_STR.setvalue(0, ''.ljust(4000))
        
        FLD_USERCODEW = conn_user
        
        try:
            str_query = """
DECLARE
        Respuesta                  VARCHAR2(4000);
        SER_SER_CODIGOW            CHAR(8);
        PCA_AGE_CODIGPROFEW        VARCHAR2(13);
        PCA_AGE_FECHACITACW        DATE;
        PCA_AGE_LUGARW             CHAR(8);
        PCA_AGE_OBJETOW            VARCHAR2(8);
        FLD_USERCODEW              CHAR(8);
        COL_PCA_AGE_HORACITAC      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_HORACITAC_Arr;
        COL_PCA_AGE_CODIGSERVI     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_CODIGSERVI_Arr;
        COL_PCA_AGE_CODIGPROFE     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_CODIGPROFE_Arr;
        COL_PCA_AGE_FECHACITAC     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_FECHACITAC_Arr;
        
        COL_PCA_AGE_CORRELHORA     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_CORRELHORA_Arr;  
        COL_PCA_AGE_ESTADO         NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_ESTADO_Arr;
        COL_PCA_AGE_OBSERVACIO     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_OBSERVACIO_ARR;
        COL_PCA_AGE_BLOQUEADO      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_BLOQUEADO_Arr;

        COL_PCA_AGE_TIPOFORMU      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_TIPOFORMU_Arr;
        COL_PCA_AGE_FECVENRESE     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_FECVENRESE_Arr;
        COL_PCA_AGE_SOBRECUPO      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_SOBRECUPO_Arr;
        COL_PCA_AGE_CAUSASOBRE     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_CAUSASOBRE_Arr;
        COL_PCA_AGE_FECHADIGIT     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_FECHADIGIT_Arr;
        COL_PCA_AGE_HORADIGIT      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_HORADIGIT_Arr;
        COL_PCA_AGE_TIPOPRACT      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_TIPOPRACT_Arr;
        COL_PCA_AGE_ACTIVPROFE     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_ACTIVPROFE_Arr;
        COL_PCA_AGE_NUMERPACIE     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_NUMERPACIE_Arr;
        COL_PCA_AGE_PACNUECONT     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_PACNUECONT_Arr;
        COL_PCA_AGE_VIARESER       NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_VIARESER_Arr;
        COL_PCA_AGE_TIMESTAMP      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_TIMESTAMP_Arr;
        COL_PCA_AGE_CODIGRECEP     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_CODIGRECEP_Arr;
        COL_PCA_AGE_RECEPFICHA     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_RECEPFICHA_Arr;
        COL_PCA_AGE_TIPOOBJET      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_TIPOOBJET_Arr;
        COL_NOM_FOLIO              NEW_CITAS.ADMONCARGAGRILLAS.NOM_FOLIO_Arr;
        COL_PCA_AGE_PROFDERIV      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_PROFDERIV_ARR;
        COL_PCA_AGE_ESPDERIV       NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_ESPDERIV_Arr;
        COL_PCA_AGE_FECHASOLICITUD  NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_FECHASOLICITUD_Arr;
        COL_PCA_AGE_FOLIOSOLICITUD  NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_FOLIOSOLICITUD_Arr;
        COL_PCA_AGE_PROCEDENCIA     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_PROCEDENCIA_Arr;
        COL_PCA_AGE_DETPROC         NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_DETPROC_Arr;
        COL_PCA_AGE_AMBULANCIA      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_AMBULANCIA_Arr;
        COL_PCA_AGE_RECEPCIONADO    NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_RECEPCIONADO_Arr;
        COL_PCA_AGE_TIMEBLOQUE      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_TIMEBLOQUE_Arr;
        COL_PCA_AGE_TIPOAGENDA      NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_TIPOAGENDA_Arr;
        COL_PCA_AGE_CODIGPROFEREEMP NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_CODIGPROFEREEMP_ARR;
        COL_PCA_AGE_EXAMENES        NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_EXAMENES_Arr;
        COL_PCA_AGE_PACRECITADO     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_PACRECITADO_ARR;
        COL_PCA_AGE_AREARESERVA     NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_AREARESERVA_Arr;
        COL_PCA_AGE_DESTINO         NEW_CITAS.ADMONCARGAGRILLAS.PCA_AGE_DESTINO_ARR;
        COL_MTVCORRELATIVO          NEW_CITAS.ADMONCARGAGRILLAS.MTVCORRELATIVO_Arr;
        COL_AUTCORRELATIVO          NEW_CITAS.ADMONCARGAGRILLAS.AUTCORRELATIVO_Arr;
        COL_CON_CON_CODIGO          NEW_CITAS.ADMONCARGAGRILLAS.CON_CON_CODIGO_Arr;
    
        I BINARY_INTEGER := 0;
        VAULT VARCHAR2(30);
        J INTEGER;
        MAX_DATA INTEGER;        
        
        HORA_STR VARCHAR2(4000);
        CORRELHORA_STR VARCHAR2(4000);
        ESTADO_STR VARCHAR2(4000);
        BLOQUEADO_STR VARCHAR2(4000);
        NUMPACIENT_STR VARCHAR2(4000);
        TIMEBLOQUE_STR VARCHAR2(4000);
        TIMESTAMP_STR VARCHAR2(4000);
BEGIN
    J := 0;
    HORA_STR := '';
    CORRELHORA_STR := '';
    ESTADO_STR := '';
    BLOQUEADO_STR := '';
    NUMPACIENT_STR := '';
    TIMEBLOQUE_STR := '';
    TIMESTAMP_STR := '';
    MAX_DATA := 30;

    Respuesta := :P_RESPUESTA;
    SER_SER_CODIGOW := :P_SER_SER_CODIGOW; 
    PCA_AGE_CODIGPROFEW := :P_PCA_AGE_CODIGPROFEW;
    PCA_AGE_FECHACITACW := :P_PCA_AGE_FECHACITACW;
    PCA_AGE_LUGARW :=  :P_PCA_AGE_LUGARW;
    PCA_AGE_OBJETOW := :P_PCA_AGE_OBJETOW;
    FLD_USERCODEW :=  :P_FLD_USERCODEW;
     
    NEW_CITAS.admoncargagrillas.carga_horarios(
        Respuesta,
        SER_SER_CODIGOW,
        PCA_AGE_CODIGPROFEW,
        PCA_AGE_FECHACITACW,
        PCA_AGE_LUGARW,
        PCA_AGE_OBJETOW,
        FLD_USERCODEW,
        COL_PCA_AGE_HORACITAC,
        COL_PCA_AGE_CODIGSERVI, 
        COL_PCA_AGE_CODIGPROFE,
        COL_PCA_AGE_FECHACITAC,
        COL_PCA_AGE_CORRELHORA,  
        COL_PCA_AGE_ESTADO,
        COL_PCA_AGE_OBSERVACIO,
        COL_PCA_AGE_BLOQUEADO,
        COL_PCA_AGE_TIPOFORMU,
        COL_PCA_AGE_FECVENRESE,
        COL_PCA_AGE_SOBRECUPO,
        COL_PCA_AGE_CAUSASOBRE,
        COL_PCA_AGE_FECHADIGIT,
        COL_PCA_AGE_HORADIGIT,
        COL_PCA_AGE_TIPOPRACT,
        COL_PCA_AGE_ACTIVPROFE,
        COL_PCA_AGE_NUMERPACIE,
        COL_PCA_AGE_PACNUECONT,
        COL_PCA_AGE_VIARESER,
        COL_PCA_AGE_TIMESTAMP,
        COL_PCA_AGE_CODIGRECEP,
        COL_PCA_AGE_RECEPFICHA,
        COL_PCA_AGE_TIPOOBJET,
        COL_NOM_FOLIO,
        COL_PCA_AGE_PROFDERIV,
        COL_PCA_AGE_ESPDERIV,
        COL_PCA_AGE_FECHASOLICITUD,
        COL_PCA_AGE_FOLIOSOLICITUD,
        COL_PCA_AGE_PROCEDENCIA,
        COL_PCA_AGE_DETPROC,
        COL_PCA_AGE_AMBULANCIA,
        COL_PCA_AGE_RECEPCIONADO,
        COL_PCA_AGE_TIMEBLOQUE,
        COL_PCA_AGE_TIPOAGENDA,
        COL_PCA_AGE_CODIGPROFEREEMP,
        COL_PCA_AGE_EXAMENES,
        COL_PCA_AGE_PACRECITADO,
        COL_PCA_AGE_AREARESERVA,
        COL_PCA_AGE_DESTINO,
        COL_MTVCORRELATIVO,
        COL_AUTCORRELATIVO,
        COL_CON_CON_CODIGO
    );
    
     :P_RESPUESTA := Respuesta;
     
    I := COL_PCA_AGE_HORACITAC.FIRST();
    while I is not null and J < MAX_DATA loop
        HORA_STR := HORA_STR || ',' || COL_PCA_AGE_HORACITAC(i);
        I := COL_PCA_AGE_HORACITAC.next(I);
        J := J + 1;
    end loop;    
    -- dbms_output.put_line('HORA_STR: ' || HORA_STR);     

    J := 0;
    I := COL_PCA_AGE_CORRELHORA.FIRST();
    while I is not null and J < MAX_DATA loop
        CORRELHORA_STR := CORRELHORA_STR || ',' || COL_PCA_AGE_CORRELHORA(i);
        I := COL_PCA_AGE_CORRELHORA.next(I);
        J := J + 1;
    end loop;    
    -- dbms_output.put_line('CORRELHORA_STR: ' || CORRELHORA_STR);     
     
    J := 0;
    I := COL_PCA_AGE_ESTADO.FIRST();
    while I is not null and J < MAX_DATA loop
        ESTADO_STR := ESTADO_STR || ',' || COL_PCA_AGE_ESTADO(i);
        I := COL_PCA_AGE_ESTADO.next(I);
        J := J + 1;
    end loop;    
    -- dbms_output.put_line('ESTADO_STR: ' || ESTADO_STR);     
     
    J := 0;
    I := COL_PCA_AGE_BLOQUEADO.FIRST();
    while I is not null and J < MAX_DATA loop
        BLOQUEADO_STR := BLOQUEADO_STR || ',' || COL_PCA_AGE_BLOQUEADO(i);
        I := COL_PCA_AGE_BLOQUEADO.next(I);
        J := J + 1;
    end loop;    
    -- dbms_output.put_line('BLOQUEADO_STR: ' || BLOQUEADO_STR);     
     
    J := 0;
    I := COL_PCA_AGE_NUMERPACIE.FIRST();
    while I is not null and J < MAX_DATA loop
        NUMPACIENT_STR := NUMPACIENT_STR || ',' || COL_PCA_AGE_NUMERPACIE(i);
        I := COL_PCA_AGE_NUMERPACIE.next(I);
        J := J + 1;
    end loop;    
    -- dbms_output.put_line('NUMPACIENT_STR: ' || NUMPACIENT_STR);     
     
    J := 0;
    I := COL_PCA_AGE_TIMEBLOQUE.FIRST();
    while I is not null and J < MAX_DATA loop
        TIMEBLOQUE_STR := TIMEBLOQUE_STR || ',' || COL_PCA_AGE_TIMEBLOQUE(i);
        I := COL_PCA_AGE_TIMEBLOQUE.next(I);
        J := J + 1;
    end loop;    
    -- dbms_output.put_line('TIMEBLOQUE_STR: ' || TIMEBLOQUE_STR);     
     
    J := 0;
    I := COL_PCA_AGE_TIMESTAMP.FIRST();
    while I is not null and J < MAX_DATA loop
        TIMESTAMP_STR := TIMESTAMP_STR || ',' || COL_PCA_AGE_TIMESTAMP(i);
        I := COL_PCA_AGE_TIMESTAMP.next(I);
        J := J + 1;
    end loop;    
    -- dbms_output.put_line('TIMESTAMP_STR: ' || TIMESTAMP_STR);     
     
    :P_HORA_STR := HORA_STR;
    :P_CORRELHORA_STR := CORRELHORA_STR;
    :P_ESTADO_STR := ESTADO_STR;
    :P_BLOQUEADO_STR := BLOQUEADO_STR;
    :P_NUMPACIENT_STR := NUMPACIENT_STR;
    :P_TIMEBLOQUE_STR := TIMEBLOQUE_STR;
    :P_TIMESTAMP_STR := TIMESTAMP_STR;
END; 
                    """
            format_date = settings.DISAN_FORMAT_DATE
            if owner.id == 'ertech':
                format_date = settings.DISAN_FORMAT_DATE_TEST
            
            cursor.execute(str_query, P_RESPUESTA = RESPUESTA, P_SER_SER_CODIGOW = service_code.ljust(8),
                                      P_PCA_AGE_CODIGPROFEW = professional_code.ljust(13),
                                      P_PCA_AGE_FECHACITACW = date.strftime(format_date),
                                      P_PCA_AGE_LUGARW = center_code.ljust(8), 
                                      P_FLD_USERCODEW = FLD_USERCODEW, 
                                      P_PCA_AGE_OBJETOW =  object_w, 
                                      P_HORA_STR = HORA_STR,
                                      P_CORRELHORA_STR = CORRELHORA_STR,
                                      P_ESTADO_STR = ESTADO_STR,
                                      P_BLOQUEADO_STR = BLOQUEADO_STR,
                                      P_NUMPACIENT_STR = NUMPACIENT_STR,
                                      P_TIMEBLOQUE_STR = TIMEBLOQUE_STR,
                                      P_TIMESTAMP_STR = TIMESTAMP_STR,
                                    )
         
            
            if RESPUESTA.values[0] == '0. No hay registros para mostrar':
                process_response['MESSAGE'] = 'No hay Agenda'
            else:
                hora_str_list = HORA_STR.values[0].split(',')
                correlhora_str_list = CORRELHORA_STR.values[0].split(',')
                bloqueado_str_value = BLOQUEADO_STR.values[0]
                bloqueado_str_list = [] 
                if bloqueado_str_value is not None:
                    bloqueado_str_list = bloqueado_str_value.split(',')

                numpacient_str_value = NUMPACIENT_STR.values[0]
                numpacient_str_list = [] 
                if numpacient_str_value is not None:
                    numpacient_str_list = numpacient_str_value.split(',')

                block_time_str_value = TIMEBLOQUE_STR.values[0]
                block_time_str_list = [] 
                if block_time_str_value is not None:
                    block_time_str_list = block_time_str_value.split(',')

                timestamp_str_value = TIMESTAMP_STR.values[0]
                timestamp_str_list = [] 
                if timestamp_str_value is not None:
                    timestamp_str_list = timestamp_str_value.split(',')

                i = 0
                for time_str in hora_str_list:
                    if time_str != '':
                        locked = False
                        if len(bloqueado_str_list) > i:
                            if bloqueado_str_list[i].strip() == 'S':
                                locked = True
                        pacient_number = None
                        if len(numpacient_str_list) > i:
                            pacient_number = numpacient_str_list[i].strip()
                        block_time = None
                        if len(block_time_str_list) > i:
                            block_time = block_time_str_list[i].strip()
                        timestamp = None
                        if len(timestamp_str_list) > i:
                            timestamp = timestamp_str_list[i].strip()
                        correlhora = False
                        if len(correlhora_str_list) > i:
                            correlhora = correlhora_str_list[i].strip()

                        if not locked and (pacient_number is None or pacient_number == '0'):
                            process_response['PROCESS'] = 'TRUE'
                            process_response['TIME'] = time_str
                            process_response['CORR_TIME'] = correlhora
                            process_response['BLOCK_TIME'] = block_time
                            process_response['TIMESTAMP'] = timestamp
                            break
                    i += 1
        except:
            process_response['MESSAGE'] =  'Se presento un error durante la validacion'

        return process_response
                    
    load_schedules = staticmethod(load_schedules)

    class Meta:
        abstract = True
        app_label = 'disanconn'
         
class LoadAssignedAppointments(models.Model):
    
    def process(owner, cursor, patient_number, today=None, max_appointments=5, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        
        if today is None:
            today = datetime.date.today()
        
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        
        RESPUESTA = cursor.var(cx_Oracle.STRING)
        RESPUESTA.setvalue(0, ''.ljust(4000))

        PCA_AGE_NUMERPACIEW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_NUMERPACIEW.setvalue(0, patient_number)

        FECHA_STR = cursor.var(cx_Oracle.STRING)
        FECHA_STR.setvalue(0, ''.ljust(4000))

        HORA_STR = cursor.var(cx_Oracle.STRING)
        HORA_STR.setvalue(0, ''.ljust(4000))

        CONSULTORIO_STR = cursor.var(cx_Oracle.STRING)
        CONSULTORIO_STR.setvalue(0, ''.ljust(4000))
        
        ESPECIALIDAD_STR = cursor.var(cx_Oracle.STRING)
        ESPECIALIDAD_STR.setvalue(0, ''.ljust(4000))
        
        CENTRO_STR = cursor.var(cx_Oracle.STRING)
        CENTRO_STR.setvalue(0, ''.ljust(4000))
        
        MEDICO_STR = cursor.var(cx_Oracle.STRING)
        MEDICO_STR.setvalue(0, ''.ljust(4000))
        
        SERVICIO_STR = cursor.var(cx_Oracle.STRING)
        SERVICIO_STR.setvalue(0, ''.ljust(4000))
        
        CODIGO_MEDICO_STR = cursor.var(cx_Oracle.STRING)
        CODIGO_MEDICO_STR.setvalue(0, ''.ljust(4000))
        
        HORA_CORR_STR = cursor.var(cx_Oracle.STRING)
        HORA_CORR_STR.setvalue(0, ''.ljust(4000))
        
        CODIGO_CENTRO_STR = cursor.var(cx_Oracle.STRING)
        CODIGO_CENTRO_STR.setvalue(0, ''.ljust(4000))
        
        try:
            str_query = """

DECLARE
    RESPUESTA               varchar2(4000); --IN OUT VARCHAR2,
    PCA_AGE_NUMERPACIEW     number; --IN NUMBER,
    COL_PCA_AGE_CODIGSERVI  NEW_CITAS.AdmonCargaGrillas.PCA_AGE_CODIGSERVI_Arr; --OUT PCA_AGE_CODIGSERVI_Arr, 
    COL_PCA_AGE_CODIGPROFE  NEW_CITAS.AdmonCargaGrillas.PCA_AGE_CODIGPROFE_Arr; --OUT PCA_AGE_CODIGPROFE_Arr, 
    COL_PCA_AGE_FECHACITAC  NEW_CITAS.AdmonCargaGrillas.PCA_AGE_FECHACITAC_Arr; --OUT PCA_AGE_FECHACITAC_Arr, 
    COL_PCA_AGE_HORACITAC   NEW_CITAS.AdmonCargaGrillas.PCA_AGE_HORACITAC_Arr; --OUT PCA_AGE_HORACITAC_Arr,
    COL_PCA_AGE_CORRELHORA  NEW_CITAS.AdmonCargaGrillas.PCA_AGE_CORRELHORA_Arr; --OUT PCA_AGE_CORRELHORA_Arr, 
    COL_PCA_AGE_NUMERFORMU  NEW_CITAS.AdmonCargaGrillas.PCA_AGE_NUMERFORMU_Arr; --OUT PCA_AGE_NUMERFORMU_Arr,
    COL_SER_SUB_DESCRIPCIO  NEW_CITAS.AdmonCargaGrillas.SER_SUB_DESCRIPCIO_Arr; --OUT SER_SUB_DESCRIPCIO_Arr,
    COL_SER_SER_DESCRIPCIO  NEW_CITAS.AdmonCargaGrillas.SER_SER_DESCRIPCIO_Arr;--OUT SER_SER_DESCRIPCIO_Arr,
    COL_NOMBRECENTROATEN    NEW_CITAS.AdmonCargaGrillas.NOMBRECENTROATEN_Arr; --OUT NOMBRECENTROATEN_Arr,
    COL_SER_OBJ_DESCRIPCIO  NEW_CITAS.AdmonCargaGrillas.SER_OBJ_DESCRIPCIO_Arr; --OUT SER_OBJ_DESCRIPCIO_Arr,
    COL_MEDICO              NEW_CITAS.AdmonCargaGrillas.MEDICO_Arr; --OUT MEDICO_Arr,
    COL_PCA_AGE_LUGAR       NEW_CITAS.AdmonCargaGrillas.PCA_AGE_LUGAR_Arr; --OUT PCA_AGE_LUGAR_Arr
    
    -- serialization
    i BINARY_INTEGER;
    j INTEGER;
    FECHA_STR VARCHAR2(4000);
    HORA_STR VARCHAR2(4000);    
    CONSULTORIO_STR VARCHAR2(4000);    
    ESPECIALIDAD_STR VARCHAR2(4000);
    CENTRO_STR VARCHAR2(4000);        
    MEDICO_STR VARCHAR2(4000);
    SERVICIO_STR VARCHAR2(4000);    
    CODIGO_MEDICO_STR VARCHAR2(4000);
    HORA_CORR_STR VARCHAR2(4000);        
    CODIGO_CENTRO_STR VARCHAR2(4000);

    MAX_DATA INTEGER;        
    
BEGIN
    RESPUESTA := '';
    PCA_AGE_NUMERPACIEW := :P_PCA_AGE_NUMERPACIEW;

    i := 0;
    j := 0;
    FECHA_STR := '';
    HORA_STR := '';    
    CONSULTORIO_STR := '';    
    ESPECIALIDAD_STR := '';
    CENTRO_STR := '';    
    MEDICO_STR := '';
    SERVICIO_STR := '';    
    CODIGO_MEDICO_STR := '';
    HORA_CORR_STR := '';    
    CODIGO_CENTRO_STR := '';
    MAX_DATA := 30;
    
    NEW_CITAS.AdmonCargaGrillas.CARGA_CITAS_ASIGNADAS (
        RESPUESTA,
        PCA_AGE_NUMERPACIEW     ,
        COL_PCA_AGE_CODIGSERVI  ,
        COL_PCA_AGE_CODIGPROFE  ,
        COL_PCA_AGE_FECHACITAC  ,
        COL_PCA_AGE_HORACITAC   ,
        COL_PCA_AGE_CORRELHORA  ,
        COL_PCA_AGE_NUMERFORMU  ,
        COL_SER_SUB_DESCRIPCIO  ,
        COL_SER_SER_DESCRIPCIO  ,
        COL_NOMBRECENTROATEN    ,
        COL_SER_OBJ_DESCRIPCIO  ,
        COL_MEDICO              ,
        COL_PCA_AGE_LUGAR       
    );
    dbms_output.put_line('Respuesta: ' || Respuesta);
    
    -- FECHA
    i := COL_PCA_AGE_FECHACITAC.FIRST();
    while i is not null and j < MAX_DATA loop
        FECHA_STR := FECHA_STR  || COL_PCA_AGE_FECHACITAC(i) || ',';
        i := COL_PCA_AGE_FECHACITAC.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('FECHA_STR: ' || FECHA_STR);

    -- HORA
    i := COL_PCA_AGE_HORACITAC.FIRST();
    j := 0;
    while i is not null and j < MAX_DATA loop
        HORA_STR := HORA_STR  || COL_PCA_AGE_HORACITAC(i) || ',';
        i := COL_PCA_AGE_HORACITAC.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('HORA_STR: ' || HORA_STR);

    -- ESPECIALIDAD
    i := COL_SER_SUB_DESCRIPCIO.FIRST();
    j := 0;
    while i is not null and j < MAX_DATA loop
        ESPECIALIDAD_STR := ESPECIALIDAD_STR  || COL_SER_SUB_DESCRIPCIO(i) || ',';
        i := COL_SER_SUB_DESCRIPCIO.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('ESPECIALIDAD_STR: ' || ESPECIALIDAD_STR);    
    
    -- CENTRO_STR
    i := COL_NOMBRECENTROATEN.FIRST();
    j := 0;
    while i is not null and j < MAX_DATA loop
        CENTRO_STR := CENTRO_STR  || COL_NOMBRECENTROATEN(i) || ',';
        i := COL_NOMBRECENTROATEN.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('CENTRO_STR: ' || CENTRO_STR);    
    
    -- MEDICO_STR
    i := COL_MEDICO.FIRST();
    j := 0;
    while i is not null and j < MAX_DATA loop
        MEDICO_STR := MEDICO_STR  || COL_MEDICO(i) || ',';
        i := COL_MEDICO.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('MEDICO_STR: ' || MEDICO_STR);    
    
    -- CONSULTORIO_STR
    i := COL_SER_OBJ_DESCRIPCIO.FIRST();
    j := 0;
    while i is not null and j < MAX_DATA loop
        CONSULTORIO_STR := CONSULTORIO_STR  || COL_SER_OBJ_DESCRIPCIO(i) || ',';
        i := COL_SER_OBJ_DESCRIPCIO.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('CONSULTORIO_STR: ' || CONSULTORIO_STR);    
    
    -- SERVICIO_STR
    i := COL_PCA_AGE_CODIGSERVI.FIRST();
    j := 0;
    while i is not null and j < MAX_DATA loop
        SERVICIO_STR := SERVICIO_STR  || COL_PCA_AGE_CODIGSERVI(i) || ',';
        i := COL_PCA_AGE_CODIGSERVI.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('SERVICIO_STR: ' || SERVICIO_STR);    
    
    -- CODIGO_MEDICO_STR
    i := COL_PCA_AGE_CODIGPROFE.FIRST();
    j := 0;
    while i is not null and j < MAX_DATA loop
        CODIGO_MEDICO_STR := CODIGO_MEDICO_STR  || COL_PCA_AGE_CODIGPROFE(i) || ',';
        i := COL_PCA_AGE_CODIGPROFE.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('CODIGO_MEDICO_STR: ' || CODIGO_MEDICO_STR);    
    
    -- HORA_CORR_STR
    i := COL_PCA_AGE_CORRELHORA.FIRST();
    j := 0;
    while i is not null and j < MAX_DATA loop
        HORA_CORR_STR := HORA_CORR_STR  || COL_PCA_AGE_CORRELHORA(i) || ',';
        i := COL_PCA_AGE_CORRELHORA.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('HORA_CORR_STR: ' || HORA_CORR_STR);    
    
    -- CODIGO_CENTRO_STR
    i := COL_PCA_AGE_LUGAR.FIRST();
    j := 0;
    while i is not null and j < MAX_DATA loop
        CODIGO_CENTRO_STR := CODIGO_CENTRO_STR  || COL_PCA_AGE_LUGAR(i) || ',';
        i := COL_PCA_AGE_LUGAR.next(i);
        j := j + 1;
    end loop;    
    -- dbms_output.put_line('CODIGO_CENTRO_STR: ' || CODIGO_CENTRO_STR);    
    
    :P_FECHA_STR := FECHA_STR;
    :P_HORA_STR := HORA_STR;
    :P_ESPECIALIDAD_STR := ESPECIALIDAD_STR;
    :P_CONSULTORIO_STR := CONSULTORIO_STR;
    :P_RESPUESTA := RESPUESTA;
    :P_CENTRO_STR := CENTRO_STR;
    :P_MEDICO_STR := MEDICO_STR;
    :P_SERVICIO_STR := SERVICIO_STR;
    :P_CODIGO_MEDICO_STR := CODIGO_MEDICO_STR;
    :P_HORA_CORR_STR := HORA_CORR_STR;
    :P_CODIGO_CENTRO_STR := CODIGO_CENTRO_STR;
END;
                    """
            cursor.execute(str_query, P_RESPUESTA = RESPUESTA, P_PCA_AGE_NUMERPACIEW = PCA_AGE_NUMERPACIEW,
                                           P_FECHA_STR = FECHA_STR,
                                           P_HORA_STR = HORA_STR,
                                           P_ESPECIALIDAD_STR = ESPECIALIDAD_STR,
                                           P_CONSULTORIO_STR = CONSULTORIO_STR,
                                           P_CENTRO_STR = CENTRO_STR,
                                           P_MEDICO_STR = MEDICO_STR,
                                           P_SERVICIO_STR = SERVICIO_STR,
                                           P_CODIGO_MEDICO_STR = CODIGO_MEDICO_STR,
                                           P_HORA_CORR_STR = HORA_CORR_STR,
                                           P_CODIGO_CENTRO_STR = CODIGO_CENTRO_STR
                                    )
            
            if RESPUESTA.values[0] == '0. No hay registros para mostrar':
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'No hay Citas'
            else:
                fecha_str_list = FECHA_STR.values[0].split(',')
                time_str_list = HORA_STR.values[0].split(',')
                speciality_str_list = ESPECIALIDAD_STR.values[0].split(',')
                consultory_str_list = CONSULTORIO_STR.values[0].split(',')
                center_str_list = CENTRO_STR.values[0].split(',')
                medic_str_list = MEDICO_STR.values[0].split(',')
                service_code_str_list = SERVICIO_STR.values[0].split(',')
                medic_code_str_list = CODIGO_MEDICO_STR.values[0].split(',')
                corr_time_str_list = HORA_CORR_STR.values[0].split(',')
                center_code_str_list = CODIGO_CENTRO_STR.values[0].split(',')
                
                date_list = []
                date_result_list = []
                i = 0
                for date_str in fecha_str_list:
                    date_result = {}
                    quantity = 0
                    try:
                        date = Holiday.texttodate(date_str)
                        if date is not None and date >= today:
                            date_text = Holiday.verbose_date(date, False)
                            if date_str not in date_list:
                                date_list.append(date_str)
                                date_result['INDEX'] = i + 1
                                date_result['DATE'] = date.strftime('%d/%m/%y')
                                date_result['DATE_VERBOSE'] = date_text
                                date_result['TIME'] = time_str_list[i]
                                date_result['SPECIALITY'] = speciality_str_list[i].strip()
                                date_result['MEDIC'] = medic_str_list[i]
                                date_result['CONSULTING_ROOM'] = consultory_str_list[i]
                                date_result['CENTER_NAME'] = center_str_list[i]
                                date_result['SERVICE_CODE'] = service_code_str_list[i].strip()
                                date_result['MEDIC_CODE'] = medic_code_str_list[i].strip()
                                date_result['COR_TIME'] = corr_time_str_list[i]
                                date_result['CENTER_CODE'] = center_code_str_list[i].strip()
                                date_result_list.append(date_result)
                                quantity += 1
                    except:
                        pass
                    i += 1
                    if quantity > max_appointments:
                        break
                if len(date_result_list) > 0: 
                    process_response['APPOINTMENT_LIST'] =  date_result_list
                    process_response['QUANTITY'] = len(date_result_list) 
                    process_response['PROCESS'] = 'TRUE'
                    process_response['MESSAGE'] =  'Hay Citas'
                else:
                    process_response['QUANTITY'] = 0
                    process_response['PROCESS'] = 'FALSE'
                    process_response['MESSAGE'] =  'No hay Citas'

        except:
            process_response['QUANTITY'] = 0
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presento un error durante la validacion'

        return process_response
                    
    process = staticmethod(process)

    class Meta:
        abstract = True
        app_label = 'disanconn'
    
class CancelAppointment(models.Model):

    def register_auditor(owner, cursor, patient_number, service_code, medic_code, appointment_date_time, corr_time, conn_user, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        
        process_response = {}
        table_name = 'ADMSALUD.TAB_AUDITORIACITAS'
        format_date = settings.DISAN_FORMAT_DATE
        if owner.id == 'ertech':
            format_date = settings.DISAN_FORMAT_DATE_TEST

        try:
            str_query = "INSERT INTO "
            str_query += table_name
            str_query += "(PCA_AGE_CODIGSERVI, PCA_AGE_CODIGPROFE, PCA_AGE_FECHACITAC, PCA_AGE_HORACITAC, PCA_AGE_CORRELHORA, FECHAHORALLAMADA, USUARIOREGIATRA, ACCION, PCA_AGE_NUMERPACIE) "
            str_query += "VALUES ("
            str_query += "'" + service_code.ljust(8) + "',"
            str_query += "'" + medic_code.ljust(13) + "',"
            str_query += "'" + appointment_date_time.strftime(format_date) + "',"
            str_query += "'" + appointment_date_time.strftime("%H:%M") + "',"
            str_query += str(corr_time) + ","
            str_query += "SYSDATE,"
            str_query += "'" + conn_user + "',"
            str_query += "'Cancelar Cita',"
            str_query += str(patient_number)
            str_query += ")"
            if debug:
                print (str_query)
            cursor.execute(str_query)
            process_response['PROCESS'] = 'TRUE'
            process_response['MESSAGE'] =  ''

        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presento un error durante la validacion'

        return process_response
                    
    register_auditor = staticmethod(register_auditor)

    def process(owner, cursor, patient_number, service_code, medic_code, appointment_date_time, corr_time, center_code, cancel_cause, conn_user, debug=False):
        if debug:
            import pdb; pdb.set_trace()

        package_name = 'admsalud.ataage_grabarelim_pkg.ATAAGE_GrabarElim'
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        format_date = settings.DISAN_FORMAT_DATE
        if owner.id == 'ertech':
            format_date = settings.DISAN_FORMAT_DATE_TEST
        
        SRV_Message = cursor.var(cx_Oracle.STRING)
        SRV_Message.setvalue(0, ''.ljust(4000))
        
        In_PAC_PAC_Numero = cursor.var(cx_Oracle.NUMBER)
        In_PAC_PAC_Numero.setvalue(0, patient_number)
        
        In_PCA_AGE_CodigServi = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_CodigServi.setvalue(0, service_code.ljust(8))
        
        In_PCA_AGE_CodigProfe = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_CodigProfe.setvalue(0, medic_code.ljust(13))
        
        In_PCA_AGE_FechaCitac = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_FechaCitac.setvalue(0, appointment_date_time.strftime(format_date))
        
        In_PCA_AGE_HoraCitac = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_HoraCitac.setvalue(0, appointment_date_time.strftime('%H:%M'))
        
        In_PCA_AGE_CorrelHora = cursor.var(cx_Oracle.NUMBER)
        In_PCA_AGE_CorrelHora.setvalue(0, corr_time)
        
        In_CausaElimCodigo = cursor.var(cx_Oracle.STRING)
        In_CausaElimCodigo.setvalue(0, cancel_cause.ljust(8))
        
        In_PCA_HCA_CodigUsuar = cursor.var(cx_Oracle.STRING)
        In_PCA_HCA_CodigUsuar.setvalue(0, conn_user.ljust(8))
        
        In_PCA_AGE_Lugar = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_Lugar.setvalue(0, center_code.ljust(8))
        
        In_PCA_HCA_TipoRegistro = cursor.var(cx_Oracle.STRING)
        In_PCA_HCA_TipoRegistro.setvalue(0, '')
        
        In_PCA_AGE_Objeto = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_Objeto.setvalue(0, '')

        Out_AUX_AvisoExistePago = cursor.var(cx_Oracle.STRING)
        Out_AUX_AvisoExistePago.setvalue(0, '')
        
        Out_RAF_PAG_NumerFormu = cursor.var(cx_Oracle.STRING)
        Out_RAF_PAG_NumerFormu.setvalue(0, ''.ljust(10))
        
        Out_TipoFormuCodigo = cursor.var(cx_Oracle.STRING)
        Out_TipoFormuCodigo.setvalue(0, ''.ljust(4))
        
        parameter_list = []
        parameter_list.append(SRV_Message)
        parameter_list.append(In_PAC_PAC_Numero)
        parameter_list.append(In_PCA_AGE_CodigServi)
        parameter_list.append(In_PCA_AGE_CodigProfe)
        parameter_list.append(In_PCA_AGE_FechaCitac)
        parameter_list.append(In_PCA_AGE_HoraCitac)
        parameter_list.append(In_PCA_AGE_CorrelHora)
        parameter_list.append(In_CausaElimCodigo)
        parameter_list.append(In_PCA_HCA_CodigUsuar)
        parameter_list.append(In_PCA_AGE_Lugar)
        parameter_list.append(In_PCA_HCA_TipoRegistro)
        parameter_list.append(In_PCA_AGE_Objeto)
        
        response_variables= []
        response_variables.append(Out_AUX_AvisoExistePago)
        response_variables.append(Out_RAF_PAG_NumerFormu)
        response_variables.append(Out_TipoFormuCodigo)

        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if SRV_Message.values[0] != '1000000':
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'La Cancelación de la cita no autorizada'
            else:
                method_response = CancelAppointment.register_auditor(owner, cursor, patient_number, service_code, medic_code, appointment_date_time, corr_time, conn_user, debug=debug)
                if method_response['PROCESS'] == 'TRUE':
                    process_response['PROCESS'] = 'TRUE'
                    process_response['MESSAGE'] =  'Cancelación de la Cita fue realizada satisfactoreamente'
                else:
                    process_response['PROCESS'] = 'TRUE'
                    process_response['MESSAGE'] =  'Cancelación de la Cita fue realizada satisfactoreamente, pero no se pudo registrar en la auditoria'
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
        
        return process_response
              
    process = staticmethod(process)

    class Meta:
        abstract = True
        app_label = 'disanconn'

class LoadCancelCause(models.Model):
    
    def process(owner, cursor, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        
        process_response = {}
        process_response['CAUSE_LIST'] = []
        statement = 'SELECT * FROM tab_causaelim'
        cursor.execute(statement)
        cause_list = cursor.fetchall()
        for cause in cause_list:
            try:
                cause_dict = {'ID': cause[0].strip(),
                              'NAME': cause[1]
                    }
                process_response['CAUSE_LIST'].append(cause_dict)
            except:
                pass
            
        process_response['PROCESS'] = 'TRUE'
        process_response['MESSAGE'] = ''
        return process_response
                    
    process = staticmethod(process)

    class Meta:
        abstract = True
        app_label = 'disanconn'
    
class ConfirmAppointment(models.Model):

    def process(owner, cursor, patient_number, service_code, medic_code, appointment_date_time, corr_time, debug=False):
        if debug:
            import pdb; pdb.set_trace()

        package_name = 'NEW_CITAS.ADMINISTRACITA.CONFIRMACITAASIGNADA'
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        format_date = settings.DISAN_FORMAT_DATE
        if owner.id == 'ertech':
            format_date = settings.DISAN_FORMAT_DATE_TEST
        
        PCA_AGE_CODIGSERVIW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_CODIGSERVIW.setvalue(0, service_code.ljust(8))

        PCA_AGE_CODIGPROFEW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_CODIGPROFEW.setvalue(0, medic_code.ljust(13))
        
        PCA_AGE_FECHACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_FECHACITACW.setvalue(0, appointment_date_time.strftime(format_date))
        
        PCA_AGE_HORACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_HORACITACW.setvalue(0, appointment_date_time.strftime('%H:%M'))
        
        PCA_AGE_CORRELHORAW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_CORRELHORAW.setvalue(0, corr_time)
        
        PCA_AGE_NUMERPACIEW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_NUMERPACIEW.setvalue(0, patient_number)
        
        FECHACITA = cursor.var(cx_Oracle.STRING)
        FECHACITA.setvalue(0, ''.ljust(128))
        
        HORACITA = cursor.var(cx_Oracle.STRING)
        HORACITA.setvalue(0, ''.ljust(5))
        
        CENTROMEDICO = cursor.var(cx_Oracle.STRING)
        CENTROMEDICO.setvalue(0, ''.ljust(60))
        
        CONSULTORIO = cursor.var(cx_Oracle.STRING)
        CONSULTORIO.setvalue(0, ''.ljust(40))
        
        ESPECIALIDAD = cursor.var(cx_Oracle.STRING)
        ESPECIALIDAD.setvalue(0, ''.ljust(40))
        
        DOCTOR = cursor.var(cx_Oracle.STRING)
        DOCTOR.setvalue(0, ''.ljust(90))
        
        Respuesta = cursor.var(cx_Oracle.STRING)
        Respuesta.setvalue(0, ''.ljust(4000))
        
        parameter_list = []
        parameter_list.append(PCA_AGE_CODIGSERVIW)
        parameter_list.append(PCA_AGE_CODIGPROFEW)
        parameter_list.append(PCA_AGE_FECHACITACW)
        parameter_list.append(PCA_AGE_HORACITACW)
        parameter_list.append(PCA_AGE_CORRELHORAW)
        parameter_list.append(PCA_AGE_NUMERPACIEW)

        response_variables= []

        response_variables.append(FECHACITA)
        response_variables.append(HORACITA)
        response_variables.append(CENTROMEDICO)
        response_variables.append(CONSULTORIO)
        response_variables.append(ESPECIALIDAD)
        response_variables.append(DOCTOR)
        response_variables.append(Respuesta)        
      
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if Respuesta.values[0] == '1. Cita Asignada de Forma Exitosa.':
                process_response['PROCESS'] = 'TRUE'
                process_response['MESSAGE'] =  'Cita Asignada de Forma Exitosa'
                process_response['DATE'] = FECHACITA.values[0]
                process_response['TIME'] = HORACITA.values[0]
                process_response['CENTER'] = CENTROMEDICO.values[0]
                process_response['CONSULTORIY'] = CONSULTORIO.values[0]
                process_response['SPECIALITY'] = ESPECIALIDAD.values[0]
                process_response['DOCTOR'] = DOCTOR.values[0]
            else:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'La Cita No Pudo Ser Asignada'
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
        
        return process_response
              
    process = staticmethod(process)

    class Meta:
        abstract = True
        app_label = 'disanconn'


class AssignAppointment(models.Model):

    def process(owner, cursor, patient_number, speciality_id, medic_code, center_code, appointment_date_time, corr_time, block_time, timestamp, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        appointment_date = appointment_date_time.date()
        try:
            validate_response = AssignAppointment.valide_month(owner, cursor, patient_number, appointment_date, debug=debug)
            if validate_response['PROCESS'] == 'FALSE':
                process_response['PROCESS'] = validate_response['PROCESS']
                process_response['MESSAGE'] = validate_response['MESSAGE']
                return process_response
 
            validate_response = AssignAppointment.valide_same_specialy_month(owner, cursor, patient_number, appointment_date, speciality_id, debug=debug)
            if validate_response['PROCESS'] == 'FALSE':
                process_response['PROCESS'] = validate_response['PROCESS']
                process_response['MESSAGE'] = validate_response['MESSAGE']
                return process_response
 
            validate_response = AssignAppointment.valide_day(owner, cursor, patient_number, appointment_date, center_code, debug=debug)
            if validate_response['PROCESS'] == 'FALSE':
                process_response['PROCESS'] = validate_response['PROCESS']
                process_response['MESSAGE'] = validate_response['MESSAGE']
                return process_response
  
            validate_response = AssignAppointment.valide_day_diferent_center(owner, cursor, patient_number, appointment_date, debug=debug)
            if validate_response['PROCESS'] == 'FALSE':
                process_response['PROCESS'] = validate_response['PROCESS']
                process_response['MESSAGE'] = validate_response['MESSAGE']
                return process_response
 
            validate_response = AssignAppointment.valide_min_minutes(owner, cursor, patient_number, appointment_date_time, center_code, debug=debug)
            if validate_response['PROCESS'] == 'FALSE':
                process_response['PROCESS'] = validate_response['PROCESS']
                process_response['MESSAGE'] = validate_response['MESSAGE']
                return process_response

            validate_response = AssignAppointment.verify_time_appointment(owner, cursor, appointment_date_time, speciality_id, medic_code, center_code, corr_time, block_time, timestamp, debug=debug)
            if validate_response['PROCESS'] == 'FALSE':
                process_response['PROCESS'] = validate_response['PROCESS']
                process_response['MESSAGE'] = validate_response['MESSAGE']
                return process_response

            confirm_response = AssignAppointment.confirm(owner, cursor, patient_number, speciality_id, medic_code, appointment_date_time, corr_time, debug=debug)
            for key in confirm_response.keys():
                process_response[key] = confirm_response[key]

        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presento un error durante la validacion'
        return process_response

    process = staticmethod(process)

    def verify_time_appointment(owner, cursor, appointment_date_time, speciality_id, medic_code, center_code, corr_time, block_time, timestamp, debug=False):
        from backend.apps.operative.models import SpecialityCode
        if debug:
            import pdb; pdb.set_trace()

        speciality_vector = SpecialityCode.generateVector(speciality_id)
        service_code, speciality_code, subspeciality_code = speciality_vector
        
        package_name = 'admsalud.ataage_verfhoraasignar_pkg.ataage_verfhoraasignar'
        process_response = {}
        process_response ['APPOINTMENT_DATE_TIME'] = appointment_date_time
        format_date = settings.DISAN_FORMAT_DATE
        if owner.id == 'ertech':
            format_date = settings.DISAN_FORMAT_DATE_TEST
        
        SRV_Message = cursor.var(cx_Oracle.STRING)
        SRV_Message.setvalue(0, ''.ljust(4000))

        In_PCA_AGE_CodigServi = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_CodigServi.setvalue(0, service_code.ljust(8))

        In_PCA_AGE_CodigProfe = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_CodigProfe.setvalue(0, medic_code.ljust(13))

        In_PCA_AGE_FechaCitac = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_FechaCitac.setvalue(0, appointment_date_time.strftime(format_date))

        In_PCA_AGE_Lugar = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_Lugar.setvalue(0, center_code.ljust(8))

        In_AUX_TiemposPrest = cursor.var(cx_Oracle.NUMBER)
        In_AUX_TiemposPrest.setvalue(0, block_time)
        
        In_PCA_AGE_HoraCitac = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_HoraCitac.setvalue(0, appointment_date_time.strftime('%H:%M').ljust(5))

        In_PCA_AGE_CorrelHora = cursor.var(cx_Oracle.NUMBER)
        In_PCA_AGE_CorrelHora.setvalue(0, corr_time)
        
        In_PCA_AGE_TimeStamp = cursor.var(cx_Oracle.STRING)
        In_PCA_AGE_TimeStamp.setvalue(0, str(timestamp).ljust(8))
        
        Out_AUX_OkHora = cursor.var(cx_Oracle.STRING)
        Out_AUX_OkHora.setvalue(0, ' ')
        
        parameter_list = []
        parameter_list.append(SRV_Message)
        parameter_list.append(In_PCA_AGE_CodigServi)
        parameter_list.append(In_PCA_AGE_CodigProfe)
        parameter_list.append(In_PCA_AGE_FechaCitac)
        parameter_list.append(In_PCA_AGE_Lugar)
        parameter_list.append(In_AUX_TiemposPrest)
        parameter_list.append(In_PCA_AGE_HoraCitac)
        parameter_list.append(In_PCA_AGE_CorrelHora)
        parameter_list.append(In_PCA_AGE_TimeStamp)

        response_variables= []

        response_variables.append(Out_AUX_OkHora)
      
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if SRV_Message.values[0][:7] == '1000000':
                process_response['PROCESS'] = 'TRUE'
            elif SRV_Message.values[0][:7] == '0780000':
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Esta Hora ya fue asignada por otro usuario a otro paciente.'
            else:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
                
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
        
        return process_response
        #0780000!Esta Hora ya fue asignada por otro usuario a otro paciente. Debe cargar nuevamente la informacion de citas disponibles.!
        
    verify_time_appointment = staticmethod(verify_time_appointment)

    """
    
o en la cita corresponde  o es suficiente para l prestacion de acuerdo al t 

SRV_Message:
1000000
0780000!Esta Hora ya fue asignada por otro usuario a otro paciente. Debe cargar nuevamente la informacion de citas disponibles.!


Out_AUX_OkHora
''  (ok)
'1' (tipo de agenda 'D' o 'E')
'2'
'3'
'4' (sin tipo de agenda)
*/

DECLARE
      SRV_Message varchar2(4000); --                    In Out     Varchar2                        /*  Parametro de uso interno  */
      In_PCA_AGE_CodigServi  varchar2(8); --          In         Varchar2                        /*  Codigo del servicio  */
      In_PCA_AGE_CodigProfe  varchar2(13); --         In         Varchar2                        /*  Codigo del profesional  */
      In_PCA_AGE_FechaCitac   varchar2(8); --        In         Varchar2                        /*  Fecha de Citacion  */
      In_PCA_AGE_Lugar  varchar2(8); --              In         Varchar2                        /*  Codigo del Lugar  */
      In_AUX_TiemposPrest  number(9); --           In         Number                          /*  Field que registra la sumatoria de los tiempos de cada prestacion asociada a la hora por el usuario  */
      In_PCA_AGE_HoraCitac  varchar2(5); --          In Out     Varchar2                        /*  Hora de Citacion  */
      In_PCA_AGE_CorrelHora  number(9); --         In Out     Number                          /*  Correlativo de la hora  */
      In_PCA_AGE_TimeStamp   varchar2(8); --         In Out     Varchar2                        /*  Campo control del registro  */
      Out_AUX_OkHora   varchar2(1); --              Out        Varchar2                        /*  Retorna al cliente el tipo de bloque hora a signara encontrada en agenda  */
BEGIN
      SRV_Message := '';
      
      In_PCA_AGE_CodigServi := 'PSC001  '; --varchar2(8); --  'PSC001  ', 'PSC007  ', 'CMG_AS5 '        In         Varchar2                        /*  Codigo del servicio  */
      In_PCA_AGE_CodigProfe := '43927309     '; -- varchar2(13); -- '43927309     ', '79951743     '    In         Varchar2                        /*  Codigo del profesional  */
      In_PCA_AGE_FechaCitac := '30/04/14'; -- varchar2(); --  '22/11/13'      In         Varchar2                        /*  Fecha de Citacion  */
      In_PCA_AGE_Lugar := '163     '; -- varchar2(8); --  '163     ', '077     '            In         Varchar2                        /*  Codigo del Lugar  */
      In_AUX_TiemposPrest := 0; -- number(9); -- 5, 10, 20,30          In         Number                          /*  Field que registra la sumatoria de los tiempos de cada prestacion asociada a la hora por el usuario  */
      In_PCA_AGE_HoraCitac := '09:00'; -- varchar2(5); --          In Out     Varchar2                        /*  Hora de Citacion  */
      In_PCA_AGE_CorrelHora := 4; -- number(9); --         In Out     Number                          /*  Correlativo de la hora  */
      In_PCA_AGE_TimeStamp := '0'; --  varchar2(8); -- 0,1,3,5,7         In Out     Varchar2                        /*  Campo control del registro  */
      Out_AUX_OkHora := ''; -- varchar2(1); --              Out        Varchar2                        /*  Retorna al cliente el tipo de bloque hora a signara encontrada en agenda  */
      
    admsalud.ataage_verfhoraasignar_pkg.ataage_verfhoraasignar (
      SRV_Message,
      In_PCA_AGE_CodigServi,
      In_PCA_AGE_CodigProfe,
      In_PCA_AGE_FechaCitac,
      In_PCA_AGE_Lugar,
      In_AUX_TiemposPrest,
      In_PCA_AGE_HoraCitac,
      In_PCA_AGE_CorrelHora,
      In_PCA_AGE_TimeStamp,
      Out_AUX_OkHora
    );
    
    dbms_output.put_line('SRV_Message: ' || SRV_Message);
    dbms_output.put_line('Out_AUX_OkHora: ' || Out_AUX_OkHora);
END;


    """    
    
    def valide_month(owner, cursor, patient_number, appointment_date, debug=False):
        if debug:
            import pdb; pdb.set_trace()

        package_name = 'new_citas.administraparametros.valid_cant_citas_mes'
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        process_response ['APPOINTMENT_DATE'] = appointment_date
        
        PCA_AGE_NUMERPACIEW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_NUMERPACIEW.setvalue(0, patient_number)
        
        PCA_AGE_FECHACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_FECHACITACW.setvalue(0, appointment_date.strftime('%Y/%m/%d').ljust(10))

        RTAVALIDACIONW = cursor.var(cx_Oracle.NUMBER)
        RTAVALIDACIONW.setvalue(0, -1)
        
        Respuesta = cursor.var(cx_Oracle.STRING)
        Respuesta.setvalue(0, ''.ljust(4000))
        
        parameter_list = []
        parameter_list.append(PCA_AGE_NUMERPACIEW)
        parameter_list.append(PCA_AGE_FECHACITACW)

        response_variables= []

        response_variables.append(RTAVALIDACIONW)
        response_variables.append(Respuesta)        
      
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if Respuesta.values[0] == '1. Usuario Con Menos Citas Al Mes Que Las Permitidas':
                process_response['PROCESS'] = 'TRUE'
            elif Respuesta.values[0] == '4. Usuario Con Mas Citas Al Mes Que Las Permitidas':
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Usuario Con Más Citas Al Mes Que Las Permitidas'
            else:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Error de Parametrización'
                
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
        
        return process_response
              
    valide_month = staticmethod(valide_month)

    def valide_day(owner, cursor, patient_number, appointment_date, center_code, debug=False):
        if debug:
            import pdb; pdb.set_trace()

        package_name = 'new_citas.administraparametros.val_cant_dia_ig_esp'
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        process_response ['APPOINTMENT_DATE'] = appointment_date
        
        PCA_AGE_NUMERPACIEW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_NUMERPACIEW.setvalue(0, patient_number)
        
        PCA_AGE_FECHACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_FECHACITACW.setvalue(0, appointment_date.strftime('%Y/%m/%d').ljust(10))

        PCA_AGE_LUGARW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_LUGARW.setvalue(0, center_code.ljust(8))

        RTAVALIDACIONW = cursor.var(cx_Oracle.NUMBER)
        RTAVALIDACIONW.setvalue(0, -1)
        
        Respuesta = cursor.var(cx_Oracle.STRING)
        Respuesta.setvalue(0, ''.ljust(4000))
        
        parameter_list = []
        parameter_list.append(PCA_AGE_NUMERPACIEW)
        parameter_list.append(PCA_AGE_FECHACITACW)
        parameter_list.append(PCA_AGE_LUGARW)

        response_variables= []

        response_variables.append(RTAVALIDACIONW)
        response_variables.append(Respuesta)        
      
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if Respuesta.values[0] == '1. Usuario Con Menos Citas Al Dia En La Misma Unidad Que Las Permitidas':
                process_response['PROCESS'] = 'TRUE'
            elif Respuesta.values[0] == '4. Usuario Con Más Citas Al Dia En La Misma Unidad Que Las Permitidas':
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Usuario Con Más Citas Al Día En La Misma Unidad Que Las Permitidas'
            else:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Error de Parametrización'
                
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
        
        return process_response
              
    valide_day = staticmethod(valide_day)

    def valide_day_diferent_center(owner, cursor, patient_number, appointment_date, debug=False):
        if debug:
            import pdb; pdb.set_trace()

        package_name = 'new_citas.administraparametros.val_cant_dia_dif_esp'
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        process_response ['APPOINTMENT_DATE'] = appointment_date
        
        PCA_AGE_NUMERPACIEW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_NUMERPACIEW.setvalue(0, patient_number)
        
        PCA_AGE_FECHACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_FECHACITACW.setvalue(0, appointment_date.strftime('%Y/%m/%d').ljust(10))

        RTAVALIDACIONW = cursor.var(cx_Oracle.NUMBER)
        RTAVALIDACIONW.setvalue(0, -1)
        
        Respuesta = cursor.var(cx_Oracle.STRING)
        Respuesta.setvalue(0, ''.ljust(4000))
        
        parameter_list = []
        parameter_list.append(PCA_AGE_NUMERPACIEW)
        parameter_list.append(PCA_AGE_FECHACITACW)

        response_variables= []

        response_variables.append(RTAVALIDACIONW)
        response_variables.append(Respuesta)        
      
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if Respuesta.values[0] == '1. Usuario Con Menos Citas Al Dia En Diferente Unidad Que Las Permitidas':
                process_response['PROCESS'] = 'TRUE'
            elif Respuesta.values[0] == '4. Usuario Con Mas Citas Al Dia En Diferente Unidad Que Las Permitidas':
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Usuario Con Más Citas Al Día En Diferente Unidad Que Las Permitidas'
            else:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Error de Parametrización'
                
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
        
        return process_response
              
    valide_day_diferent_center = staticmethod(valide_day_diferent_center)

    def valide_min_minutes(owner, cursor, patient_number, appointment_date_time, center_code, debug=False):
        if debug:
            import pdb; pdb.set_trace()

        package_name = 'new_citas.administraparametros.VAL_DIF_HOR_DIA_IG_ESP'
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        process_response ['APPOINTMENT_DATE_TIME'] = appointment_date_time
        
        PCA_AGE_NUMERPACIEW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_NUMERPACIEW.setvalue(0, patient_number)
        
        PCA_AGE_FECHACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_FECHACITACW.setvalue(0, appointment_date_time.strftime('%Y/%m/%d').ljust(10))

        PCA_AGE_HORACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_HORACITACW.setvalue(0, appointment_date_time.strftime('%H:%M').ljust(5))

        PCA_AGE_LUGARW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_LUGARW.setvalue(0, center_code.ljust(8))

        RTAVALIDACIONW = cursor.var(cx_Oracle.NUMBER)
        RTAVALIDACIONW.setvalue(0, -1)
        
        Respuesta = cursor.var(cx_Oracle.STRING)
        Respuesta.setvalue(0, ''.ljust(4000))
        
        parameter_list = []
        parameter_list.append(PCA_AGE_NUMERPACIEW)
        parameter_list.append(PCA_AGE_FECHACITACW)
        parameter_list.append(PCA_AGE_HORACITACW)
        parameter_list.append(PCA_AGE_LUGARW)

        response_variables= []

        response_variables.append(RTAVALIDACIONW)
        response_variables.append(Respuesta)        
      
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if Respuesta.values[0] == '1. Horario Seleccionado Sin Restriccion Para La Misma Unidad.':
                process_response['PROCESS'] = 'TRUE'
            elif Respuesta.values[0] == '2. Intenta Asignar Una Cita A La Misma Hora De Una Ya Asignada En La Misma Unidad':
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Intenta Asignar Una Cita A La Misma Hora De Una Ya Asignada En La Misma Unidad'
            elif 'Intenta Asignar Una Cita Con Una Diferencia Menor De' in Respuesta.values[0]:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  Respuesta.values[0][2:]
            else:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Error de Parametrización'
                
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
        
        return process_response
              
    valide_min_minutes = staticmethod(valide_min_minutes)

    def valide_same_specialy_month(owner, cursor, patient_number, appointment_date, speciality_id, debug=False):
        from backend.apps.operative.models import SpecialityCode
        if debug:
            import pdb; pdb.set_trace()

        speciality_vector = SpecialityCode.generateVector(speciality_id)
        service_code, speciality_code, subspeciality_code = speciality_vector

        package_name = 'new_citas.administraparametros.val_cit_igu_esp_med'
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        process_response ['APPOINTMENT_DATE'] = appointment_date
        process_response ['SPECIALITY'] = speciality_id
        
        PCA_AGE_NUMERPACIEW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_NUMERPACIEW.setvalue(0, patient_number)
        
        PCA_AGE_FECHACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_FECHACITACW.setvalue(0, appointment_date.strftime('%Y/%m/%d').ljust(10))

        SER_ESP_CODIGOW = cursor.var(cx_Oracle.STRING)
        SER_ESP_CODIGOW.setvalue(0, speciality_code.ljust(8))

        SER_SUB_CODIGOW = cursor.var(cx_Oracle.STRING)
        SER_SUB_CODIGOW.setvalue(0, subspeciality_code.ljust(8))

        RTAVALIDACIONW = cursor.var(cx_Oracle.NUMBER)
        RTAVALIDACIONW.setvalue(0, -1)
        
        Respuesta = cursor.var(cx_Oracle.STRING)
        Respuesta.setvalue(0, ''.ljust(4000))
        
        parameter_list = []
        parameter_list.append(PCA_AGE_NUMERPACIEW)
        parameter_list.append(PCA_AGE_FECHACITACW)
        parameter_list.append(SER_ESP_CODIGOW)
        parameter_list.append(SER_SUB_CODIGOW)

        response_variables= []

        response_variables.append(RTAVALIDACIONW)
        response_variables.append(Respuesta)        
      
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if Respuesta.values[0] == '1. Usuario Con Menos Citas Al Mes Que Las Permitidas Para La Misma Especialidad':
                process_response['PROCESS'] = 'TRUE'
            elif Respuesta.values[0] == '4. Usuario Con Mas Citas Al Mes Que Las Permitidas Para La Misma Especialidad':
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Usuario Con Más Citas Al Mes de la misma especialidad Que Las Permitidas'
            else:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'Error de Parametrización'
                
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
        
        return process_response
              
    valide_same_specialy_month = staticmethod(valide_same_specialy_month)
    
    """
    
DECLARE
      SRV_Message                    VARCHAR2(4000); -- IN OUT     VARCHAR2                        /*  Parametro de uso interno  */
    , In_AUX_Habilita                VARCHAR2(1); -- NO se utiliza, IN         VARCHAR2                        /*  Campo auxiliar para manejo de hora habilitada  */
    , In_AUX_ControlTiempo           VARCHAR2(1); -- NO se utiliza, IN         VARCHAR2                        /*  Auxiliar para el control del tiempo  */
    , In_PCA_AGE_CodigServi          VARCHAR2(8); -- IN         VARCHAR2                        /*  Csdigo del servicio  */
    , In_PCA_AGE_CodigProfe          VARCHAR2(13); -- IN         VARCHAR2                        /*  Csdigo del profesional  */
    , In_PCA_AGE_HoraCitac           VARCHAR2(5); -- IN         VARCHAR2                        /*  Hora de Citacisn  */
    , In_PCA_AGE_CorrelHora          number; -- IN         NUMBER                          /*  Correlativo de la hora  */
    , In_PCA_AGE_FechaCitac          VARCHAR2(10); -- Formato yyyy/mm/dd IN         VARCHAR2                        /*  Fecha de Citacisn  */
    , In_PCA_AGE_NumerPacie          number; -- IN         NUMBER                          /*  Nzmero del paciente  */
    , In_PCA_AGE_Lugar               VARCHAR2(8); -- IN         VARCHAR2                        /*  Csdigo del Lugar  */
    , In_PCA_AGE_PacNueCont          VARCHAR2(); -- IN         VARCHAR2                        /*  Paciente Nuevo o Control  */
    , In_PCA_AGE_Ambulancia          VARCHAR2(); -- IN         VARCHAR2                        /*  Indicativo de si el paciente requiere de ambulancai  */
    , In_PCA_AGE_TimeStamp           VARCHAR2(); -- IN         VARCHAR2                        /*  Campo control del registro  */
    , In_AUX_TiemposPrest            number; -- IN         NUMBER                          /*  Field que registra la sumatoria de los tiempos de cada prestacion asociada a la hora por el usuario  */
    , In_AUX_OkHora                  VARCHAR2(); -- IN         VARCHAR2                        /*  Retorna al cliente el tipo de bloque hora a signara encontrada en agenda  */
    , In_PCA_HCA_CodigUsuar          VARCHAR2(); -- IN         VARCHAR2                        /*  C_digo Usuario  */
    , In_PCA_AGE_Examenes            VARCHAR2(); -- IN         VARCHAR2                        /*  Indicativo de Examenes  */
    , In_PCA_AGE_Procedencia         VARCHAR2(); -- IN         VARCHAR2                        /*  Procedencia del Paciente  */
    , In_PCA_AGE_DetProc             VARCHAR2(); -- IN         VARCHAR2                        /*  Csdigo de la procedencia del paciente  */
    , In_PCA_AGE_ProfDeriv           VARCHAR2(); -- IN         VARCHAR2                        /*  Csdigo del profesional derivador  */
    , In_PCA_AGE_Recepcionado        VARCHAR2(); -- IN         VARCHAR2                        /*  Indicativo de si el paciente fue recepcionado  */
    , In_AutCorrelativo              number; -- IN         NUMBER                          /*  Correlativo de la Autorizacisn  */
    , In_CON_CON_Codigo              VARCHAR2(); -- IN         VARCHAR2                        /*  Csdigo de Convenio  */
    , Out_AUX_AvisoHoraOcupada       VARCHAR2(); -- OUT        VARCHAR2                        /*  Variable auxiliar para saber si una hora fue ocupada  */
BEGIN
    ADMSALUD.ATAAGE_GRABARHORAAGE3_PKG.ATAAGE_GRABARHORAAGE3(
      SRV_Message
    , In_AUX_Habilita                
    , In_AUX_ControlTiempo           
    , In_PCA_AGE_CodigServi          
    , In_PCA_AGE_CodigProfe          
    , In_PCA_AGE_HoraCitac           
    , In_PCA_AGE_CorrelHora          
    , In_PCA_AGE_FechaCitac          
    , In_PCA_AGE_NumerPacie          
    , In_PCA_AGE_Lugar               
    , In_PCA_AGE_PacNueCont          
    , In_PCA_AGE_Ambulancia          
    , In_PCA_AGE_TimeStamp           
    , In_AUX_TiemposPrest            
    , In_AUX_OkHora                  
    , In_PCA_HCA_CodigUsuar          
    , In_PCA_AGE_Examenes            
    , In_PCA_AGE_Procedencia         
    , In_PCA_AGE_DetProc             
    , In_PCA_AGE_ProfDeriv           
    , In_PCA_AGE_Recepcionado        
    , In_AutCorrelativo              
    , In_CON_CON_Codigo              
    , Out_AUX_AvisoHoraOcupada       
    );
    
    dbms_output.put_line('SRV_Message: ' || SRV_Message);
END;
    """

    def confirm(owner, cursor, patient_number, speciality_id, medic_code, appointment_date_time, corr_time, debug=False):
        from backend.apps.operative.models import SpecialityCode
        if debug:
            import pdb; pdb.set_trace()

        speciality_vector = SpecialityCode.generateVector(speciality_id)
        service_code, speciality_code, subspeciality_code = speciality_vector
        format_date = settings.DISAN_FORMAT_DATE
        if owner.id == 'ertech':
            format_date = settings.DISAN_FORMAT_DATE_TEST

        package_name = 'NEW_CITAS.ADMINISTRACITA.CONFIRMACITAASIGNADA'
        process_response = {}
        process_response ['PATIENT_NUMBER'] = patient_number
        
        PCA_AGE_CODIGSERVIW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_CODIGSERVIW.setvalue(0, service_code.ljust(8))

        PCA_AGE_CODIGPROFEW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_CODIGPROFEW.setvalue(0, medic_code.ljust(13))
        
        PCA_AGE_FECHACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_FECHACITACW.setvalue(0, appointment_date_time.strftime(format_date))
        
        PCA_AGE_HORACITACW = cursor.var(cx_Oracle.STRING)
        PCA_AGE_HORACITACW.setvalue(0, appointment_date_time.strftime('%H:%M'))
        
        PCA_AGE_CORRELHORAW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_CORRELHORAW.setvalue(0, corr_time)
        
        PCA_AGE_NUMERPACIEW = cursor.var(cx_Oracle.NUMBER)
        PCA_AGE_NUMERPACIEW.setvalue(0, patient_number)
        
        FECHACITA = cursor.var(cx_Oracle.STRING)
        FECHACITA.setvalue(0, ''.ljust(128))
        
        HORACITA = cursor.var(cx_Oracle.STRING)
        HORACITA.setvalue(0, ''.ljust(5))
        
        CENTROMEDICO = cursor.var(cx_Oracle.STRING)
        CENTROMEDICO.setvalue(0, ''.ljust(60))
        
        CONSULTORIO = cursor.var(cx_Oracle.STRING)
        CONSULTORIO.setvalue(0, ''.ljust(40))
        
        ESPECIALIDAD = cursor.var(cx_Oracle.STRING)
        ESPECIALIDAD.setvalue(0, ''.ljust(40))
        
        DOCTOR = cursor.var(cx_Oracle.STRING)
        DOCTOR.setvalue(0, ''.ljust(90))
        
        Respuesta = cursor.var(cx_Oracle.STRING)
        Respuesta.setvalue(0, ''.ljust(4000))
        
        parameter_list = []
        parameter_list.append(PCA_AGE_CODIGSERVIW)
        parameter_list.append(PCA_AGE_CODIGPROFEW)
        parameter_list.append(PCA_AGE_FECHACITACW)
        parameter_list.append(PCA_AGE_HORACITACW)
        parameter_list.append(PCA_AGE_CORRELHORAW)
        parameter_list.append(PCA_AGE_NUMERPACIEW)

        response_variables= []

        response_variables.append(FECHACITA)
        response_variables.append(HORACITA)
        response_variables.append(CENTROMEDICO)
        response_variables.append(CONSULTORIO)
        response_variables.append(ESPECIALIDAD)
        response_variables.append(DOCTOR)
        response_variables.append(Respuesta)        
      
        try:
            query_response = DBConn.executeQuery(cursor, package_name, parameter_list, response_variables, debug=debug)
            if Respuesta.values[0] == '1. Cita Asignada de Forma Exitosa.':
                process_response['PROCESS'] = 'TRUE'
                process_response['MESSAGE'] =  'Cita Asignada de Forma Exitosa'
                process_response['DATE'] = FECHACITA.values[0]
                process_response['TIME'] = HORACITA.values[0]
                process_response['CENTER'] = CENTROMEDICO.values[0]
                process_response['CONSULTING_ROOM'] = CONSULTORIO.values[0]
                process_response['SPECIALITY'] = ESPECIALIDAD.values[0]
                process_response['MEDIC'] = DOCTOR.values[0]
            else:
                process_response['PROCESS'] = 'FALSE'
                process_response['MESSAGE'] =  'La Cita No Pudo Ser Asignada'
        except:
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] =  'Se presentó un error durante la validacion'
        
        return process_response
              
    confirm = staticmethod(confirm)

    class Meta:
        abstract = True
        app_label = 'disanconn'
