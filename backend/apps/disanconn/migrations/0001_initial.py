# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-05-08 16:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('license', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DBConn',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60, verbose_name='Nombre')),
                ('username', models.CharField(max_length=60, verbose_name='Usuario')),
                ('password', models.CharField(max_length=60, verbose_name='Contraseña')),
                ('host', models.CharField(max_length=60, verbose_name='Host')),
                ('port', models.CharField(max_length=60, verbose_name='Puerto')),
                ('scheme', models.CharField(max_length=60, verbose_name='Esquema')),
                ('is_active', models.BooleanField(verbose_name='Está Activo?')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='license.Owner')),
            ],
            options={
                'verbose_name': 'Conexión Disan',
                'verbose_name_plural': 'Conexiones Disan',
                'ordering': ['name'],
            },
        ),
    ]
