# -*- coding: utf-8 -*-
from rest_framework import serializers

class validePersonSerializer(serializers.Serializer):
    auth_key = serializers.CharField()
    document_type = serializers.CharField()
    document = serializers.CharField()

    class Meta:
        verbose_name = 'Validar Persona'
        fields = ('auth_key', "document_type", "document", )

class loadScheduleDateSerializer(serializers.Serializer):
    auth_key = serializers.CharField()
    speciality = serializers.CharField()

    class Meta:
        verbose_name = 'Cargue de Agenda'
        fields = ('auth_key', "speciality", )

class LoadAssignedAppointmentsSerializer(serializers.Serializer):
    auth_key = serializers.CharField()
    patient_number = serializers.CharField()
    today = serializers.CharField()

    class Meta:
        verbose_name = 'Cargue de Citas Asignadas'
        fields = ('auth_key', "patient_number", 'today',)

class CancelAppointmentSerializer(serializers.Serializer):
    auth_key = serializers.CharField()
    patient_number = serializers.CharField()
    service_code = serializers.CharField()
    medic_code = serializers.CharField()
    appointment_date = serializers.CharField()
    appointment_time = serializers.CharField()
    corr_time = serializers.IntegerField()
    center_code = serializers.CharField()
    cancel_cause = serializers.CharField()

    class Meta:
        verbose_name = 'Cancelación de Cita'
        fields = ('auth_key', "patient_number", 'service_code', 'medic_code', 'appointment_date', 'appointment_time', 'corr_time', 'center_code', 'cancel_cause', )

class LoadCancelCauseSerializer(serializers.Serializer):
    auth_key = serializers.CharField()

    class Meta:
        verbose_name = 'Cargue de Causas de Cancelación'
        fields = ('auth_key',)
        
class ConfirmAppointmentSerializer(serializers.Serializer):
    auth_key = serializers.CharField()
    patient_number = serializers.CharField()
    service_code = serializers.CharField()
    medic_code = serializers.CharField()
    appointment_date = serializers.CharField()
    appointment_time = serializers.CharField()
    corr_time = serializers.IntegerField()

    class Meta:
        verbose_name = 'Confirmación de Cita'
        fields = ('auth_key', "patient_number", 'service_code', 'medic_code', 'appointment_date', 'appointment_time', 'corr_time', )

class AssignAppointmentSerializer(serializers.Serializer):
    auth_key = serializers.CharField()
    patient_number = serializers.CharField()
    speciality = serializers.CharField()
    medic_code = serializers.CharField()
    center_code = serializers.CharField()
    appointment_date = serializers.CharField()
    appointment_time = serializers.CharField()
    corr_time = serializers.IntegerField()
    block_time = serializers.IntegerField()
    timestamp = serializers.IntegerField()

    class Meta:
        verbose_name = 'Asignación de Cita'
        fields = ('auth_key', "patient_number", 'speciality', 'medic_code', 'center_code', 'appointment_date', 'appointment_time', 'corr_time', 'block_time', 'timestamp',)
