# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-05-16 16:56
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('operative', '0009_auto_20190516_1650'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SpecialtyCode',
            new_name='SpecialityCode',
        ),
    ]
