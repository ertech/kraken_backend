# -*- coding: utf-8 -*-
from django.db import models
from backend.apps.disanconn.models import DBConn, HasRigths, LoadSchedule,\
    LoadAssignedAppointments, CancelAppointment, LoadCancelCause,\
    ConfirmAppointment, AssignAppointment
from django.contrib.postgres.fields import JSONField
from backend.auth.license.models import Environment

class GeneralParameters(models.Model):
    owner = models.ForeignKey('license.Owner')
    environment = models.ForeignKey('license.Environment')
    get_permit = models.BooleanField(verbose_name='Permite GET?')

    def __str__(self):
        return u"%s-%s" % (self.owner, self.environment)

    def permit_method(owner, method):
        
        try:
            general_parameter = GeneralParameters.objects.get(owner=owner, environment=Environment.getEnvironmentName())
            if method == 'GET': 
                return general_parameter.get_permit
        except GeneralParameters.DoesNotExist:
            return False
        return True
    
    permit_method = staticmethod(permit_method)

    class Meta:
        app_label = 'operative'
        verbose_name = 'Parametros Generales'
        verbose_name_plural = 'Parametros Generales'

class DocumentType(models.Model):
    id = models.CharField(verbose_name='ID', max_length=2, primary_key=True)
    name = models.CharField(verbose_name='Nombre', max_length=20)
    disan_code = models.CharField(verbose_name='Código DISAN', max_length=2)

    def __str__(self):
        return u"%s" % (self.name)

    class Meta:
        app_label = 'operative'
        verbose_name = 'Tipo de Documento'
        verbose_name_plural = 'Tipos de Documentos'

class SpecialityCode(models.Model):
    id = models.CharField(verbose_name='ID', max_length=2, primary_key=True)
    name = models.CharField(verbose_name='Nombre', max_length=40)
    service_code = models.CharField(verbose_name='Código Servicio', max_length=8)
    speciality_code = models.CharField(verbose_name='Código Especialidad', max_length=10)
    subspeciality_code = models.CharField(verbose_name='Código Subespecialidad', max_length=10)

    def __str__(self):
        return u"%s" % (self.name)

    def generateVector(extern_speciality):
        result = (None, None, None)
        try:
            speciality = SpecialityCode.objects.get(pk=extern_speciality)
            result = (speciality.service_code, speciality.speciality_code, speciality.subspeciality_code)
        except SpecialityCode.DoesNotExist:
            pass
        return result

    generateVector = staticmethod(generateVector)
    
    class Meta:
        app_label = 'operative'
        verbose_name = 'Especialidad'
        verbose_name_plural = 'Especialidades'

class Transaction(models.Model):
    owner = models.ForeignKey('license.Owner', null=True, blank=True)
    system = models.CharField(verbose_name='Sistema', max_length=100, null=True, blank=True)
    date_time = models.DateTimeField(auto_now=True)
    request = models.TextField(verbose_name='Request')
    parameters = JSONField(verbose_name='Parametros', null=True, blank=True)
    response = models.TextField(verbose_name='Response', null=True, blank=True)
    error = models.TextField(verbose_name='Error', null=True, blank=True)
    exception = models.TextField(verbose_name='Excepcion', null=True, blank=True)
    document_type = models.CharField(verbose_name='Tipo Documentación', max_length=2, null=True, blank=True)
    identification = models.CharField(verbose_name='Identificación', max_length=20, null=True, blank=True)
    patient_number = models.IntegerField(verbose_name='Número de Paciente', null=True, blank=True)
    
    def changeDocument(extern_document_type):
        try:
            document_type = DocumentType.objects.get(pk=extern_document_type)
        except DocumentType.DoesNotExist:
            return ''
        return document_type.disan_code

    changeDocument = staticmethod(changeDocument)
    
    def TranslateResponse(response):
        result = {}
        result['PROCESS'] = 'FALSE'
        result['MESSAGE'] = ''
        if len(response) > 0: 
            if response[0] == '1. Usuario Con Afiliacion Correcta.':
                result['PROCESS'] = 'TRUE'
            elif response[0] == '2. Tipo de Documento No Registrado en <Tipos de Documentos>.':
                result['MESSAGE'] = 'Tipo de Documento Inexistente'
            elif response[0] == '3. Usuario Con Afiliacion No Regularizada, Favor Presentarse A La Oficina Registro Y Control De Usuarios (Afiliaciones).':
                result['MESSAGE'] = 'Validar la Afiliacion'
            elif response[0] == '4. Usuario Con Plan De Atencion No Valido.':
                result['MESSAGE'] = 'Paciente fuera de la Jurisdiccion'
            else:
                result['MESSAGE'] = 'Se presento un error durante la validacion'
        else:
            result['MESSAGE'] = 'Se presento un error durante la validacion'
        
        return result
        
    TranslateResponse = staticmethod(TranslateResponse)
    
    def validate_person(request, external_system, owner, document_type_id, document, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        error = ''
        process_response = {}
        transaction = Transaction()
        transaction.owner = owner
        transaction.system = external_system.name
        transaction.request = request
        transaction.document_type = document_type_id
        transaction.document = document
        transaction.parameters = {'DOCUMENT_TYPE': document_type_id,
                                 'DOCUMENT': document
                        }
        dbconn_list = DBConn.objects.filter(owner=owner, is_active=True)
        if dbconn_list.count() > 0:
            dbconn = dbconn_list[0]
            
            conn = dbconn.createConn()
            
            if conn is not None:
                cursor = conn.cursor()
                
                try:
                    process_response = HasRigths.process(owner, cursor, Transaction.changeDocument(document_type_id), document, dbconn.username, debug=debug) 
                    if process_response['PATIENT_NUMBER'] is not None:
                        transaction.patient_number = process_response['PATIENT_NUMBER']
                    if process_response['PROCESS'] == 'FALSE':
                        error = process_response['MESSAGE']
                except Exception as e:
                    exception = 'No se pudo ejecutar el query: %s' % str(e)
                    transaction.exception = exception
                    error = 'Se presento un error durante la validacion'

                cursor.close()
                conn.close()
            else:
                error = 'No se puede establecer la conexión con el servidor'
        else:
            error = 'No se ha parametrizado el sistema correctamente'  
        
        transaction.error = error
        if error != '':
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = error

        transaction.response = process_response
        transaction.save()
        process_response['ID'] = transaction.id
        process_response['PATIENT_NUMBER'] = transaction.patient_number
        return process_response, error
    
    validate_person = staticmethod(validate_person)

    def load_schedule(request, external_system, owner, speciality_id, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        error = ''
        process_response = {}
        transaction = Transaction()
        transaction.owner = owner
        transaction.system = external_system.name
        transaction.request = request
        transaction.parameters = {'SPECIALITY': speciality_id,
                        }
        dbconn_list = DBConn.objects.filter(owner=owner, is_active=True)
        if dbconn_list.count() > 0:
            dbconn = dbconn_list[0]
            
            conn = dbconn.createConn()
            
            if conn is not None:
                cursor = conn.cursor()
                 
                try:
                    process_response = LoadSchedule.process(owner, cursor, speciality_id, dbconn.username, debug=debug) 
#                     
                    if process_response['PROCESS'] == 'FALSE':
                        error = process_response['MESSAGE']
                except Exception as e:
                    exception = 'No se pudo ejecutar el query: %s' % str(e)
                    transaction.exception = exception
                    error = 'Se presento un error durante la validacion'

                cursor.close()
                conn.close()
            else:
                error = 'No se puede establecer la conexión con el servidor'
        else:
            error = 'No se ha parametrizado el sistema correctamente'  
        
        transaction.error = error
        if error != '':
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = error

        transaction.response = process_response
        transaction.save()
        process_response['ID'] = transaction.id
        return process_response, error
    
    load_schedule = staticmethod(load_schedule)

    def load_assigned_appointment(request, external_system, owner, patient_number, today=None, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        error = ''
        process_response = {}
        transaction = Transaction()
        transaction.owner = owner
        transaction.system = external_system.name
        transaction.request = request
        transaction.patient_number = patient_number 
        transaction.parameters = {'PATIENT_NUMBER': patient_number,
                        }
        dbconn_list = DBConn.objects.filter(owner=owner, is_active=True)
        if dbconn_list.count() > 0:
            dbconn = dbconn_list[0]
            
            conn = dbconn.createConn()
            
            if conn is not None:
                cursor = conn.cursor()
                 
                try:
                    process_response = LoadAssignedAppointments.process(owner, cursor, patient_number, today, debug=debug) 
                    
                    if process_response['PROCESS'] == 'FALSE':
                        error = process_response['MESSAGE']
                except Exception as e:
                    exception = 'No se pudo ejecutar el query: %s' % str(e)
                    transaction.exception = exception
                    error = 'Se presento un error durante la validacion'

                cursor.close()
                conn.close()
            else:
                error = 'No se puede establecer la conexión con el servidor'
        else:
            error = 'No se ha parametrizado el sistema correctamente'  
        
        transaction.error = error
        if error != '':
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = error

        transaction.response = process_response
        transaction.save()
        process_response['ID'] = transaction.id
        return process_response, error
    
    load_assigned_appointment = staticmethod(load_assigned_appointment)

    def cancel_appointment(request, external_system, owner, patient_number, service_code, medic_code, appointment_date_time, corr_time, center_code, cancel_cause, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        error = ''
        process_response = {}
        transaction = Transaction()
        transaction.owner = owner
        transaction.system = external_system.name
        transaction.request = request
        transaction.patient_number = patient_number 
        transaction.parameters = {
                                    'PATIENT_NUMBER': patient_number,
                                    'SERVICE_CODE': service_code,
                                    'MEDIC_CODE': medic_code,
                                    'APOPOINTMENT_DATE_TIME': appointment_date_time.strftime('%Y-%m-%d %H:%M'),
                                    'CORR_TIME': corr_time,
                                    'CENTER_CODE': center_code,
                                    'CANCEL_CAUSE': cancel_cause,
                        }
        dbconn_list = DBConn.objects.filter(owner=owner, is_active=True)
        if dbconn_list.count() > 0:
            dbconn = dbconn_list[0]
            
            conn = dbconn.createConn()
            
            if conn is not None:
                cursor = conn.cursor()
                 
                try:
                    process_response = CancelAppointment.process(owner, cursor, patient_number, service_code, medic_code, appointment_date_time, corr_time, center_code, cancel_cause, dbconn.username, debug=debug) 
                    
                    if process_response['PROCESS'] == 'FALSE':
                        error = process_response['MESSAGE']
                except Exception as e:
                    exception = 'No se pudo ejecutar el query: %s' % str(e)
                    transaction.exception = exception
                    error = 'Se presento un error durante la validacion'

                cursor.close()
                conn.close()
            else:
                error = 'No se puede establecer la conexión con el servidor'
        else:
            error = 'No se ha parametrizado el sistema correctamente'  
        
        transaction.error = error
        if error != '':
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = error

        transaction.response = process_response
        transaction.save()
        process_response['ID'] = transaction.id
        return process_response, error
    
    cancel_appointment = staticmethod(cancel_appointment)

    def cancel_cause(request, external_system, owner, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        error = ''
        process_response = {}
        transaction = Transaction()
        transaction.owner = owner
        transaction.system = external_system.name
        transaction.request = request
        transaction.parameters = { }
        dbconn_list = DBConn.objects.filter(owner=owner, is_active=True)
        if dbconn_list.count() > 0:
            dbconn = dbconn_list[0]
            conn = dbconn.createConn()
            if conn is not None:
                cursor = conn.cursor()
                try:
                    method_response = LoadCancelCause.process(owner, cursor, debug=debug)
                    process_response['PROCESS'] = method_response['PROCESS'] 
                    if method_response['PROCESS'] == 'FALSE':
                        error = method_response['MESSAGE']
                    else:
                        for key in method_response:
                            process_response[key] = method_response[key]

                except Exception as e:
                    exception = 'No se pudo ejecutar el query: %s' % str(e)
                    transaction.exception = exception
                    error = 'Se presento un error durante la validacion'
                cursor.close()
                conn.close()
            else:
                error = 'No se puede establecer la conexión con el servidor'
        else:
            error = 'No se ha parametrizado el sistema correctamente'  
        transaction.error = error
        if error != '':
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = error
        transaction.response = process_response
        transaction.save()
        process_response['ID'] = transaction.id
        return process_response, error

    cancel_cause = staticmethod(cancel_cause)

    def confirm_appointment(request, external_system, owner, patient_number, service_code, medic_code, appointment_date_time, corr_time, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        error = ''
        process_response = {}
        transaction = Transaction()
        transaction.owner = owner
        transaction.system = external_system.name
        transaction.request = request
        transaction.patient_number = patient_number 
        transaction.parameters = {
                                    'PATIENT_NUMBER': patient_number,
                                    'SERVICE_CODE': service_code,
                                    'MEDIC_CODE': medic_code,
                                    'APOPOINTMENT_DATE_TIME': appointment_date_time.strftime('%Y-%m-%d %H:%M'),
                                    'CORR_TIME': corr_time,
                        }
        dbconn_list = DBConn.objects.filter(owner=owner, is_active=True)
        if dbconn_list.count() > 0:
            dbconn = dbconn_list[0]
            
            conn = dbconn.createConn()
            
            if conn is not None:
                cursor = conn.cursor()
                 
                try:
                    process_response = ConfirmAppointment.process(owner, cursor, patient_number, service_code, medic_code, appointment_date_time, corr_time, debug=debug) 
                    
                    if process_response['PROCESS'] == 'FALSE':
                        error = process_response['MESSAGE']
                except Exception as e:
                    exception = 'No se pudo ejecutar el query: %s' % str(e)
                    transaction.exception = exception
                    error = 'Se presento un error durante la validacion'

                cursor.close()
                conn.close()
            else:
                error = 'No se puede establecer la conexión con el servidor'
        else:
            error = 'No se ha parametrizado el sistema correctamente'  
        
        transaction.error = error
        if error != '':
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = error

        transaction.response = process_response
        transaction.save()
        process_response['ID'] = transaction.id
        return process_response, error
    
    confirm_appointment = staticmethod(confirm_appointment)

    def assign_appointment(request, external_system, owner, patient_number, speciality_id, medic_code, center_code, appointment_date_time, corr_time, block_time, timestamp, debug=False):
        if debug:
            import pdb; pdb.set_trace()
        error = ''
        process_response = {}
        transaction = Transaction()
        transaction.owner = owner
        transaction.system = external_system.name
        transaction.request = request
        transaction.patient_number = patient_number 
        transaction.parameters = {
                                    'PATIENT_NUMBER': patient_number,
                                    'SPECIALITY': speciality_id,
                                    'MEDIC_CODE': medic_code,
                                    'CENTER_CODE': center_code,
                                    'APOPOINTMENT_DATE_TIME': appointment_date_time.strftime('%Y-%m-%d %H:%M'),
                                    'CORR_TIME': corr_time,
                                    'BLOCK_TIME': block_time,
                        }
        dbconn_list = DBConn.objects.filter(owner=owner, is_active=True)
        if dbconn_list.count() > 0:
            dbconn = dbconn_list[0]
            
            conn = dbconn.createConn()
            
            if conn is not None:
                cursor = conn.cursor()
                 
                try:
                    process_response = AssignAppointment.process(owner, cursor, patient_number, speciality_id, medic_code, center_code, appointment_date_time, corr_time, block_time, timestamp, debug=debug) 
                    
                    if process_response['PROCESS'] == 'FALSE':
                        error = process_response['MESSAGE']
                except Exception as e:
                    exception = 'No se pudo ejecutar el query: %s' % str(e)
                    transaction.exception = exception
                    error = 'Se presento un error durante la validacion'

                cursor.close()
                conn.close()
            else:
                error = 'No se puede establecer la conexión con el servidor'
        else:
            error = 'No se ha parametrizado el sistema correctamente'  
        
        transaction.error = error
        if error != '':
            process_response['PROCESS'] = 'FALSE'
            process_response['MESSAGE'] = error

        transaction.response = process_response
        transaction.save()
        process_response['ID'] = transaction.id
        return process_response, error
    
    assign_appointment = staticmethod(assign_appointment)

    def __str__(self):
        return u"%s" % (self.name)

    class Meta:
        ordering = ['-date_time']
        app_label = 'operative'
        verbose_name = 'Transacción'
        verbose_name_plural = 'Transacciones'
