# -*- coding: utf-8 -*-

from backend.auth.accounts.models import ExternalSystem
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from backend.apps.operative.serializers import validePersonSerializer,\
    loadScheduleDateSerializer, LoadAssignedAppointmentsSerializer,\
    CancelAppointmentSerializer, LoadCancelCauseSerializer,\
    ConfirmAppointmentSerializer, AssignAppointmentSerializer
from backend.apps.operative.models import Transaction, GeneralParameters
import datetime

def validate_data(method, request_data):
    error_msg = ''
    auth_key = None
    external_system = None
    owner = None
    data = {}
    data['RESPONSE_KEY'] = '' 
    validate_response = '1'
    if 'auth_key' in request_data.keys() and request_data['auth_key'] is not None and request_data['auth_key'] != '': 
        auth_key = request_data['auth_key']
        try:
            external_system = ExternalSystem.objects.get(auth_key=auth_key)
        except ExternalSystem.DoesNotExist:
            validate_response = '0'
            error_msg = 'Sistema no registrado'
    else:
        validate_response = '0'
        error_msg = 'No se ha enviado la llave del Servidor'
    
    if validate_response == '1':
        if external_system is not None:
            owner = external_system.owner
            if external_system is not None:
                data['RESPONSE_KEY'] = external_system.response_key
        else:
            validate_response = '0'
            error_msg = 'Sistema no registrado'

    if validate_response == '1':
        if not GeneralParameters.permit_method(owner, method):
            validate_response = '0'
            error_msg = 'Método no valido'

    if validate_response == '0':
        data['ERROR'] = error_msg
        data['PROCESS'] = 'FALSE'
    else:
        data['PROCESS'] = 'TRUE'

    return external_system, data

class validePersonViewSet(APIView):
    serializer_class = validePersonSerializer
    
    def get(self, request):
        external_system, data = validate_data('GET', request.GET)
        if data['PROCESS'] == 'TRUE':
            return self.process(request.GET, external_system, data)
        response_status = status.HTTP_400_BAD_REQUEST
        return Response(data=data, status=response_status)

    def post(self, request):
        external_system, data = validate_data('POST', request.data)
        if data['PROCESS'] == 'TRUE':
            return self.process(request.data, external_system, data)
        response_status = status.HTTP_400_BAD_REQUEST
        return Response(data=data, status=response_status)

    def process(self, request_data, external_system, data):
        validate_response = '1'
        error_msg = ''
        method_response = {}
        
        document_type = None
        document = None

        if validate_response == '1':
            if 'document_type' in request_data.keys() and request_data['document_type'] is not None and request_data['document_type'] != '': 
                document_type = request_data['document_type']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el tipo de documento'

        if validate_response == '1':
            if 'document' in request_data.keys() and request_data['document'] is not None and request_data['document'] != '': 
                document = request_data['document']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el documento'
    
        data['DOCUMENT'] = document
        if validate_response == '1':
            method_response, error_msg = Transaction.validate_person('person/validate/', external_system, external_system.owner, document_type, document)
            if method_response is None:
                validate_response = '0'
                error_msg = 'Se presento un error durante la validacion'
                
        if validate_response == '1':
            for key in method_response:
                data[key] = method_response[key]
            if method_response['PROCESS'] == 'FALSE':
                error_msg = method_response['MESSAGE']
                validate_response = '0'
        
        if validate_response == '0':
            data['ERROR'] = error_msg
            data['PROCESS'] = 'FALSE'
         
        if validate_response == '1':
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST

        return Response(data=data, status=response_status)

class loadScheduleDateViewSet(APIView):
    serializer_class = loadScheduleDateSerializer
    
    def get(self, request):
        return self.process(request, request.GET)

    def post(self, request):
        return self.process(request, request.data)

    def process(self, request, request_data):
        validate_response = '1'
        error_msg = ''
        method_response = {}
        
        owner = None
        external_system = None
        auth_key = None
        speciality = None

        if 'auth_key' in request_data.keys() and request_data['auth_key'] is not None and request_data['auth_key'] != '': 
            auth_key = request_data['auth_key']
            try:
                external_system = ExternalSystem.objects.get(auth_key=auth_key)
            except ExternalSystem.DoesNotExist:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
        else:
            validate_response = '0'
            error_msg = 'No se ha enviado la llave del Servidor'

        if validate_response == '1':
            if 'speciality' in request_data.keys() and request_data['speciality'] is not None and request_data['speciality'] != '': 
                speciality = request_data['speciality']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la Especialidad'

        if validate_response == '1':
            if external_system is not None:
                owner = external_system.owner
            else:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
            
        data = {}

        if validate_response == '1':
            method_response, error_msg = Transaction.load_schedule('load/scheduledate/', external_system, owner, speciality)
            if method_response is None:
                validate_response = '0'
                error_msg = 'Se presento un error durante la validacion'
                
        if validate_response == '1':
            for key in method_response:
                data[key] = method_response[key]
            if method_response['PROCESS'] == 'FALSE':
                error_msg = method_response['MESSAGE']
                validate_response = '0'
        
        if validate_response == '0':
            data['ERROR'] = error_msg
            data['PROCESS'] = 'FALSE'
        if external_system is not None:
            data['RESPONSE_KEY'] = external_system.response_key
        else:
            data['RESPONSE_KEY'] = '' 
         
        if validate_response == '1':
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST

        return Response(data=data, status=response_status)

class LoadAssignedAppointmentsViewSet(APIView):
    serializer_class = LoadAssignedAppointmentsSerializer
    
    def get(self, request):
        return self.process(request, request.GET)

    def post(self, request):
        return self.process(request, request.data)

    def process(self, request, request_data):
        validate_response = '1'
        error_msg = ''
        method_response = {}
        
        owner = None
        external_system = None
        auth_key = None
        patient_number = None
        today = None

        if 'auth_key' in request_data.keys() and request_data['auth_key'] is not None and request_data['auth_key'] != '': 
            auth_key = request_data['auth_key']
            try:
                external_system = ExternalSystem.objects.get(auth_key=auth_key)
            except ExternalSystem.DoesNotExist:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
        else:
            validate_response = '0'
            error_msg = 'No se ha enviado la llave del Servidor'

        if validate_response == '1':
            if 'patient_number' in request_data.keys() and request_data['patient_number'] is not None and request_data['patient_number'] != '': 
                try:
                    patient_number = float(request_data['patient_number'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato del número del paciente no es correcto'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Número del paciente'

        if validate_response == '1':
            if 'today' in request_data.keys() and request_data['today'] is not None and request_data['today'] != '': 
                try:
                    today = datetime.datetime.strptime(request_data['today'], '%d/%m/%y').date()
                except:
                    pass

        if validate_response == '1':
            if external_system is not None:
                owner = external_system.owner
            else:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
            
        data = {}

        if validate_response == '1':
            method_response, error_msg = Transaction.load_assigned_appointment('load/assignedappointments/', external_system, owner, patient_number, today)
            if method_response is None:
                validate_response = '0'
                error_msg = 'Se presento un error durante la validacion'
                
        if validate_response == '1':
            for key in method_response:
                data[key] = method_response[key]
            if method_response['PROCESS'] == 'FALSE':
                error_msg = method_response['MESSAGE']
                validate_response = '0'
        
        if validate_response == '0':
            data['ERROR'] = error_msg
            data['PROCESS'] = 'FALSE'
        if external_system is not None:
            data['RESPONSE_KEY'] = external_system.response_key
        else:
            data['RESPONSE_KEY'] = '' 
         
        if validate_response == '1':
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST

        return Response(data=data, status=response_status)

class CancelAppointmentViewSet(APIView):
    serializer_class = CancelAppointmentSerializer
    
    def get(self, request):
        return self.process(request, request.GET)

    def post(self, request):
        return self.process(request, request.data)

    def process(self, request, request_data):
        validate_response = '1'
        error_msg = ''
        method_response = {}
        
        owner = None
        external_system = None
        auth_key = None
        patient_number = None
        service_code = None
        medic_code = None
        appointment_date = None
        appointment_time = None
        appointment_date_time = None
        corr_time = None
        center_code = None
        cancel_cause = None

        if 'auth_key' in request_data.keys() and request_data['auth_key'] is not None and request_data['auth_key'] != '': 
            auth_key = request_data['auth_key']
            try:
                external_system = ExternalSystem.objects.get(auth_key=auth_key)
            except ExternalSystem.DoesNotExist:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
        else:
            validate_response = '0'
            error_msg = 'No se ha enviado la llave del Servidor'

        if validate_response == '1':
            if 'patient_number' in request_data.keys() and request_data['patient_number'] is not None and request_data['patient_number'] != '': 
                try:
                    patient_number = float(request_data['patient_number'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato del número del paciente no es correcto'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Número del paciente'

        if validate_response == '1':
            if 'service_code' in request_data.keys() and request_data['service_code'] is not None and request_data['service_code'] != '': 
                service_code = request_data['service_code']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Servicio'

        if validate_response == '1':
            if 'medic_code' in request_data.keys() and request_data['medic_code'] is not None and request_data['medic_code'] != '': 
                medic_code = request_data['medic_code']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Médico'

        if validate_response == '1':
            if 'appointment_date' in request_data.keys() and request_data['appointment_date'] is not None and request_data['appointment_date'] != '': 
                try:
                    appointment_date = datetime.datetime.strptime(request_data['appointment_date'], '%d/%m/%y').date()
                except:
                    validate_response = '0'
                    error_msg = 'Error en el Formato de la Fecha'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la Fecha de la Cita'

        if validate_response == '1':
            if 'appointment_time' in request_data.keys() and request_data['appointment_time'] is not None and request_data['appointment_time'] != '': 
                try:
                    appointment_time = datetime.datetime.strptime(request_data['appointment_time'], '%H:%M').time()
                except:
                    validate_response = '0'
                    error_msg = 'Error en el Formato de la Hora'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la Hora de la Cita'

        if validate_response == '1':
            appointment_date_time = datetime.datetime.combine(appointment_date, appointment_time)

        if validate_response == '1':
            if 'corr_time' in request_data.keys() and request_data['corr_time'] is not None and request_data['corr_time'] != '': 
                try:
                    corr_time = float(request_data['corr_time'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato del CORR_TIME no es correcto'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el CORR_TIME'
                
                
        if validate_response == '1':
            if 'center_code' in request_data.keys() and request_data['center_code'] is not None and request_data['center_code'] != '': 
                center_code = request_data['center_code']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Código del Lugar'
                
                
        if validate_response == '1':
            if 'cancel_cause' in request_data.keys() and request_data['cancel_cause'] is not None and request_data['cancel_cause'] != '': 
                cancel_cause = request_data['cancel_cause']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la Causa de la Cancelación'
                
        if validate_response == '1':
            if external_system is not None:
                owner = external_system.owner
            else:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
            
        data = {}

        if validate_response == '1':
            method_response, error_msg = Transaction.cancel_appointment('cancel/appointment/', external_system, owner, patient_number, service_code, medic_code, appointment_date_time, corr_time, center_code, cancel_cause)
            if method_response is None:
                validate_response = '0'
                error_msg = 'Se presento un error durante la validacion'
                
        if validate_response == '1':
            for key in method_response:
                data[key] = method_response[key]
            if method_response['PROCESS'] == 'FALSE':
                error_msg = method_response['MESSAGE']
                validate_response = '0'
        
        if validate_response == '0':
            data['ERROR'] = error_msg
            data['PROCESS'] = 'FALSE'
        if external_system is not None:
            data['RESPONSE_KEY'] = external_system.response_key
        else:
            data['RESPONSE_KEY'] = '' 
         
        if validate_response == '1':
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST

        return Response(data=data, status=response_status)

class LoadCancelCauseViewSet(APIView):
    serializer_class = LoadCancelCauseSerializer
    
    def get(self, request):
        return self.process(request, request.GET)

    def post(self, request):
        return self.process(request, request.data)

    def process(self, request, request_data):
        validate_response = '1'
        error_msg = ''
        method_response = {}
        
        owner = None
        external_system = None
        auth_key = None
        
        if 'auth_key' in request_data.keys() and request_data['auth_key'] is not None and request_data['auth_key'] != '': 
            auth_key = request_data['auth_key']
            try:
                external_system = ExternalSystem.objects.get(auth_key=auth_key)
            except ExternalSystem.DoesNotExist:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
        else:
            validate_response = '0'
            error_msg = 'No se ha enviado la llave del Servidor'

        data = {}

        if validate_response == '1':
            if external_system is not None:
                owner = external_system.owner
            else:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
            
        if validate_response == '1':
            method_response, error_msg = Transaction.cancel_cause('cancel/cause/', external_system, owner)
            if method_response is None:
                validate_response = '0'
                error_msg = 'Se presento un error durante la validacion'
                
        if validate_response == '1':
            for key in method_response:
                data[key] = method_response[key]
            if method_response['PROCESS'] == 'FALSE':
                error_msg = method_response['MESSAGE']
                validate_response = '0'
        
        if validate_response == '0':
            data['ERROR'] = error_msg
            data['PROCESS'] = 'FALSE'
        if external_system is not None:
            data['RESPONSE_KEY'] = external_system.response_key
        else:
            data['RESPONSE_KEY'] = '' 
         
        if validate_response == '1':
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST

        return Response(data=data, status=response_status)

class ConfirmAppointmentViewSet(APIView):
    serializer_class = ConfirmAppointmentSerializer
    
    def get(self, request):
        return self.process(request, request.GET)

    def post(self, request):
        return self.process(request, request.data)

    def process(self, request, request_data):
        validate_response = '1'
        error_msg = ''
        method_response = {}
        
        owner = None
        external_system = None
        auth_key = None
        patient_number = None
        service_code = None
        medic_code = None
        appointment_date = None
        appointment_time = None
        appointment_date_time = None
        corr_time = None

        if 'auth_key' in request_data.keys() and request_data['auth_key'] is not None and request_data['auth_key'] != '': 
            auth_key = request_data['auth_key']
            try:
                external_system = ExternalSystem.objects.get(auth_key=auth_key)
            except ExternalSystem.DoesNotExist:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
        else:
            validate_response = '0'
            error_msg = 'No se ha enviado la llave del Servidor'

        if validate_response == '1':
            if 'patient_number' in request_data.keys() and request_data['patient_number'] is not None and request_data['patient_number'] != '': 
                try:
                    patient_number = float(request_data['patient_number'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato del número del paciente no es correcto'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Número del paciente'

        if validate_response == '1':
            if 'service_code' in request_data.keys() and request_data['service_code'] is not None and request_data['service_code'] != '': 
                service_code = request_data['service_code']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Servicio'

        if validate_response == '1':
            if 'medic_code' in request_data.keys() and request_data['medic_code'] is not None and request_data['medic_code'] != '': 
                medic_code = request_data['medic_code']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Médico'

        if validate_response == '1':
            if 'appointment_date' in request_data.keys() and request_data['appointment_date'] is not None and request_data['appointment_date'] != '': 
                try:
                    appointment_date = datetime.datetime.strptime(request_data['appointment_date'], '%d/%m/%y').date()
                except:
                    validate_response = '0'
                    error_msg = 'Error en el Formato de la Fecha'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la Fecha de la Cita'

        if validate_response == '1':
            if 'appointment_time' in request_data.keys() and request_data['appointment_time'] is not None and request_data['appointment_time'] != '': 
                try:
                    appointment_time = datetime.datetime.strptime(request_data['appointment_time'], '%H:%M').time()
                except:
                    validate_response = '0'
                    error_msg = 'Error en el Formato de la Hora'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la Hora de la Cita'

        if validate_response == '1':
            appointment_date_time = datetime.datetime.combine(appointment_date, appointment_time)

        if validate_response == '1':
            if 'corr_time' in request_data.keys() and request_data['corr_time'] is not None and request_data['corr_time'] != '': 
                try:
                    corr_time = float(request_data['corr_time'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato del CORR_TIME no es correcto'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el CORR_TIME'
                
        if validate_response == '1':
            if external_system is not None:
                owner = external_system.owner
            else:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
            
        data = {}

        if validate_response == '1':
            method_response, error_msg = Transaction.confirm_appointment('confirm/appointment/', external_system, owner, patient_number, service_code, medic_code, appointment_date_time, corr_time)
            if method_response is None:
                validate_response = '0'
                error_msg = 'Se presento un error durante la validacion'
                
        if validate_response == '1':
            for key in method_response:
                data[key] = method_response[key]
            if method_response['PROCESS'] == 'FALSE':
                error_msg = method_response['MESSAGE']
                validate_response = '0'
        
        if validate_response == '0':
            data['ERROR'] = error_msg
            data['PROCESS'] = 'FALSE'
        if external_system is not None:
            data['RESPONSE_KEY'] = external_system.response_key
        else:
            data['RESPONSE_KEY'] = '' 
         
        if validate_response == '1':
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST

        return Response(data=data, status=response_status)

class AssignAppointmentViewSet(APIView):
    serializer_class = AssignAppointmentSerializer
    
    def get(self, request):
        return self.process(request, request.GET)

    def post(self, request):
        return self.process(request, request.data)

    def process(self, request, request_data):
        validate_response = '1'
        error_msg = ''
        method_response = {}
        
        owner = None
        external_system = None
        auth_key = None
        patient_number = None
        speciality = None
        center_code = None
        medic_code = None
        appointment_date = None
        appointment_time = None
        appointment_date_time = None
        corr_time = None
        block_time = None
        timestamp = None

        if 'auth_key' in request_data.keys() and request_data['auth_key'] is not None and request_data['auth_key'] != '': 
            auth_key = request_data['auth_key']
            try:
                external_system = ExternalSystem.objects.get(auth_key=auth_key)
            except ExternalSystem.DoesNotExist:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
        else:
            validate_response = '0'
            error_msg = 'No se ha enviado la llave del Servidor'

        if validate_response == '1':
            if 'patient_number' in request_data.keys() and request_data['patient_number'] is not None and request_data['patient_number'] != '': 
                try:
                    patient_number = float(request_data['patient_number'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato del número del paciente no es correcto'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Número del paciente'

        if validate_response == '1':
            if 'speciality' in request_data.keys() and request_data['speciality'] is not None and request_data['speciality'] != '': 
                speciality = request_data['speciality']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la Especialidad'

        if validate_response == '1':
            if 'medic_code' in request_data.keys() and request_data['medic_code'] is not None and request_data['medic_code'] != '': 
                medic_code = request_data['medic_code']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Médico'

        if validate_response == '1':
            if 'center_code' in request_data.keys() and request_data['center_code'] is not None and request_data['center_code'] != '': 
                center_code = request_data['center_code']
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el Código del Centro'

        if validate_response == '1':
            if 'appointment_date' in request_data.keys() and request_data['appointment_date'] is not None and request_data['appointment_date'] != '': 
                try:
                    appointment_date = datetime.datetime.strptime(request_data['appointment_date'], '%d/%m/%y').date()
                except:
                    validate_response = '0'
                    error_msg = 'Error en el Formato de la Fecha'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la Fecha de la Cita'

        if validate_response == '1':
            if 'appointment_time' in request_data.keys() and request_data['appointment_time'] is not None and request_data['appointment_time'] != '': 
                try:
                    appointment_time = datetime.datetime.strptime(request_data['appointment_time'], '%H:%M').time()
                except:
                    validate_response = '0'
                    error_msg = 'Error en el Formato de la Hora'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la Hora de la Cita'

        if validate_response == '1':
            appointment_date_time = datetime.datetime.combine(appointment_date, appointment_time)

        if validate_response == '1':
            if 'corr_time' in request_data.keys() and request_data['corr_time'] is not None and request_data['corr_time'] != '': 
                try:
                    corr_time = float(request_data['corr_time'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato del CORR_TIME no es correcto'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado el CORR_TIME'
                
        if validate_response == '1':
            if 'block_time' in request_data.keys() and request_data['block_time'] is not None and request_data['block_time'] != '': 
                try:
                    block_time = float(request_data['block_time'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato de la Duración de la cita no es correcto'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la duración de la cita'
                 
        if validate_response == '1':
            if 'timestamp' in request_data.keys() and request_data['timestamp'] is not None and request_data['timestamp'] != '': 
                try:
                    timestamp = float(request_data['timestamp'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato de la Duración de la cita no es correcto'
            else:
                validate_response = '0'
                error_msg = 'No se ha enviado la duración de la cita'
                
        if validate_response == '1':
            if external_system is not None:
                owner = external_system.owner
            else:
                validate_response = '0'
                error_msg = 'Sistema no registrado'
            
        data = {}

        if validate_response == '1':
            method_response, error_msg = Transaction.assign_appointment('assign/appointment/', external_system, owner, patient_number, speciality, medic_code, center_code, appointment_date_time, corr_time, block_time, timestamp)
            if method_response is None:
                validate_response = '0'
                error_msg = 'Se presento un error durante la validacion'
                
        if validate_response == '1':
            for key in method_response:
                data[key] = method_response[key]
            if method_response['PROCESS'] == 'FALSE':
                error_msg = method_response['MESSAGE']
                validate_response = '0'
        
        if validate_response == '0':
            data['ERROR'] = error_msg
            data['PROCESS'] = 'FALSE'
        if external_system is not None:
            data['RESPONSE_KEY'] = external_system.response_key
        else:
            data['RESPONSE_KEY'] = '' 
         
        if validate_response == '1':
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST

        return Response(data=data, status=response_status)

