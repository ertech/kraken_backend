from backend.auth.accounts.models import ExternalSystem
from backend.apps.operative.models import Transaction
import datetime

request =''

external_system = ExternalSystem.objects.all()[0] # PRODUCTION
external_system = ExternalSystem.objects.all()[1] # TEST

owner = external_system.owner 

# document_type_id, document = '1', '1053807720'
# document_type_id, document = '1', '31987031'
# document_type_id, document = '1', '1053817558'
# document_type_id, document = '1', '1053870487'
# document_type_id, document = '1', '30283836'
# document_type_id, document = '1', '1204377'
# document_type_id, document = '1', '24545253'
# document_type_id, document = '5', '27535329'
# document_type_id, document = '1', '16288417'
# document_type_id, document = '1', '7844089'
# document_type_id, document = '1', '19175507
document_type_id, document = '1', '51649874' #14729
# document_type_id, document = '1', '80063659'
document_type_id, document = '1', '41735125'    #27437
document_type_id, document = '2', '1140926792'    #1045011
document_type_id, document = '1', '123456'

process_response, error = Transaction.validate_person(request, external_system, owner, document_type_id, document, debug=True)

patient_number = 110121
today = datetime.datetime.strptime('20/03/16', '%d/%m/%y').date()


if 'PATIENT_NUMBER' in process_response.keys() and process_response['PATIENT_NUMBER'] is not None:
    patient_number = process_response['PATIENT_NUMBER']
    
process_response, error = Transaction.load_assigned_appointment(request, external_system, owner, patient_number, today, debug=True)

patient_number = 110121
service_code = 'ADO001'
medic_code = '2867618'
appointment_date_time = datetime.datetime(2004, 5, 7, 9, 40)
corr_time = 2
center_code = '077'
cancel_cause = '41'

process_response, error = Transaction.cancel_appointment(request, external_system, owner, patient_number, service_code, medic_code, appointment_date_time, corr_time, center_code, cancel_cause, debug=True)

process_response, error = Transaction.cancel_cause(request, external_system, owner, debug=True)

patient_number = 110121
service_code = 'EDO001'
medic_code = '63474726'
appointment_date_time = datetime.datetime(2016, 3, 29, 17, 40)
corr_time = 5
center_code = '174'

method_response, error_msg = Transaction.confirm_appointment('confirm/appointment/', external_system, owner, patient_number, service_code, medic_code, appointment_date_time, corr_time, debug=True)

speciality = 1
method_response, error_msg = Transaction.load_schedule(request, external_system, owner, speciality, debug=True)
speciality = 2

for speciality in range(1, 8):
    method_response, error_msg = Transaction.load_schedule(request, external_system, owner, speciality)
    print (speciality, method_response, error_msg)

patient_number = 27437
speciality = 1
medic_code = '1030549584'
appointment_date_time = datetime.datetime(2019, 6, 21, 16, 20)
corr_time = 29
center_code = '124'
block_time = 20
timestamp = 0

process_response, error = Transaction.assign_appointment(request, external_system, owner, patient_number, speciality, medic_code, center_code, appointment_date_time, corr_time, block_time, timestamp, debug=True)

{'CENTER_NAME': 'ESPHA HOSPITAL CENTRAL', 'CENTER_CODE': '124', 'DAY_NAME': 'Sabado', 'CORR_TIME': '29', 'SPECIALITY_ID': 1, 'DATE_VERBOSE': '21 de Junio de 2019', 
'BLOCK_TIME': '20', 'TIME': '16:20', 'MEDIC': 'ABRIL CLAVIJO LINA ALEJANDRA', 'DATE': '21/06/19', 'TIMESTAMP': '0', 'MEDIC_CODE': '1030549584'}

{'CENTER_NAME': 'USP NARIQO', 'CENTER_CODE': '323', 'DAY_NAME': 'Viernes', 'CORR_TIME': '1', 'SPECIALITY_ID': 1, 'DATE_VERBOSE': '27 de Junio de 2019', 
'BLOCK_TIME': '20', 'TIME': '07:20', 'MEDIC': 'MARTINEZ ORTEGA PAOLA ANDREA', 'DATE': '27/06/19', 'TIMESTAMP': '0', 'MEDIC_CODE': '59312882'}, 
6673737

