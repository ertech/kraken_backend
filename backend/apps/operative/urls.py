from backend.apps.operative import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

router = DefaultRouter()

urlpatterns = [
    url(r'', include(router.urls)), 
]

urlpatterns += format_suffix_patterns([
        url(r'^person/validate/$', views.validePersonViewSet.as_view()),
        url(r'^load/scheduledate/$', views.loadScheduleDateViewSet.as_view()),
        url(r'^load/assignedappointments/$', views.LoadAssignedAppointmentsViewSet.as_view()),
        url(r'^cancel/appointment/$', views.CancelAppointmentViewSet.as_view()),
        url(r'^cancel/cause/$', views.LoadCancelCauseViewSet.as_view()),
        url(r'^confirm/appointment/$', views.ConfirmAppointmentViewSet.as_view()),
        url(r'^assign/appointment/$', views.AssignAppointmentViewSet.as_view()),
])