# -*- coding: utf-8 -*-
from backend.auth.accounts.models import ExternalSystem
from backend.auth.accounts.serializers import ExternalSystemTableSerializer
from backend.core.views import GenericTableViewSet

class ExternalSystemTableViewSet(GenericTableViewSet):
    app_name = 'accounts'
    model_name = 'ExternalSystem'
    queryset = ExternalSystem.objects.all()
    serializer_class = ExternalSystemTableSerializer

    
