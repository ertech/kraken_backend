# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings

class ExternalSystem(models.Model):
    owner = models.ForeignKey('license.Owner')
    name = models.CharField(max_length=60, verbose_name='Nombre')
    auth_key = models.CharField(max_length=60, verbose_name='Llave de Autenticación',unique=True)
    response_key = models.CharField(max_length=60, verbose_name='Llave de Respuesta',blank=True)
    
    def generateKeys(self):
        import datetime
        import hashlib
        
        validate_response = '1'
        auth_key = ''
        response_key = ''
        
        is_correct = False
        while validate_response == '1' and not is_correct:
            try:
                base_str = 'KEY'
                base_str += settings.COMERCIAL_APP_NAME.lower()
                base_str += self.owner_id
                base_str += datetime.datetime.now().strftime('%Y%m%d%H%M%S')
                base_str += self.name.upper()
                base_str += 'ERTECH'
            except:
                validate_response = '0'
            if validate_response == '1':
                try:
                    auth_key = hashlib.new("md5", base_str.encode('utf-8')).hexdigest()
                    try:
                        ExternalSystem.objects.get(auth_key=auth_key)
                    except ExternalSystem.DoesNotExist:
                        response_key = hashlib.new("sha1", base_str.encode('utf-8')).hexdigest()
                        is_correct = True
                except:
                    validate_response = '0'
        
        if validate_response == '1':
            self.auth_key = auth_key
            self.response_key = response_key
        
        return validate_response
    
    def save(self, *args, **kwargs):
        if not self.auth_key or not self.response_key:
            self.generateKeys()
        super(ExternalSystem, self).save()
    
    def __str__(self):
        return u"%s" % (self.name)

    class Meta:
        unique_together = [['owner','name']]
        ordering = ['name']
        app_label = 'accounts'
        verbose_name = 'Sistema Externo'
        verbose_name_plural = 'Sistemas Externos'

    
