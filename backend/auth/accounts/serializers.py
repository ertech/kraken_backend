# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.auth.accounts.models import ExternalSystem
from backend.auth.license.models import ResourceHost

class ExternalSystemTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExternalSystem
        verbose_name = 'Sistemas Externos'
        fields = ("id", 'name', "auth_key", "response_key", )
        ordering_fields = ('name', )
        search_fields = ['$name', ]
        add_actions = [{ 'title': 'Cambiar Claves', 
                         'url': ResourceHost.getURL('BASE_URL') + '/accounts/setexternalkeys/',
                         "icon" : 'lock_outline',
                         'form_type': "ACTION"
                        }
                       ]
