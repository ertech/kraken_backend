# -*- coding: utf-8 -*-
from backend.auth.accounts import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

router = DefaultRouter()
router.register(r'externalsystemtable', views.ExternalSystemTableViewSet)

urlpatterns = [
    url(r'', include(router.urls)), 
]

urlpatterns += format_suffix_patterns([
])



