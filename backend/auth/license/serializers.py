# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.auth.license.models import ModuleInstalled, Owner

class ModuleInstalledSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ModuleInstalled
        fields = ("id", "name")

class OwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Owner
        fields = ("id", "name", "logo", "email", "address", "phone")
