from django.conf.urls import url, include
from backend.auth.license import views
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

router = DefaultRouter()
router.register(r'modulesinstalled', views.ModuleInstalledViewSet)

urlpatterns = [
    url(r'', include(router.urls)), 
]

urlpatterns += format_suffix_patterns([
])
