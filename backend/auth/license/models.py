# -*- coding: utf-8 -*-
from django.db import models
import os
from django.conf import settings
from backend.library.sendmail import send_mail

class Environment(models.Model):
    name = models.CharField(max_length=20,primary_key=True)
    is_active = models.BooleanField(default=False)
    
    def save(self, *args, **kwargs):
        if self.is_active:
            Environment.objects.filter(is_active=True).update(is_active=False)
        super(Environment, self).save()
    
    def getEnvironmentName():
        try:
            environment = Environment.objects.get(is_active=True)
            environment_name = environment.name
        except:
            environment_name = settings.CONF_ENVIRONMENT
        return environment_name
    
    getEnvironmentName = staticmethod(getEnvironmentName)
    
    def __str__(self):
        return self.name
    
    class Meta:
        app_label = 'license'
        verbose_name = 'Environment'
        verbose_name_plural = 'Environment'

class Protocol(models.Model):
    name = models.CharField(max_length=20,primary_key=True)
    
    def __str__(self):
        return self.name
    
    class Meta:
        app_label = 'license'
        verbose_name = 'Protocol'
        verbose_name_plural = 'Protocol'

class ResourceHost(models.Model):
    environment = models.ForeignKey('Environment', on_delete=models.PROTECT)
    name = models.CharField(max_length=30)
    host = models.CharField(max_length=30)
    port = models.IntegerField(default=80, null=True,blank=True)
    protocol = models.ForeignKey('Protocol', on_delete=models.PROTECT)

    def __str__(self):
        return self.name
    
    def getProtocol(name='BASE_URL'):
        environment_name = Environment.getEnvironmentName()

        try:
            resource_host = ResourceHost.objects.get(environment__name=environment_name, name=name)
        except ResourceHost.DoesNotExist:
            resource_host = None
        return resource_host.protocol.name
        
    getProtocol = staticmethod(getProtocol)
    
    def getURL(name='BASE_URL'):
        environment_name = Environment.getEnvironmentName()

        try:
            resource_host = ResourceHost.objects.get(environment__name=environment_name, name=name)
        except ResourceHost.DoesNotExist:
            resource_host = None
        result_url = ''
        if resource_host is not None:
            result_url += resource_host.protocol.name
            result_url += '://'
            result_url += resource_host.host
            result_url += ':'
            result_url += str(resource_host.port)
        else:
            result_url += settings.PRODUCTION_BASE_URL
        return result_url
    
    getURL = staticmethod(getURL)
    
    def getEnvironment(account=None):
        data = {}
        data['NAME'] = Environment.getEnvironmentName()
        data['HOME'] = ResourceHost.getURL('BASE_URL') + '/'
        data['HEALTH'] = ResourceHost.getURL('BASE_URL') + '/health/'
        data['LOGIN'] =  data['HOME'] + 'accounts/login/'
        if account is not None:
            data['PROFILE'] =  data['HOME'] + 'accounts/profile/'
            data['CHANGEPASSWORD'] =  data['HOME'] + 'accounts/changepassword/'
            data['DASHBOARD'] =  data['HOME'] + 'dashboard/query/'
            data['MENU'] =  data['HOME'] + 'menu/query/'
            data['TOKENREFRESH'] =  data['HOME'] + 'accounts/tokenrefresh/'
            data['TOKENVERIFY'] = data['HOME'] + 'accounts/tokenverify/'
            data['REGISTERSESSION'] =  data['HOME'] + 'accounts/registersession/' 
            data['PUSHREGISTER'] = data['HOME'] + 'pushmessages/register/'
            data['PUSHUNREGISTER'] = data['HOME'] + 'pushmessages/unregister/'
            data['GLOBALFILTERLIST'] = None

            data['ENABLE_TOOLBAR_COMPONENTS'] = []
            for toolbar_component in settings.TOOLBAR_COMPONENTS:
                enable_component = True
                if enable_component:
                    data['ENABLE_TOOLBAR_COMPONENTS'].append(toolbar_component)

        return data
    
    getEnvironment = staticmethod(getEnvironment)
    
    class Meta:
        unique_together = [['environment', 'name']]
        app_label = 'license'
        verbose_name = 'Resource Host'
        verbose_name_plural = 'Resources Hosts'

class ModuleInstalled(models.Model):
    name = models.CharField(max_length=20,unique=True)

    def __str__(self):
        return self.name
    
    class Meta:
        app_label = 'license'
        verbose_name = 'Module Installed'
        verbose_name_plural = 'Modules Installed'

def upload_logo_to(instance, filename):
    from django.utils.timezone import now
    filename_tuple = os.path.splitext(filename)
    return 'license/%d_%s%s' % (
        instance.id,
        now().strftime("%Y%m%d%H%M%S"),
        filename_tuple[1].lower(),
    )

class Owner(models.Model):
    id = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=60, verbose_name='Nombre')
    logo = models.ImageField(null=True, upload_to=upload_logo_to, blank=True)
    email = models.EmailField(max_length=60, null=True, blank=True, verbose_name='Email')
    admin_email = models.EmailField(max_length=60, null=True, blank=True, verbose_name='Email Administrador')
    phone = models.CharField(max_length=60, null=True, blank=True, verbose_name='Teléfono',help_text='large|only_number')
    address = models.TextField(null=True,blank=True, verbose_name='Dirección')
    city = models.ForeignKey('localization.City',null=True,blank=True,verbose_name='Ciudad', on_delete=models.SET_NULL)

    def __str__(self):
        return u"%s" % (self.name)

    def sendLogEmail(self, filename, process, date):
        import socket
        email_dict = {}
        try:
            email_dict['REPLY_TO'] = settings.ADMINS[0]
        except:
            pass
        email_dict['TO_EMAIL'] = []
        for admin in settings.ADMINS:
            email_dict['TO_EMAIL'].append(admin[0] + '<' + admin[1] + '>')
        if self.admin_email is not None:
            email_dict['TO_EMAIL'].append(self.id + '<' + self.admin_email + '>')
    
        email_dict['SUBJECT'] = settings.COMERCIAL_APP_NAME.replace(' ', '_') 
        email_dict['SUBJECT'] += " ENV: " + Environment.getEnvironmentName()
        email_dict['SUBJECT'] += " " + process + " " + self.id
        
        email_dict['VERBOSE_TITLE'] = 'Reporte de Proceso %s del %s Entorno %s' % (process, date.strftime('%Y-%m-%d'), Environment.getEnvironmentName()) 
        
        email_dict['DATA'] = {}
        mailfile = open(filename, "r")
        email_dict['DATA']['BODY'] = str(date) + '<br />'
        
        lines = mailfile.readlines()
        for line in lines:
            email_dict['DATA']['BODY'] += line
            email_dict['DATA']['BODY'] += '<br />'
        mailfile.close()
        
        email_sent = send_mail(email_dict)
        if not email_sent:
            print  ( 'No se pudo enviar el correo' )

    def getPoweredby(owner=None):
        poweredby = "Producto fabricado por ER Technology LTDA "
        if owner is not None:
            poweredby += ' para '
            poweredby += owner.name.upper()        
        return poweredby 
        
    getPoweredby = staticmethod(getPoweredby)
    
    class Meta:
        ordering = ['name']
        app_label = 'license'
        verbose_name = 'Propietario'
        verbose_name_plural = 'Propietarios'
