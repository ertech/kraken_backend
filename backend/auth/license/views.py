# -*- coding: utf-8 -*-
from django.shortcuts import render

from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from backend.auth.license.models import ModuleInstalled, Owner
from backend.auth.license.serializers import ModuleInstalledSerializer
from rest_framework import viewsets
from backend.core.permissions import IsAdminAndReadOnly

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

class ModuleInstalledViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ModuleInstalled.objects.all()
    serializer_class = ModuleInstalledSerializer
    permission_classes = (IsAdminAndReadOnly, )

                
