# -*- encoding: utf-8 -*-

from django import http
from django.template.loader import get_template
from django.template import Context
from django.conf import settings
import xhtml2pdf.pisa as pisa
import cStringIO as StringIO
import cgi
import os

#pisa.showLogging(debug=True) 

def downloadAsPdf(data, filename='document.pdf'):
    result = StringIO.StringIO()
    pdf = pisa.CreatePDF(StringIO.StringIO(data), result)

    if not pdf.err:
        response = http.HttpResponse(result.getvalue(),
                                     mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=' + filename
        return response
    return http.HttpResponse('Some errors produced while generating the pdf file.')

def renderToPdf(template_src, context_dict, filename='document.pdf',path=None):
    template = get_template(template_src)
    context = Context(context_dict)
    html  = template.render(context)
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(html, result)
    if not pdf.err:
        if path is not None:
            filepath = path + filename
            ft = open(filepath,'wb')
            ft.write(result.getvalue())
            ft.close()
        response = http.HttpResponse(result.getvalue(), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=' + filename
        return response
    return http.HttpResponse('We had some errors %s <pre>%s</pre>' % (pdf.err, cgi.escape(html)))


def createPdf(template_src, context_dict, filename):
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    # open output file for writing (truncated binary)
    resultFile = open(filename, "w+b")
    # convert HTML to PDF
    pisaStatus = pisa.CreatePDF(html,  # the HTML to convert
                                dest=resultFile)  # file handle to recieve result
    resultFile.close()  # close output file
    # return True on success and False on errors
    return pisaStatus.err


def htmlcreatePdf(template_html,filename,context=None,tmp_filename='tmp_document.html'):
    tmp_file = 'tmp/' + tmp_filename
    tmp_file_path = settings.MY_TEMPLATES_TMP_PATH + '/' + tmp_filename
 
    string = 'mkdir -p '
    string += settings.MY_TEMPLATES_TMP_PATH + '/tmp'
    os.system(string)
 
    file = open(tmp_file_path,'w')
    file.write(template_html)
    file.close()
    template = get_template(tmp_file)
    
    if context is None:
        context = {}
    if 'pagesize' not in context.keys():
        context['pagesize'] = 'Letter'
    
    context = Context(context)
    html  = template.render(context)
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(
        html.encode("UTF-8")), result)
    #pdf = pisa.pisaDocument(template_html, result)
    ft = open(filename,'wb')
    ft.write(result.getvalue())
    ft.close()
