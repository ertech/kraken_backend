from backend.library.datemanager import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

router = DefaultRouter()
router.register(r'monthnames', views.MonthNameViewSet)
router.register(r'daynames', views.DayNameViewSet)
router.register(r'holidays', views.HolidayViewSet)
 
urlpatterns = [
    url(r'', include(router.urls)), 
]
urlpatterns += format_suffix_patterns([
            url(r'^isbusinessday/(?P<date_text>\d{4}-\d{2}-\d{2})/$', views.DateIsBusinessDayView.as_view()),
            url(r'^nextbusinessday/(?P<date_text>\d{4}-\d{2}-\d{2})/$', views.NextBusinessDayView.as_view()),
            url(r'^previousbusinessday/(?P<date_text>\d{4}-\d{2}-\d{2})/$', views.PreviousBusinessDayView.as_view()),
            url(r'^addbusinessday/(?P<date_text>\d{4}-\d{2}-\d{2})/(?P<days>\d+)/$', views.AddBusinessDayView.as_view()),
            url(r'^subtractbusinessday/(?P<date_text>\d{4}-\d{2}-\d{2})/(?P<days>\d+)/$', views.SubtractBusinessDayView.as_view()),
            url(r'^monthlastdate/(?P<date_text>\d{4}-\d{2}-\d{2})/$', views.MonthLastDateView.as_view()),
            url(r'^monthlastdatenumber/(?P<year>\d+)/(?P<month>\d+)/$', views.MonthLastDateNumberView.as_view()),
            url(r'^daterange/(?P<from_date_text>\d{4}-\d{2}-\d{2})/(?P<to_date_text>\d{4}-\d{2}-\d{2})/$', views.DateRangeView.as_view()),
            url(r'^getholidaylist/(?P<from_date_text>\d{4}-\d{2}-\d{2})/(?P<to_date_text>\d{4}-\d{2}-\d{2})/$', views.getHolidayListView.as_view()),
            url(r'^translatemonthname/(?P<month_text>\w+)/$', views.TranslateMonthNameView.as_view()),
            url(r'^verbosemonth/(?P<month_number>\d+)/$', views.VerboseMonthView.as_view()),
            url(r'^verboseday/(?P<date_text>\d{4}-\d{2}-\d{2})/$', views.VerboseDayView.as_view()),
            url(r'^verbosenumberday/(?P<day_number>[0-6])/$', views.VerboseNumberDayView.as_view()),
            url(r'^getmountlist/$', views.MountListView.as_view()),
            url(r'^calendarmatrix/(?P<year>\d+)/(?P<month>\d+)/$', views.CalendarMatrixView.as_view()),
])

