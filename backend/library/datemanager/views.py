# -*- coding: utf-8 -*-
 
from rest_framework import viewsets
from backend.library.datemanager.models import MonthName, DayName, Holiday
from backend.library.datemanager.serializers import MonthNameSerializer, DayNameSerializer, HolidaySerializer
from rest_framework.response import Response
from rest_framework.views import APIView
import datetime
from backend.core.permissions import IsAdminOrReadOnly
 
class MonthNameViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MonthName.objects.all()
    serializer_class = MonthNameSerializer
    permission_classes = (IsAdminOrReadOnly,)
 
class DayNameViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = DayName.objects.all()
    serializer_class = DayNameSerializer
    permission_classes = (IsAdminOrReadOnly,)
 
class HolidayViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Holiday.objects.all()
    serializer_class = HolidaySerializer
    permission_classes = (IsAdminOrReadOnly,)

class DateIsBusinessDayView(APIView):
    def get(self, request, date_text, format=None):
        saturday_too = False
        if 'saturday_too' in request.GET and request.GET['saturday_too'].upper() == 'TRUE':
            saturday_too = True
        date = datetime.datetime.strptime(date_text, '%Y-%m-%d').date()
        result = Holiday.is_business_day(date, saturday_too)
        return Response(result)

class NextBusinessDayView(APIView):
    def get(self, request, date_text, format=None):
        saturday_too = False
        if 'saturday_too' in request.GET and request.GET['saturday_too'].upper() == 'TRUE':
            saturday_too = True
        date = datetime.datetime.strptime(date_text, '%Y-%m-%d').date()
        result = Holiday.next_business_day(date, saturday_too)
        return Response(result)

class getHolidayListView(APIView):
    def get(self, request, from_date_text, to_date_text, sunday_like_holiday=True, format=None):
        from_date = datetime.datetime.strptime(from_date_text, '%Y-%m-%d').date()
        to_date = datetime.datetime.strptime(to_date_text, '%Y-%m-%d').date()
        result = Holiday.get_holidays(from_date, to_date)
        result['sundays'] = Holiday.get_sundays(from_date, to_date, getdict=False)
        return Response(result)

class PreviousBusinessDayView(APIView):
    def get(self, request, date_text, format=None):
        saturday_too = False
        if 'saturday_too' in request.GET and request.GET['saturday_too'].upper() == 'TRUE':
            saturday_too = True
        date = datetime.datetime.strptime(date_text, '%Y-%m-%d').date()
        result = Holiday.previous_business_day(date, saturday_too)
        return Response(result)

class AddBusinessDayView(APIView):
    def get(self, request, date_text, days, format=None):
        saturday_too = False
        if 'saturday_too' in request.GET and request.GET['saturday_too'].upper() == 'TRUE':
            saturday_too = True
        date = datetime.datetime.strptime(date_text, '%Y-%m-%d').date()
        result = Holiday.add_business_day(date, int(days), saturday_too)
        return Response(result)

class SubtractBusinessDayView(APIView):
    def get(self, request, date_text, days, format=None):
        saturday_too = False
        if 'saturday_too' in request.GET and request.GET['saturday_too'].upper() == 'TRUE':
            saturday_too = True
        date = datetime.datetime.strptime(date_text, '%Y-%m-%d').date()
        result = Holiday.substract_business_day(date, int(days), saturday_too)
        return Response(result)

class MonthLastDateView(APIView):
    def get(self, request, date_text, format=None):
        date = datetime.datetime.strptime(date_text, '%Y-%m-%d').date()
        result = Holiday.month_last_date(date)
        return Response(result)

class MonthLastDateNumberView(APIView):
    def get(self, request, year, month, format=None):
        result = Holiday.month_last_date_number(int(month), int(year))
        return Response(result)

class DateRangeView(APIView):
    def get(self, request, from_date_text, to_date_text, format=None):
        from_date = datetime.datetime.strptime(from_date_text, '%Y-%m-%d').date()
        to_date = datetime.datetime.strptime(to_date_text, '%Y-%m-%d').date()
        result = Holiday.dateRange(from_date, to_date)
        return Response(result)

class TranslateMonthNameView(APIView):
    def get(self, request, month_text, format=None):
        result = Holiday.translate_text_name(month_text)
        return Response(result)

class VerboseMonthView(APIView):
    def get(self, request, month_number, format=None):
        result = Holiday.verbose_month(int(month_number))
        return Response(result)

class VerboseDayView(APIView):
    def get(self, request, date_text, format=None):
        date = datetime.datetime.strptime(date_text, '%Y-%m-%d').date()
        result = Holiday.verbose_day(date)
        return Response(result)

class VerboseNumberDayView(APIView):
    def get(self, request, day_number, format=None):
        result = Holiday.verbose_number_day(int(day_number))
        return Response(result)

class MountListView(APIView):
    def get(self, request, format=None):
        result = Holiday.getMountList()
        return Response(result)

class CalendarMatrixView(APIView):
    def get(self, request, year, month, format=None):
        result = Holiday.CalendarMatrix(int(month), int(year))
        return Response(result)


