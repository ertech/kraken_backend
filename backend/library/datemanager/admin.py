from django.contrib import admin
from backend.library.datemanager.models import MonthName, DayName, Holiday
 
class MonthNameAdmin(admin.ModelAdmin):
    list_display_links = ['name']
    list_display = ['id', 'name']
    search_fields = ['id', 'name']
 
admin.site.register(MonthName, MonthNameAdmin)
 
class DayNameAdmin(admin.ModelAdmin):
    list_display_links = ['name']
    list_display = ['id', 'name']
    search_fields = ['id', 'name']
 
admin.site.register(DayName, DayNameAdmin)
 
class HolidayAdmin(admin.ModelAdmin):
    list_display_links = ['name']
    list_display = ['day', 'name']
    list_filter = ['day']
    search_fields = ['day', 'name']
 
admin.site.register(Holiday, HolidayAdmin)
