# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.library.datemanager.models import MonthName, DayName, Holiday
 
class MonthNameSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = MonthName
        fields = ("id", "name")
 
class DayNameSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = DayName
        fields = ("id", "name")
 
class HolidaySerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Holiday
        fields = ("day", "name")
