from email.mime.image import MIMEImage
from email.mime.base import MIMEBase
from email import encoders
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.conf import settings

def send_mail(email_dict):
    template_file = 'email_template.html'
    if 'TEMPLATE' in email_dict.keys():
        template_file = email_dict['TEMPLATE']

    text_template_file = 'email_template.html'
    if 'TEXT_TEMPLATE' in email_dict.keys():
        text_template_file = email_dict['TEXT_TEMPLATE']

    plaintext = get_template(text_template_file)
    htmly = get_template(template_file)

    send_from = settings.DEFAULT_FROM_EMAIL

    if 'FROM_EMAIL' in email_dict.keys():
        send_from = email_dict['FROM_EMAIL']

    reply_to = None
    if 'REPLY_TO' in email_dict.keys():
        reply_to = email_dict['REPLY_TO']
    
    send_to_list = []
    if 'TO_EMAIL' in email_dict.keys():
        send_to_list = email_dict['TO_EMAIL']

    cc_send_to_list = []
    if 'CC_EMAIL' in email_dict.keys():
        cc_send_to_list = email_dict['CC_EMAIL']

    bcc_send_to_list = []
    if 'BCC_EMAIL' in email_dict.keys():
        bcc_send_to_list = email_dict['BCC_EMAIL']

    subject = None
    if 'SUBJECT' in email_dict.keys():
        subject = email_dict['SUBJECT']

    data_dict = {}
    data_dict['verbose_title'] = ''
    if 'VERBOSE_TITLE' in email_dict.keys():
        data_dict['verbose_title'] = email_dict['VERBOSE_TITLE']

    if 'DATA' in email_dict.keys():
        for key in email_dict['DATA']:
            data_dict[key] = email_dict['DATA'][key]

    attach_filename_list = []
    if 'FILES' in email_dict.keys():
        for file in email_dict['FILES']:
            attach_filename_list.append(file)
    
    text_content = plaintext.render(data_dict)
    html_content = htmly.render(data_dict)

    email_message = EmailMultiAlternatives(subject=subject, body=text_content, from_email=send_from, to=send_to_list, bcc=bcc_send_to_list, cc=cc_send_to_list, reply_to=reply_to)
    email_message.attach_alternative(html_content, "text/html")

    for attach_filepath in attach_filename_list:
        try:
            filename = attach_filepath.split('/')[-1]
            fp = open(attach_filepath, "rb")

            part = MIMEBase('application', "octet-stream")
            part.set_payload( fp.read() )
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % filename )
            email_message.attach(part)

            fp.close()
        except:
            pass

    if 'BACKGROUND' in email_dict.keys():
        try:
            attach_filepath = settings.MEDIA_ROOT + email_dict['BACKGROUND']
            filename = attach_filepath.split('/')[-1]
            fp = open(attach_filepath, "rb")
            attach_image = MIMEImage(fp.read())
            attach_image.add_header('Content-ID', '<email_background>')
            attach_image.add_header('Content-Disposition', 'attachment; filename = "%s"' % filename)
            email_message.attach(attach_image)
        except:
            pass

    email_message.content_subtype = "html"
    try:
        email_message.send()
        result = True
    except Exception as e:
        error = u'Error al enviar el email: %s' % str(e) 
        print (error)
        result = False
    
    return result

