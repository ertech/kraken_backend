# -*- coding: utf-8 -*-

import os
import tempfile

def handle_uploaded_file_tmp(f):
    destination = tempfile.TemporaryFile("ar+b")
    for chunk in f.chunks():
        destination.write(chunk)
    #destination.flush()
    destination.seek(0)
    return f.name, destination 
