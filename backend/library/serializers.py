from rest_framework import serializers

class SerializableGeoLocationField(serializers.Serializer):
    def __init__(self, **kwargs):
        super(SerializableGeoLocationField, self).__init__(**kwargs)

    def to_representation(self, value):
        if value in ('', None):
            return ''
        data_dict = {}
        data_dict['lat'] = value.lat
        data_dict['lon'] = value.lon
        return data_dict
