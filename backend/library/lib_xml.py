# -*- coding: utf-8 -*-

from xml.dom import minidom

def xmlToDictList(items_xml,item_name='item'):
    dict_list = []
    doc = minidom.parseString(items_xml.encode( "utf-8" ) )
    root_element = doc.documentElement
    tag_element_list = root_element.childNodes
    for tag_element in tag_element_list:
        dict_list.append(createDict(tag_element,item_name))
    return dict_list

def createDict(item_node,item_name='item'):
    item_dict = {}
    node_child_list = []
    tag_children_list = item_node.childNodes
    for tag_element in tag_children_list:
        if tag_element.nodeName == item_name:
            node_child_list.append(tag_element)
        else:
            if tag_element.firstChild is not None:
                item_dict[tag_element.nodeName] = tag_element.firstChild.data
    return item_dict