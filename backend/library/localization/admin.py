from backend.library.localization.models import City
from backend.library.localization.models import State
from backend.library.localization.models import Country
from django.contrib import admin

class CountryAdmin(admin.ModelAdmin):
    list_display_links = ['name']
    list_display = ['id', 'name']
    search_fields = ['id', 'name']

admin.site.register(Country, CountryAdmin)

class StateAdmin(admin.ModelAdmin):
    list_display_links = ['name']
    list_display = ['id', 'name','country','region']
    list_filter = ['country','region']
    search_fields = ['id', 'name']

admin.site.register(State, StateAdmin)

class CityAdmin(admin.ModelAdmin):
    list_display_links = ['name']
    list_display = ['id', 'name','state']
    list_filter = ['state']
    search_fields = ['id', 'name']

admin.site.register(City, CityAdmin)

