from backend.library.localization import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include

router = DefaultRouter()
router.register(r'country', views.CountryViewSet)
router.register(r'countrytable', views.CountryTableViewSet)
router.register(r'countryquery', views.CountryQueryViewSet)
router.register(r'region', views.RegionViewSet)
router.register(r'regiontable', views.RegionTableViewSet)
router.register(r'regionquery', views.RegionQueryViewSet)
router.register(r'state', views.StateViewSet)
router.register(r'statetable', views.StateTableViewSet)
router.register(r'statequery', views.StateQueryViewSet)
router.register(r'city', views.CityViewSet)
router.register(r'citytable', views.CityTableViewSet)
router.register(r'cityquery', views.CityQueryViewSet)

urlpatterns = [
    url(r'', include(router.urls)), 
]
