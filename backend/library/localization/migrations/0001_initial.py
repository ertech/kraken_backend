# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-03-08 15:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.CharField(help_text='small', max_length=10, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, help_text='large', max_length=60, verbose_name='Nombre')),
                ('is_state_capital', models.NullBooleanField(verbose_name='Es Capital de departamento)')),
                ('is_country_capital', models.NullBooleanField(verbose_name='Es Capital de departamento)')),
            ],
            options={
                'verbose_name': 'Ciudad',
                'verbose_name_plural': 'Ciudades',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.CharField(help_text='small', max_length=5, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, help_text='large', max_length=60, verbose_name='Nombre')),
            ],
            options={
                'verbose_name': 'País',
                'verbose_name_plural': 'Paises',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.CharField(help_text='small', max_length=10, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, help_text='large', max_length=60, verbose_name='Nombre')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='localization.Country', verbose_name='País')),
            ],
            options={
                'verbose_name': 'Región',
                'verbose_name_plural': 'Regiones',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.CharField(help_text='small', max_length=10, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, help_text='large', max_length=60, verbose_name='Nombre')),
                ('country', models.ForeignKey(help_text='large', on_delete=django.db.models.deletion.PROTECT, to='localization.Country', verbose_name='País')),
                ('region', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='localization.Region', verbose_name='Región')),
            ],
            options={
                'verbose_name': 'Departamento',
                'verbose_name_plural': 'Departamentos',
                'ordering': ('name',),
            },
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(help_text='large', on_delete=django.db.models.deletion.PROTECT, to='localization.Country', verbose_name='País'),
        ),
        migrations.AddField(
            model_name='city',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='localization.Region', verbose_name='Región'),
        ),
        migrations.AddField(
            model_name='city',
            name='state',
            field=models.ForeignKey(blank=True, help_text='large', null=True, on_delete=django.db.models.deletion.SET_NULL, to='localization.State', verbose_name='Departamento'),
        ),
    ]
