# -*- coding: utf-8 -*-
from rest_framework import viewsets
from backend.library.localization.models import Country, Region, State, City
from backend.library.localization.serializers import CountrySerializer, RegionSerializer, \
                                             StateSerializer, CitySerializer,\
    StateTableSerializer, CityTableSerializer, StateQuerySerializer,\
    CityQuerySerializer, CountryTableSerializer, CountryQuerySerializer,\
    RegionQuerySerializer, RegionTableSerializer
from backend.core.permissions import IsAdminOrReadOnly, IsAdminAndReadOnly
from backend.library.localization.filters import StateTableFilter,\
    CityTableFilter, CountryTableFilter, RegionTableFilter
from backend.core.views import GenericQuerynoOwnerViewSet,\
    GenericModelnoOwnerViewSet, GenericTablenoOwnerViewSet
from rest_framework.permissions import IsAuthenticated

class CountryTableViewSet(GenericTablenoOwnerViewSet):
    app_name = 'operative'
    model_name = 'Country'
    queryset = Country.objects.all()
    serializer_class = CountryTableSerializer
    filter_class = CountryTableFilter
    permission_classes = (IsAdminAndReadOnly,)

class CountryQueryViewSet(GenericQuerynoOwnerViewSet):
    app_name = 'operative'
    model_name = 'Country'
    queryset = Country.objects.all()
    serializer_class = CountryQuerySerializer
    serializer_class = CountryTableSerializer
    permission_classes = (IsAdminAndReadOnly,)

class CountryViewSet(GenericModelnoOwnerViewSet):
    app_name = 'operative'
    model_name = 'Country'
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = (IsAuthenticated, IsAdminOrReadOnly,)

class RegionTableViewSet(GenericTablenoOwnerViewSet):
    app_name = 'operative'
    model_name = 'Region'
    queryset = Region.objects.all()
    serializer_class = RegionTableSerializer
    filter_class = RegionTableFilter
    permission_classes = (IsAdminAndReadOnly,)

class RegionQueryViewSet(GenericQuerynoOwnerViewSet):
    app_name = 'operative'
    model_name = 'Region'
    queryset = Region.objects.all()
    serializer_class = RegionQuerySerializer
    serializer_class = RegionTableSerializer
    permission_classes = (IsAdminAndReadOnly,)

class RegionViewSet(GenericModelnoOwnerViewSet):
    app_name = 'operative'
    model_name = 'Region'
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    permission_classes = (IsAuthenticated, IsAdminOrReadOnly,)

class StateTableViewSet(GenericTablenoOwnerViewSet):
    app_name = 'operative'
    model_name = 'State'
    queryset = State.objects.all()
    serializer_class = StateTableSerializer
    filter_class = StateTableFilter
    permission_classes = (IsAdminAndReadOnly,)

class StateQueryViewSet(GenericQuerynoOwnerViewSet):
    app_name = 'operative'
    model_name = 'State'
    queryset = State.objects.all()
    serializer_class = StateQuerySerializer
    permission_classes = (IsAdminAndReadOnly,)

class StateViewSet(GenericModelnoOwnerViewSet):
    app_name = 'operative'
    model_name = 'State'
    queryset = State.objects.all()
    serializer_class = StateSerializer
    permission_classes = (IsAuthenticated, IsAdminOrReadOnly,)

class CityTableViewSet(GenericTablenoOwnerViewSet):
    app_name = 'operative'
    model_name = 'City'
    queryset = City.objects.all()
    serializer_class = CityTableSerializer
    filter_class = CityTableFilter
    permission_classes = (IsAdminAndReadOnly,)

class CityQueryViewSet(GenericQuerynoOwnerViewSet):
    app_name = 'operative'
    model_name = 'City'
    queryset = City.objects.all()
    serializer_class = CityQuerySerializer
    serializer_class = CityTableSerializer
    permission_classes = (IsAdminAndReadOnly,)

class CityViewSet(GenericModelnoOwnerViewSet):
    app_name = 'operative'
    model_name = 'City'
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = (IsAuthenticated, IsAdminOrReadOnly,)

