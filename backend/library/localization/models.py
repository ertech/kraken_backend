# -*- coding: utf-8 -*-
from django.db import models

class Country(models.Model):
    id = models.CharField(max_length=5, primary_key=True, verbose_name='ID', help_text='small')
    name = models.CharField(max_length=60, db_index=True, verbose_name='Nombre', help_text='large')
    
    def __str__(self):
        return u"%s" % (self.name)

    class Meta:
        app_label = 'localization'
        ordering = ('name',)
        verbose_name = 'País'
        verbose_name_plural = 'Paises'

class Region(models.Model):
    country = models.ForeignKey('Country', db_index=True, verbose_name='País', on_delete=models.PROTECT)
    id = models.CharField(max_length=10, primary_key=True, verbose_name='ID', help_text='small')
    name = models.CharField(max_length=60, db_index=True, verbose_name='Nombre', help_text='large')

    def __str__(self):
        return u"%s" % self.name

    class Meta:
        app_label = 'localization'
        ordering = ('name',)
        verbose_name = 'Región'
        verbose_name_plural = 'Regiones'


class State(models.Model):
    id = models.CharField(max_length=10, primary_key=True, verbose_name='ID', help_text='small')
    country = models.ForeignKey('Country', db_index=True, verbose_name='País', help_text='large', on_delete=models.PROTECT)
    region = models.ForeignKey('Region', null=True, blank=True, db_index=True, verbose_name='Región', on_delete=models.SET_NULL)
    name = models.CharField(max_length=60, db_index=True, verbose_name='Nombre', help_text='large')

    def __str__(self):
        return u"%s" % self.name

    class Meta:
        app_label = 'localization'
        ordering = ('name',)
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'


class City(models.Model):
    id = models.CharField(max_length=10, primary_key=True, verbose_name='ID', help_text='small')
    country = models.ForeignKey('Country', db_index=True, verbose_name='País', help_text='large', on_delete=models.PROTECT)
    region = models.ForeignKey('Region', null=True, blank=True, db_index=True, verbose_name='Región', on_delete=models.SET_NULL)
    state = models.ForeignKey('State', db_index=True, null=True, blank=True, verbose_name='Departamento', help_text='large', on_delete=models.SET_NULL)
    name = models.CharField(max_length=60, db_index=True, verbose_name='Nombre', help_text='large')
    is_state_capital = models.NullBooleanField(verbose_name='Es Capital de departamento)')
    is_country_capital = models.NullBooleanField(verbose_name='Es Capital de departamento)')

    def __str__(self):
        return "%s" % self.name

    class Meta:
        app_label = 'localization'
        ordering = ('name',)
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'
