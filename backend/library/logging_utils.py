# -*- coding:utf-8 -*-
import os
import logging 
from io import StringIO

def getLoggingPath (owner, component_name, model_name, process_name, objectid):
    logging_filename = 'log_%s_%s_%s_%s.txt' % ( owner.id, model_name.lower(), process_name.lower(), str(objectid) ) 
    
    format_desc='%(asctime)s %(levelname)s %(message)s'
    
    log_stream = StringIO()    
    logging.basicConfig(stream=log_stream, 
                        level=logging.DEBUG,
                        format=format_desc
                        )
    
    logger = logging.getLogger('%s_%s_%s_%s' % (component_name.upper(), model_name.lower(), process_name, str(objectid)))
    return logger, log_stream, logging_filename


    
