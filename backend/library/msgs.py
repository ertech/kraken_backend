# -*- coding: utf-8 -*-

from xml.dom import minidom
from django.forms.models import model_to_dict
from odisea.library import utils
import datetime

xhrOk = 'success'
xhrErr = 'error'
xhrNew = 'new'
xhrUpdate = 'updated'
xhrNotExist = 'not_found'
xhrTypeUnk = 'data_type_unknown'
xhrProtected = 'deletion_not_allowed'

def createSingleMsgXML(item_name, id, status, msg=None):
    doc = minidom.Document()
    root_node = doc.createElement(item_name)
    if id and id != '':
        id_node = doc.createElement("id")
        id_node.appendChild(doc.createTextNode(id))
        root_node.appendChild(id_node)
    if msg and msg != '':
        msg_node = doc.createElement("msg")
        msg_node.appendChild(doc.createTextNode(msg))
        root_node.appendChild(msg_node)        
    root_node.setAttribute('status', status)
    doc.appendChild(root_node)
    return doc

def createDocFromDic(item_name, dic, status):
    doc = minidom.Document()
    root_node = doc.createElement(item_name)
    for key in dic.keys():
        node = doc.createElement(key)
        if type(dic[key]) == unicode:
            value = dic[key]
        elif type(dic[key]) == datetime.datetime:
            value = dic[key].strftime("%Y-%m-%d %H:%M")
        elif dic[key] == None:
            value = ""
        else:
            value = str(dic[key])
        node.appendChild(doc.createTextNode(value))
        root_node.appendChild(node)
    root_node.setAttribute('status', status)
    doc.appendChild(root_node)
    return doc
    
def createJGridDocFromRawQuerySet(root_node, objects, fields, exclude=[]):
    doc = minidom.Document()
    for item in objects:
        dic = model_to_dict(item, fields, exclude)
        item_node = doc.createElement("row")
        item_node.setAttribute('id', str(dic["id"]))
        for key in fields:
            node = doc.createElement("cell")
            if type(dic[key]) == unicode:
                value = dic[key]
            elif dic[key] == None:
                value = ""         
            else:
                value = str(dic[key])
            node.appendChild(doc.createTextNode(value))
            item_node.appendChild(node)
        root_node.appendChild(item_node)
    return root_node

def createJgridDocFromDic(root_node, objects, keys):
    doc = minidom.Document()
    for dic in objects:
        item_node = doc.createElement("row")
        item_node.setAttribute('id', str(dic["id"]))
        for key in keys:
            node = doc.createElement("cell")
            if type(dic[key]) == unicode:
                value = dic[key]
            elif dic[key] == None:
                value = ""         
            else:
                value = str(dic[key])
            node.appendChild(doc.createTextNode(value))
            item_node.appendChild(node)
        root_node.appendChild(item_node)
    return root_node
    
def createDocFromValuesQuerySet(item_name, values_query_set, status):
    list_name = utils.pluralize(item_name)
    doc = minidom.Document()
    root_node = doc.createElement(list_name)
    for dic in values_query_set:
        item_node = doc.createElement(item_name)
        for key in dic.keys():
            node = doc.createElement(key)
            if type(dic[key]) == unicode:
                value = dic[key]
            elif dic[key] == None:
                value = ""         
            else:
                value = str(dic[key])
            node.appendChild(doc.createTextNode(value))
            item_node.appendChild(node)
        root_node.appendChild(item_node)
    root_node.setAttribute('status', status)
    doc.appendChild(root_node)
    return doc


def check_existence(object_list, object_id):
    if object_list.count() == 0:
        msg = xhrOk
    else:
        if object_id:
            object_list = object_list.filter(pk=object_id)
            if object_list.count() == 0:
                msg = xhrErr
            else:
                msg = xhrOk
        else:
            msg = xhrErr
    return msg
