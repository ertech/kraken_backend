# -*- coding: utf-8 -*-
import argparse
from swaggerpython.client import SwaggerClient
from swaggerpython.http_client import SynchronousHttpClient

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('endpoint', type=str, help='Endpoint, e.g. SIP/operator/123456789')
    parser.add_argument('callerid', type=str, help='CallerID, e.g. 111111')
    parser.add_argument('context', type=str, help='Asterisk context to connect call, e.g. default')
    parser.add_argument('extension', type=str, help='Context\'s extension, e.g. s')
    parser.add_argument('priority', type=str, help='Context\'s priority, e.g. 1')
    parser.add_argument('timeout', type=int, help='Originate timeout, e.g. 60')
    return parser.parse_args()

def process_call(ari_url, ari_host, ari_user, ari_password, args_dict):
    http_client = SynchronousHttpClient()
    http_client.set_basic_auth(ari_host, ari_user, ari_password)
    client_ari_url = ari_url + '/api-docs/resources.json'
    ari = SwaggerClient(
                        client_ari_url,
                        http_client=http_client)

    ari.channels.originate(endpoint=args_dict['endpoint'], callerId=args_dict['callerid'],
                        context=args_dict['context'], extension=args_dict['extension'],
                        priority=args_dict['priority'], timeout=args_dict['timeout'], variables=args_dict['variables']
            )
if __name__ == '__main__':
    ARI_URL = 'http://192.168.130.196:8088/ari'
    ARI_HOST = '192.168.130.196'
    ARI_USER = 'ertech'
    ARI_PASSWORD = 'ERTECH_DEVELOPMENT'
    
    args = parse_args()
    
    args_dict = {
                'endpoint': args.endpoint,
                'callerid': args.callerid,
                'context': args.context,
                'extension': args.extension,
                'priority': args.priority,
                'timeout': args.timeout,
                'variables': {'variables': {'__DATAKEY':'1'}}
            }
    
    process_call(ARI_URL, ARI_HOST, ARI_USER, ARI_PASSWORD, args_dict)

