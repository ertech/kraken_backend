# -*- coding: utf-8 -*-

from xml.dom import minidom
from odisea.library import msgs
import django

def createJgridXML(data, p_vars, fields, exclude=[]):
    doc = minidom.Document()
    root_node = doc.createElement("rows")
    page_node = doc.createElement("page")
    page_node.appendChild(doc.createTextNode(str(p_vars["page"])))
    total_node = doc.createElement("total")
    total_node.appendChild(doc.createTextNode(str(p_vars["total_pages"])))
    records_node = doc.createElement("records")
    records_node.appendChild(doc.createTextNode(str(p_vars["count"])))
    root_node.appendChild(page_node)
    root_node.appendChild(total_node)
    root_node.appendChild(records_node)
    root_node = msgs.createJGridDocFromRawQuerySet(root_node, data, fields, exclude)
    doc.appendChild(root_node)
    return doc

def createJgridXMLFromDic(data, p_vars, fields):
    doc = minidom.Document()
    root_node = doc.createElement("rows")
    page_node = doc.createElement("page")
    page_node.appendChild(doc.createTextNode(str(p_vars["page"])))
    total_node = doc.createElement("total")
    total_node.appendChild(doc.createTextNode(str(p_vars["total_pages"])))
    records_node = doc.createElement("records")
    records_node.appendChild(doc.createTextNode(str(p_vars["count"])))
    root_node.appendChild(page_node)
    root_node.appendChild(total_node)
    root_node.appendChild(records_node)    
    root_node = msgs.createJgridDocFromDic(root_node, data, fields)
    doc.appendChild(root_node)
    return doc

def packResponseXML(item_name, data, status, format="", p_vars=[], fields=[], msg=None):
    if type(data) == dict:
        doc = msgs.createDocFromDic(item_name, data, status)
    elif type(data) == django.db.models.query.ValuesQuerySet:
        if format == "jgrid":
            doc = createJgridXMLFromDic(data, p_vars, fields)
        else:        
            doc = msgs.createDocFromValuesQuerySet(item_name, data, status)
    elif type(data) == list:
        doc = msgs.createDocFromValuesQuerySet(item_name, data, status)
    else:
        pk_id = None
        if not status:
            status = msgs.xhrTypeUnk
        doc = msgs.createSingleMsgXML(item_name, pk_id, status, msg)
    return doc

def convertVectorToCSV(vector):
    csv_values = ""
    for item in vector:
        if len(csv_values) > 0:
            csv_values += ","

        csv_values += item

    return csv_values

def convertVectorToString(vector):
    data_str = ""
    for msg in vector:
        data_str += msg
    return data_str

def stringToBoolean(data_str):
    if data_str == "True":
        return True
    else:
        return False
    
