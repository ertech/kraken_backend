DELETE FROM dashboard_dashboardwidget;
DELETE FROM dashboard_widget;
DELETE FROM dashboard_widgettype;

INSERT INTO dashboard_widgettype ( id, name ) VALUES ( 'MENU', 'MENU');
INSERT INTO dashboard_widgettype ( id, name ) VALUES ( 'GRAPH', 'GRAFICA');
INSERT INTO dashboard_widgettype ( id, name ) VALUES ( 'BOXES', 'CAJAS');
INSERT INTO dashboard_widgettype ( id, name ) VALUES ( 'TABLE', 'TABLA');
INSERT INTO dashboard_widgettype ( id, name ) VALUES ( 'TEXT', 'TEXTO');
INSERT INTO dashboard_widgettype ( id, name ) VALUES ( 'LIST', 'LISTA');

INSERT INTO dashboard_widget ( id, title, url, type_id, cols, "rows" ) VALUES (1, 'Información de Agentes', 'report/queueagentinfo/', 'BOXES', 4, 1);
INSERT INTO dashboard_dashboardwidget (owner_id, position, widget_id, account_type_id, account_id ) SELECT 'ertech', 1, 1, id, NULL FROM accounts_accounttype WHERE id IN ('A', 'R', 'C');

INSERT INTO dashboard_widget ( id, title, url, type_id, cols, "rows" ) VALUES (2, 'Agentes', 'report/agentlist/', 'TABLE', 4, 2);
INSERT INTO dashboard_dashboardwidget (owner_id, position, widget_id, account_type_id, account_id ) SELECT 'ertech', 2, 2, id, NULL FROM accounts_accounttype WHERE id IN ('C', 'A', 'R');

INSERT INTO dashboard_widget ( id, title, url, type_id, cols, "rows" ) VALUES (3, 'Distribución por Cola', 'report/queuecalls/', 'GRAPH', 2, 2);
INSERT INTO dashboard_dashboardwidget (owner_id, position, widget_id, account_type_id, account_id ) SELECT 'ertech', 3, 3, id, NULL FROM accounts_accounttype WHERE id IN ('C', 'A', 'R');

INSERT INTO dashboard_widget ( id, title, url, type_id, cols, "rows" ) VALUES (4, 'Distribución Llamadas', 'report/totalcalls/', 'GRAPH', 2, 2);
INSERT INTO dashboard_dashboardwidget (owner_id, position, widget_id, account_type_id, account_id ) SELECT 'ertech', 4, 4, id, NULL FROM accounts_accounttype WHERE id IN ('R', 'A', 'C');

INSERT INTO dashboard_widget ( id, title, url, type_id, cols, "rows" ) VALUES (5, 'Distribución por Troncal', 'report/trunkcalls/', 'GRAPH', 2, 2);
INSERT INTO dashboard_dashboardwidget (owner_id, position, widget_id, account_type_id, account_id ) SELECT 'ertech', 5, 5, id, NULL FROM accounts_accounttype WHERE id IN ('C', 'A', 'R');

INSERT INTO dashboard_widget ( id, title, url, type_id, cols, "rows" ) VALUES (6, 'Distribución Agente', 'report/agentcalls/', 'GRAPH', 2, 2);
INSERT INTO dashboard_dashboardwidget (owner_id, position, widget_id, account_type_id, account_id ) SELECT 'ertech', 6, 6, id, NULL FROM accounts_accounttype WHERE id IN ('R', 'A', 'T');

INSERT INTO dashboard_widget ( id, title, url, type_id, cols, "rows" ) VALUES (7, 'Llamadas en curso', 'report/trunknowcalls/', 'GRAPH', 2, 1);
INSERT INTO dashboard_dashboardwidget (owner_id, position, widget_id, account_type_id, account_id ) SELECT 'ertech', 7, 7, id, NULL FROM accounts_accounttype WHERE id IN ('A', 'R');
