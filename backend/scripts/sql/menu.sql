DELETE FROM menu_optionaccess;
DELETE FROM menu_option;

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('200', 'Configurar Sistema', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('201', 'Configurar Menú', 'menu/optionaccesstable/', FALSE, '200' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('202', 'Configurar DashBoard', 'dashboard/dashboardwidgettable/', FALSE, '200' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('203', 'Configurar Panel de Control', 'controlpanel/actiontable/', FALSE, '200' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('204', 'Base de Datos de Errores', 'errors/errormessagestable/', FALSE, '200' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('300', 'WorkFlow', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('301', 'Definición Workflow', 'workflow/workflowtable/', FALSE, '300' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('302', 'Definición de Nodos', 'workflow/nodesteptable/', FALSE, '300' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('303', 'Definición de Acciones', 'workflow/actionsteptable/', FALSE, '300' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('400', 'Pestañas', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('401', 'Definición Procesos', 'tabs/tabsprocesstable/', FALSE, '400' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('402', 'Definición de Pestañas', 'tabs/tabtable/', FALSE, '400' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1000', 'Admin', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1001', 'Parámetros Generales', 'asterisk/telephonyparametertable/', FALSE, '1000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1002', 'Cuentas', 'accounts/accounttable/', FALSE, '1000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1003', 'Colas', 'staff/queuetable/', FALSE, '1000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1004', 'Agentes', 'staff/agenttable/', FALSE, '1000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1005', 'Administración de Correo de Voz', 'staff/vmmanagertable/', FALSE, '1000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1006', 'Números Personales', 'staff/personalnumbertable/', FALSE, '1000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1007', 'Copias de Respaldo', 'backup/backupfiletable/', FALSE, '1000');
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1998', 'Aplicar Cambios', 'controlpanel/menu/', FALSE, '1000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('1999', 'Panel de Control', 'controlpanel/menu/', FALSE, '1000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('2000', 'Parámetros', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('2001', 'Razones de Bloqueos', 'staff/lockingreasontable/', FALSE, '2000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('2002', 'Servidores SQL', 'sqlconn/sqlservertable/', FALSE, '2000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('3000', 'Restricciones de llamadas', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('3001', 'Lista Negra', 'blacklist/blacklisttable/', FALSE, '3000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('3002', 'Lista Gris', 'blacklist/graylisttable/', FALSE, '3000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('3003', 'Lista Blanca', 'blacklist/whitelisttable/', FALSE, '3000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('4000', 'Configurar PBX', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('4001', 'DIDS', 'didmanager/didtable/', FALSE, '4000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('4002', 'Menu PBX', 'menupbx/menupbxtable/', FALSE, '4000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('4003', 'Encuestas', 'survey/surveytable/', FALSE, '4000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('4004', 'Acciones', 'actions/dialactiontable/', FALSE, '4000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('4005', 'Audios Predefinidos', 'actions/predefinedaudiotable/', FALSE, '4000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('5000', 'Plan de Marcado', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('5001', 'Proveedores IP', 'dialplan/ipprovidertable/', FALSE, '5000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('5002', 'Troncales', 'dialplan/dialtrunktable/', FALSE, '5000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('5003', 'Esquemas Marcado', 'dialplan/dialschemetable/', FALSE, '5000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('6000', 'Fax', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('6001', 'Disposivos Fax', 'faxmanager/faxdevicetable/', FALSE, '6000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('6002', 'Documentos Fax', 'faxmanager/documenttable/', FALSE, '6000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('6003', 'Documentos Enviados Fax', 'faxmanager/sentdocumenttable/', FALSE, '6000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('7000', 'Bloqueos', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('7001', 'Agentes Bloqueados', 'staff/lockedagenttable/', FALSE, '7000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('8000', 'Configurar IVR', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('8001', 'IVR', 'ivrmanager/ivrtable/', FALSE, '8000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('9000', 'Autollamadas', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('9001', 'Parámetros', 'autocall/parametertable/', FALSE, '9000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('9002', 'Plantillas', 'autocall/templatetable/', FALSE, '9000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('9003', 'Campañas', 'autocall/campaigntable/', FALSE, '9000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('9004', 'Exportar', 'autocall/export/', FALSE, '9000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('10000', 'Operativo', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('10001', 'Tipificación', 'operative/typingtable/', FALSE, '10000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('10002', 'Campos de Gestión', 'operative/gestionfieldtable/', FALSE, '10000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('10004', 'Iniciar Gestión', 'operative/startgestion/', FALSE, '10000');
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('10005', 'Zona de Trabajo', 'operative/executeworkzonegestion/', FALSE, '10000');

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('11000', 'Control', NULL, FALSE, NULL );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('11001', 'Grabaciones Agentes', 'operative/agerecordtable/', FALSE, '11000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('11002', 'Grabaciones Esquema', 'operative/schrecordtable/', FALSE, '11000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('11003', 'Grabaciones DID', 'operative/didrecordtable/', FALSE, '11000' );
INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('11004', 'Grabaciones Encuesta', 'operative/surrecordtable/', FALSE, '11000' );

INSERT INTO menu_option ( id, title, url, other_window, parent_id ) VALUES ('100000', 'Acerca de', 'about/', FALSE, NULL);

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'ertech', id, 'R' FROM menu_option;

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'ertech', id, 'A' FROM menu_option WHERE id >= '1000'; 

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'ertech', '100000', id FROM accounts_accounttype WHERE id not in ('R', 'A') ;

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'ertech', id, 'C' FROM menu_option WHERE id >= '10000' AND id < '11000'; 

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'ertech', '10000', id FROM accounts_accounttype WHERE id = 'T' ;
INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'ertech', '10004', id FROM accounts_accounttype WHERE id = 'T' ;
INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'ertech', '10005', id FROM accounts_accounttype WHERE id = 'T' ;

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'ertech', id, 'M' FROM menu_option WHERE id >= '11000' AND id < '12000'; 

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'pacific', id, 'R' FROM menu_option;

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'pacific', id, 'A' FROM menu_option WHERE id >= '1000'; 

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'pacific', '100000', id FROM accounts_accounttype WHERE id not in ('R', 'A') ;

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'pacific', id, 'C' FROM menu_option WHERE id >= '10000' AND id < '11000'; 

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'pacific', '10000', id FROM accounts_accounttype WHERE id = 'T' ;
INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'pacific', '10004', id FROM accounts_accounttype WHERE id = 'T' ;
INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'pacific', '10005', id FROM accounts_accounttype WHERE id = 'T' ;

INSERT INTO menu_optionaccess (owner_id, option_id, account_type_id) SELECT 'pacific', id, 'M' FROM menu_option WHERE id >= '11000' AND id < '12000'; 



