INSERT INTO errors_errortype (id, name) VALUES ('INT', 'INTERFACE');
INSERT INTO errors_errortype (id, name) VALUES ('TEL', 'TELEPHONY');

INSERT INTO errors_errormessage (error_type_id, id, name, description) VALUES ('TEL', 'TEL0001', 'El agente no tiene Permisos', 'El usuario no pertenece a un grupo autorizado para llamar a través de este esquema');
INSERT INTO errors_errormessage (error_type_id, id, name, description) VALUES ('TEL', 'TEL0002', 'El agente no se encuentra activo ', 'El usuario se encuentra inactivo para hacer llamadas');
INSERT INTO errors_errormessage (error_type_id, id, name, description) VALUES ('TEL', 'BLI0001', 'No se permite marcar al número', 'El Número marcado se encuentra en la lista negra para llamaadas salientes');
INSERT INTO errors_errormessage (error_type_id, id, name, description) VALUES ('TEL', 'BLI0002', 'No se permite recibir llamadas del número', 'El Número origen se encuentra en la lista negra para llamaadas entrantes');
INSERT INTO errors_errormessage (error_type_id, id, name, description) VALUES ('TEL', 'LCK0001', 'El agente se encuentra bloqueado', 'El usuario se encuentra bloqueado para hacer llamadas');
INSERT INTO errors_errormessage (error_type_id, id, name, description) VALUES ('TEL', 'ACT0001', 'La acción ingresada no es válida', 'El Usuario ingresó una opción invalida');
INSERT INTO errors_errormessage (error_type_id, id, name, description) VALUES ('TEL', 'AUT0001', 'La llamada está fuera de rango de tiempo', 'El llamada enviada está fuera del rango de tiempo permitido');
