INSERT INTO operative_generalparameters ( owner_id, environment_id, get_permit ) VALUES ( 'ertech', 'DEVELOPMENT', TRUE );
INSERT INTO operative_generalparameters ( owner_id, environment_id, get_permit ) VALUES ( 'ertech', 'TESTING', TRUE );
INSERT INTO operative_generalparameters ( owner_id, environment_id, get_permit ) VALUES ( 'ertech', 'CLIENT_TESTING', TRUE );
INSERT INTO operative_generalparameters ( owner_id, environment_id, get_permit ) VALUES ( 'ertech', 'PRODUCTION', FALSE );
INSERT INTO operative_generalparameters ( owner_id, environment_id, get_permit ) VALUES ( 'conal', 'DEVELOPMENT', TRUE );
INSERT INTO operative_generalparameters ( owner_id, environment_id, get_permit ) VALUES ( 'conal', 'TESTING', TRUE );
INSERT INTO operative_generalparameters ( owner_id, environment_id, get_permit ) VALUES ( 'conal', 'CLIENT_TESTING', FALSE );
INSERT INTO operative_generalparameters ( owner_id, environment_id, get_permit ) VALUES ( 'conal', 'PRODUCTION', FALSE );

INSERT INTO operative_documenttype ( id, name, disan_code ) VALUES ( '1', 'CEDULA CIUDADANIA', '4' );
INSERT INTO operative_documenttype ( id, name, disan_code ) VALUES ( '2', 'REG. CIVIL', '2' );
INSERT INTO operative_documenttype ( id, name, disan_code ) VALUES ( '3', 'CEDULA EXT.', '5' );
INSERT INTO operative_documenttype ( id, name, disan_code ) VALUES ( '4', 'NUIP', 'N' );
INSERT INTO operative_documenttype ( id, name, disan_code ) VALUES ( '5', 'T. IDENTIDAD', '3' );
INSERT INTO operative_documenttype ( id, name, disan_code ) VALUES ( '6', 'PASAPORTE', 'P' );
INSERT INTO operative_documenttype ( id, name, disan_code ) VALUES ( '7', 'MENOR SIN ID', 'M' );


--Para cédula de ciudadanía marque 1
--
--Registro civil y Nuip marque 2
--
--Cédula de extranjería marque 3
--
--Tarjeta de identidad marque 4
--
--Pasaporte marque 5

INSERT INTO operative_specialitycode ( id, name, service_code, speciality_code, subspeciality_code ) VALUES ( '1', 'MEDICINA GENERAL', 'CMG001', '6', '177' );
INSERT INTO operative_specialitycode ( id, name, service_code, speciality_code, subspeciality_code ) VALUES ( '2', 'ODONTOLOGIA GENERAL', 'ODG001', '25', '181' );
INSERT INTO operative_specialitycode ( id, name, service_code, speciality_code, subspeciality_code ) VALUES ( '3', 'ODONTOLOGIA PEDIATRICA', 'ODP001', '25', '111' );
INSERT INTO operative_specialitycode ( id, name, service_code, speciality_code, subspeciality_code ) VALUES ( '4', 'PEDIATRIA', 'PED001', '11', '135' );
INSERT INTO operative_specialitycode ( id, name, service_code, speciality_code, subspeciality_code ) VALUES ( '5', 'OPTOMETRIA', 'OPT001', '7', '119' );
INSERT INTO operative_specialitycode ( id, name, service_code, speciality_code, subspeciality_code ) VALUES ( '6', 'NUTRICION PROMOCION Y PREVENCION', 'NUP001', '12', '280' );
INSERT INTO operative_specialitycode ( id, name, service_code, speciality_code, subspeciality_code ) VALUES ( '7', 'PSICOLOGIA', 'PSC007', '29', '179' );

  