--
INSERT INTO license_protocol ( name ) VALUES ('http');
INSERT INTO license_protocol ( name ) VALUES ('https');

INSERT INTO license_environment ( name, is_active ) VALUES ('DEVELOPMENT', FALSE);
INSERT INTO license_environment ( name, is_active ) VALUES ('TESTING', FALSE);
INSERT INTO license_environment ( name, is_active ) VALUES ('CLIENT_TESTING', FALSE);
INSERT INTO license_environment ( name, is_active ) VALUES ('PRODUCTION', FALSE);

INSERT INTO license_resourcehost ( environment_id, name, host, port, protocol_id ) VALUES ( 'DEVELOPMENT', 'BASE_URL', '192.168.130.196', 8080, 'http');
INSERT INTO license_resourcehost ( environment_id, name, host, port, protocol_id ) VALUES ( 'TESTING', 'BASE_URL', '201.217.206.142', 8080, 'http');
INSERT INTO license_resourcehost ( environment_id, name, host, port, protocol_id ) VALUES ( 'CLIENT_TESTING', 'BASE_URL', '201.217.206.142', 8080, 'http');
INSERT INTO license_resourcehost ( environment_id, name, host, port, protocol_id ) VALUES ( 'PRODUCTION', 'BASE_URL', '201.217.206.142', 8080, 'http');

INSERT INTO license_owner (id, name, email) VALUES ('ertech', 'ER Technology LTDA', 'info@er-technology.net');
INSERT INTO license_owner (id, name, email) VALUES ('conal', 'Conalcréditos', 'info@er-technology.net');

    