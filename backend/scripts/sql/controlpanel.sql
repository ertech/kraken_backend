DELETE FROM controlpanel_action_account_type_list;
DELETE FROM controlpanel_action;
DELETE FROM controlpanel_block;
DELETE FROM controlpanel_actiontype;

INSERT INTO controlpanel_actiontype ( id, name ) VALUES ('1', 'TABLE');
INSERT INTO controlpanel_actiontype ( id, name ) VALUES ('2', 'ACTION');
INSERT INTO controlpanel_actiontype ( id, name ) VALUES ('3', 'DIALOG');

INSERT INTO controlpanel_block ( id, name ) VALUES ('1', 'Aplicar Cambios');
INSERT INTO controlpanel_block ( id, name ) VALUES ('2', 'Ejecución Procesos');
INSERT INTO controlpanel_block ( id, name ) VALUES ('3', 'Procesos Nocturnos');
INSERT INTO controlpanel_block ( id, name ) VALUES ('4', 'Servidor');

INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('1', 'Generar y Actualizar Todos los Archivos', 'controlpanel/menu/', 'settings', 1, '2');
INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('1', 'Aplicar Todos los Cambios', 'controlpanel/menu/', 'done', 2, '2');

INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('2', 'Recargar Antares', '/', 'settings_phone', 1, '2');
INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('2', 'Reiniciar Asterisk', 'controlpanel/menu/', 'update', 2, '2');
INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('2', 'Parar Asterisk', 'controlpanel/menu/', 'visibility_off', 3, '2');
INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('2', 'Iniciar Asterisk', 'controlpanel/menu/', 'visibility', 4, '2');

INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('3', 'Ejecutar Cierre (Retrofecha)', 'controlpanel/menu/', 'vertical_align_bottom', 1, '2');
INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('3', 'Ejecutar Inicio de Día', 'controlpanel/menu/', 'vertical_align_top', 2, '2');

INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('4', 'Reiniciar Servidor', 'controlpanel/menu/', 'refresh', 1, '2');
INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('4', 'Encender Servidor', 'controlpanel/menu/', 'power_settings_new', 2, '2');
INSERT INTO controlpanel_action ( block_id, title, url, icon, "order", type_id ) VALUES ('4', 'Apagar Servidor', 'controlpanel/menu/', 'settings_power', 3, '2');

INSERT INTO controlpanel_action_account_type_list (action_id, accounttype_id) SELECT id, 'R' FROM controlpanel_action;
INSERT INTO controlpanel_action_account_type_list (action_id, accounttype_id) SELECT id, 'A' FROM controlpanel_action;

