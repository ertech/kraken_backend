# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-05-02 17:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ErrorMessage',
            fields=[
                ('id', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('name', models.CharField(help_text='large', max_length=255)),
                ('description', models.TextField(blank=True, help_text='xlarge', max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ErrorType',
            fields=[
                ('id', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='errormessage',
            name='error_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='errors.ErrorType'),
        ),
    ]
