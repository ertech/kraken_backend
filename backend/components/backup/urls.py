from backend.components.backup import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

router = DefaultRouter()

router.register(r'backupfile', views.BackupFileViewSet)
router.register(r'backupfiletable', views.BackupFileTableViewSet)

urlpatterns = [
    url(r'', include(router.urls)), 
]

urlpatterns += format_suffix_patterns([
        url(r'^downloadfile/(?P<backup_file_id>\d+)/$', views.downloadBackupViewSet.as_view()),
])