# -*- coding: utf-8 -*-
from backend.core.metadata import GenericTableMetadata

class BackupFileTableMetadata(GenericTableMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericTableMetadata.determine_metadata(self, request, view)
        meta_data_dict['can_add'] = False
        meta_data_dict['can_delete'] = True
        meta_data_dict['can_update'] = False
        meta_data_dict['can_query'] = False
        return meta_data_dict
    