# -*- coding: utf-8 -*-

from backend.core.views import GenericTableViewSet, GenericModelViewSet
from backend.components.backup.models import BackupFile
from backend.components.backup.serializers import BackupFileTableSerializer,\
    BackupFileSerializer
from backend.core.permissions import IsAdminAndReadOnly, IsAdminOrReadOnly
from backend.components.backup.metadata import BackupFileTableMetadata
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

class BackupFileViewSet(GenericModelViewSet):
    app_name = 'backup'
    model_name = 'BackupFile'
    queryset = BackupFile.objects.all()
    serializer_class = BackupFileSerializer
    permission_classes = (IsAuthenticated, IsAdminOrReadOnly,)
    
class BackupFileTableViewSet(GenericTableViewSet):
    app_name = 'backup'
    model_name = 'BackupFile'
    queryset = BackupFile.objects.all()
    serializer_class = BackupFileTableSerializer
    permission_classes = (IsAdminAndReadOnly,)
    metadata_class = BackupFileTableMetadata

class downloadBackupViewSet(APIView):
    
    def get(self, request, backup_file_id):
        validate_response = '1'
        error_msg = ''
        owner = None
            
        backup_file = None
        data = {}

        if validate_response == '1':
            try:
                backup_file = BackupFile.objects.get(owner=owner, pk=backup_file_id)
            except BackupFile.DoesNotExist:
                validate_response = '0'
                error_msg = 'Registro no encontrado'
         
        if validate_response == '1':
            data = backup_file.getDict()

        if validate_response == '1':
            return Response(data=data)
        return Response(data={'ERROR': error_msg}, status=status.HTTP_400_BAD_REQUEST)
