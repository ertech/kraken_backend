# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.components.backup.models import BackupFile
from backend.auth.license.models import ResourceHost

class BackupFileSerializer(serializers.ModelSerializer):
 
    class Meta:
        verbose_name = 'Copia de Respaldo'
        model = BackupFile
        fields = ("id", "owner", "creation_date_time", "backup_file",)
                      
class BackupFileTableSerializer(serializers.ModelSerializer):
    owner = serializers.CharField(label='Cliente')
    creation_date_time = serializers.SerializerMethodField('get_date_time')

    def get_date_time(self, obj):
        return "%s" % obj.creation_date_time.strftime('%Y-%m-%d %H:%M')

    class Meta:
        model = BackupFile
        verbose_name = 'Copia de Respaldo'
        fields = ("id", "owner", "creation_date_time", )
        ordering_fields = ('owner', "creation_date_time", )
        add_actions = [{ 'title': 'Descargar', 
                         'form_type': 'DOWNLOAD', 
                          'url': ResourceHost.getURL('BASE_URL') + '/backup/downloadfile/',
                          "icon" : 'file_download',
                          'backurl': ResourceHost.getURL('BASE_URL') + '/backup/backupfiletable/',
                        },
            ]
