# -*- coding:utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
import logging
import datetime
from backend.auth.license.models import Owner
from backend.components.backup.models import BackupFile
from backend.apps.staff.models import Agent

class Command(BaseCommand):

    def handle(self, *args, **options):
        date = datetime.date.today()
        owner_list = Owner.objects.all()
        for owner in owner_list:
            log_filename = settings.LOG_ROOT 
            log_filename += '/init_day_'
            log_filename += settings.APP_NAME
            log_filename += '_'
            log_filename += owner.id
            log_filename += '_'
            log_filename += date.strftime('%Y%m%d')
            log_filename += '.txt'
            
            Command.process(owner, log_filename, date)
    
            try:
                owner.sendLogEmail(log_filename, "InitDay", date)
            except Exception as e:
                print ("Exception: " + str(e))
                print ("Error Send Mail Init Day...")

    def unlock_agent(step, owner, date, logger):
        logger.info( str(step) + ". Start Unlock Agents...")
        try:
            Agent.objects.filter(owner=owner, locked=True).update(locked=False)
        except Exception as e:
            logger.error( str(step) + ". Exception: %s" % str(e))
        logger.info( str(step) + ". End Unlock Agents...")
            
    unlock_agent = staticmethod(unlock_agent)

    def generate_backup(step, owner, date, logger):
        logger.info( str(step) + ". Start Backup...")
        try:
            BackupFile.createBackup(owner, logger)
        except Exception as e:
            logger.error( str(step) + ". Exception: %s" % str(e))
        logger.info( str(step) + ". End Backup...")
            
    generate_backup = staticmethod(generate_backup)

    def process(owner, log_filename, date):
        logger = logging.getLogger('initDay')
        hdlr = logging.FileHandler(log_filename)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.DEBUG)
        logger.info('----------------------------')
        logger.info("Begin Init Day...")
        try:
            actual_step = 1
            Command.unlock_agent(actual_step, owner, date, logger)
            actual_step += 1
            Command.generate_backup(actual_step, owner, date, logger)
            actual_step += 1
            logger.info( "End Init Day...")
        except Exception as e:
            logger.error("Error Init Day...")
            logger.error("Exception: " + str(e) + "")
        logger.removeHandler(hdlr)
        hdlr.close()
    
    process = staticmethod(process)

