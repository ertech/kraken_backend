# -*- coding:utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
import logging
import datetime
from backend.auth.license.models import Owner
from backend.apps.autocall.models import AutocallExecution, Parameter
import sys
import time

class Command(BaseCommand):
    help = 'Ejecuta los procesos de Autollamadas'

    def handle(self, *args, **kwargs):
        owner_list = Owner.objects.all()
        while True:
            date_time = datetime.datetime.now()
            for owner in owner_list:
                log_filename = settings.LOG_ROOT 
                log_filename += '/autocall_process_'
                log_filename += owner.id
                log_filename += '_'
                log_filename += date_time.strftime('%Y%m%d%H%M%S')
                log_filename += '.txt'
                    
                result = Command.process(owner, log_filename)

                time.sleep(600)
                
                if not result:
                    try:
                        owner.sendLogEmail(log_filename, "Autocall", date_time.date())
                    except Exception as e:
                        print ("Exception: " + str(e))
                        print ("Error Send Mail Autocall Process %s..." % owner.id)
            
    def process(owner, log_filename):
        logger = logging.getLogger('autocall_process_' + str(owner.id))
        hdlr = logging.FileHandler(log_filename)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.DEBUG)
        logger.info('----------------------------')
        logger.info("Start Autocall Process %s..." % owner.id)
        result = True
        
        try:
            run_process = True
            while run_process: 
                try:
                    autocall_execution = AutocallExecution.objects.get(owner=owner)
                except AutocallExecution.DoesNotExist:
                    autocall_execution = AutocallExecution()
                    autocall_execution.owner = owner
                    autocall_execution.save()
                
                try:
                    parameter = Parameter.objects.get(owner=owner)
                    now = datetime.datetime.now()
                    if now.time() < parameter.first_hour or now.time() > parameter.last_hour:
                        autocall_execution.in_execution = False
                        autocall_execution.save()
                    
                except Parameter.DoesNotExist:
                    parameter = None
                    result = False
                    
                if parameter is not None:
                    if not autocall_execution.in_execution:
                        now = datetime.datetime.now()
                        if now.time() >= parameter.first_hour and now.time() <= parameter.last_hour:
                            autocall_execution.in_execution = True
                            autocall_execution.save()
                        
                    run_process = autocall_execution.in_execution
                    if run_process:
                        autocall_execution.runCampaign()
                    
                    del parameter
                    del autocall_execution
                   
                time.sleep(30)
                
            logger.info("End Autocall Process %s..." % owner.id)
        except Exception as e:
            logger.error("Error Autocall Process %s: Exception: %s" % (owner.id, str(e)))
            
        logger.removeHandler(hdlr)
        hdlr.close()
        return result
    
    process = staticmethod(process)
