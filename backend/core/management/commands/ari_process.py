# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
import logging
import datetime
from backend.auth.license.models import Owner
import sys
import time
import os
import ari
import cmd
import sys
import simplejson as json
import time
import uuid
import requests


import os
import sys
import configparser
from backend.apps.asterisk.models import ARIExecution

class Config:
    
    def __init__(self):
        configuration = configparser.RawConfigParser()
        filename = 'queue_ari.conf'
        filepath = settings.ASTERISK_CONF_ROOT + filename
        configuration.read(filepath)

        self.strategy=configuration.get('general','strategy')
        self.queue_timeout=configuration.get('general','queue_timeout')
        self.extension_ringtime=configuration.get('general','extension_ringtime')
        self.moh=configuration.get('general','moh')
        self.chantype=configuration.get('general','chantype')

        # logs
        self.debug=configuration.get('logs','debug')
        self.app_logfile=configuration.get('logs','app_logfile')
        self.app_errlogfile=configuration.get('logs','app_errlogfile')
        self.queue_logfile=configuration.get('logs','queue_logfile')
        self.queue_logtype=configuration.get('logs','queue_logtype')
    
        # tools
        self.tools_xmpp=configuration.get('tools','xmpp')
        self.tools_statsd=configuration.get('tools','statsd')

        # ami
        self.ami_host=configuration.get('ami','host')
        self.ami_user=configuration.get('ami','user')
        self.ami_pwd=configuration.get('ami','password')

        # ari
        self.ari_host=configuration.get('ari','host')
        self.ari_user=configuration.get('ari','user')
        self.ari_pwd=configuration.get('ari','password')

        # xmpp
        self.xmpp_resource=configuration.get('xmpp','resource')
        self.xmpp_name=configuration.get('xmpp','name')
        self.xmpp_send=configuration.get('xmpp','send')
        

class Command(BaseCommand):
    help = 'Ejecuta los procesos de Autollamadas'

    def add_arguments(self, parser):
        parser.add_argument('ownerid', type=str, help='Indica el Código del Cliente')

    def handle(self, *args, **kwargs):
        result = True
        date_time = datetime.datetime.now()
        ownerid = kwargs['ownerid']
        
        if result:
            try:
                owner = Owner.objects.get(pk=ownerid)
            except Owner.DoesNotExist:
                self.stdout.write(self.style.ERROR('error - El Cliente no Existe.'))
                result = False
        if result:
            log_filename = settings.LOG_ROOT 
            log_filename += '/ari_process_'
            log_filename += owner.id
            log_filename += '_'
            log_filename += date_time.strftime('%Y%m%d%H%M%S')
            log_filename += '.txt'
                
            result = Command.process(owner, log_filename)
    
            try:
                owner.sendLogEmail(log_filename, "ARI", date_time.date())
            except Exception as e:
                print ("Exception: " + str(e))
                print ("Error Send Mail Autocall Process %s..." % ownerid)
        
        if result:
            sys.exit(0)
        else:
            sys.exit(1)

    def process(owner, log_filename):
        logger = logging.getLogger('ARI_process_' + str(owner.id))
        hdlr = logging.FileHandler(log_filename)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.DEBUG)
        logger.info('----------------------------')
        logger.info("Start ARI Process %s..." % owner.id)
        result = True
        if True:
#         try:
            run_process = True
            while run_process: 
                try:
                    ari_execution = ARIExecution.objects.get(owner=owner)
                except ARIExecution.DoesNotExist:
                    ari_execution = ARIExecution()
                    ari_execution.owner = owner
                    ari_execution.save()
                
                try:
                    configuration = Config()
                except:
                    configuration = None
                    
                run_process = ari_execution.in_execution
                if run_process:
                    ari_execution.runProcess(configuration, logger)
                
                del configuration
                del ari_execution
                   
                time.sleep(30)
                
            logger.info("End ARI Process %s..." % owner.id)
#         except Exception as e:
#             logger.error("Error Autocall Process %s: Exception: %s" % (owner.id, str(e)))
        logger.removeHandler(hdlr)
        hdlr.close()
        return result
    
    process = staticmethod(process)
