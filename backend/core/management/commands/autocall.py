# -*- coding:utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
import logging
import datetime
from backend.auth.license.models import Owner
from backend.apps.autocall.models import Campaign
import sys

class Command(BaseCommand):
    help = 'Ejecuta los procesos de Autollamadas'

    def add_arguments(self, parser):
        action_choices = ['load', 'unprogram', 'start', 'pause', 'stop', 'restart', 'program', 'delete']
        parser.add_argument('campaign_id', type=int, metavar='N', help='Indica el identificador de la campaña')
        parser.add_argument('action', type=str, choices=action_choices, help="""Indica la acción para aplicar a la campaña. start=Inicia la ejecución de la campaña de autollamadas.
        pause=Pausa la ejecución de la campaña de autollamadas. stop=Para la ejecución de la campaña de autollamadas. restart=Reinicia el proceso con las llamadas que están en estado no realizada. 
        program=Programa la ejecución de la campaña, (debe ingresar la fecha y hora de programación). unprogram=Eliminar la programación de ejecución de la Campaña. delete=Elimina la campaña""")
        parser.add_argument('-dt', '--datetime', type=str, help='Fecha y Hora de programación', metavar='AAAA-MM-DD HH(24):MM')

    def handle(self, *args, **kwargs):
        result = True
        date_time = datetime.datetime.now()
        campaign_id = kwargs['campaign_id']
        action = kwargs['action']
        datetime = kwargs['datetime']
        programmed_date_time = None
        
        if result:
            try:
                campaign = Campaign.objects.get(pk=campaign_id)
                owner = campaign.owner
            except Campaign.DoesNotExist:
                self.stdout.write(self.style.ERROR('error - La Campaña ingresada no es permitida.'))
                result = False

        if result:
            if action == 'program':
                if datetime is None:
                    self.stdout.write(self.style.ERROR('error - Para programar la ejecución debe ingresar la fecha y la hora (--datetime).'))
                    result = False
                else:
                    try:
                        programmed_date_time = datetime.datetime.strptime(datetime, '%Y-%m-%d %H:%M')
                    except:
                        self.stdout.write(self.style.ERROR('error - El formato de la fecha es errado.'))
                        result = False
        
            
        if result:
            log_filename = settings.LOG_ROOT 
            log_filename += '/autocall_'
            log_filename += owner.id
            log_filename += '_'
            log_filename += action
            log_filename += '_'
            log_filename += str(campaign_id)
            log_filename += '_'
            log_filename += date_time.strftime('%Y%m%d%H%M%S')
            log_filename += '.txt'
                
            result = Command.process(owner, campaign, action, programmed_date_time, log_filename)
    
            try:
                owner.sendLogEmail(log_filename, "Autocall", date_time.date())
            except Exception as e:
                print ("Exception: " + str(e))
                print ("Error Send Mail %s Autocall %d - %s..." % (action.capitalize(), campaign.id, str(campaign)))
        
        if result:
            sys.exit(0)
        else:
            sys.exit(1)

    def process(owner, campaign, action, programmed_date_time, log_filename):
        result = False
        logger = logging.getLogger('autocall_' + str(campaign.id))
        hdlr = logging.FileHandler(log_filename)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.DEBUG)
        logger.info('----------------------------')
        logger.info("Begin %s Autocall %d - %s..." % (action.capitalize(), campaign.id, str(campaign)))
        try:
            if action == 'load':
                result = campaign.Load(logger)
            elif action == 'start':
                result = campaign.Start(logger)
            elif action == 'stop':
                result = campaign.Stop(logger)
            elif action == 'pause':
                result = campaign.Pause(logger)
            elif action == 'restart':
                result = campaign.Restart(logger)
            elif action == 'program':
                result = campaign.Program(programmed_date_time, logger)
            elif action == 'unprogram':
                result = campaign.Unprogram(logger)
            elif action == 'delete':
                result = campaign.Delete(logger)
            
            if not result:
                logger.error("Error %s Autocall %d - %s: Process not executed" % (action.capitalize(), campaign.id, str(campaign)))

            logger.info("End %s Autocall %d - %s..." % (action.capitalize(), campaign.id, str(campaign)))
        except Exception as e:
            logger.error("Error %s Autocall %d - %s: Exception: %s" % (action.capitalize(), campaign.id, str(campaign)), str(e))
        logger.removeHandler(hdlr)
        hdlr.close()
        return result
    
    process = staticmethod(process)
