from django.conf.urls import url, include
from django.contrib import admin
from backend.core import views
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = [
    url(r'^$', views.HomeView.as_view()),
    url(r'^backup/', include('backend.components.backup.urls')),
    url(r'^accounts/', include('backend.auth.accounts.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^favicon\.ico$', views.favicon_view),
    url(r'^health/?', include('health_check.urls')),
] 
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static('favicon.ico', document_root=settings.FAVICON_ROOT)

