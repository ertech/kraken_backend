# -*- coding: utf-8 -*-
from rest_framework.metadata import BaseMetadata
from backend.auth.license.models import ResourceHost

class GenericTotalField:
    
    def checkSerializeField(serializer_class, field_name):
        field = None
        verbose_name = None
        try:
            field = serializer_class._declared_fields[field_name]
            try:
                verbose_name = field.label.capitalize()
            except:
                pass
        except:
            pass
        return field, verbose_name
        
    checkSerializeField = staticmethod(checkSerializeField)
    
    def checkModelField(class_model, field_name):
        field = None
        verbose_name = None
        try:
            field = class_model._meta.get_field(field_name)
            try:
                verbose_name = field.verbose_name.capitalize()
            except:
                pass
        except:
            pass
        return field, verbose_name
        
    checkModelField = staticmethod(checkModelField)
    
    def getFieldData(field_name, fields_actions, serializer_class, class_model, order, dynamic_fields, fields_readonly, m2mtoforeignkey=False,datewithrange=False, mandatory_fields=[], is_filter=False):
        field_dict = None
        
        field_action = None
        if field_name in fields_actions.keys():
            field_action = fields_actions[field_name]
                        
        verbose_name = None
        field = None
        
        if is_filter:
            field, verbose_name = GenericTotalField.checkModelField(class_model, field_name)
            if field is None or verbose_name is None:
                field, verbose_name = GenericTotalField.checkSerializeField(serializer_class, field_name)
        else:
            field, verbose_name = GenericTotalField.checkSerializeField(serializer_class, field_name)
            if field is None or verbose_name is None:
                field, verbose_name = GenericTotalField.checkModelField(class_model, field_name)
            
        if verbose_name is None:
            verbose_name = field_name.capitalize()

        if field is not None:
            field_dict = GenericTotalField.generate_field_parameters(field, field_name, verbose_name, order, dynamic_fields, field_action, fields_readonly, m2mtoforeignkey, datewithrange, mandatory_fields, is_filter)

        return field_dict
    
    getFieldData = staticmethod(getFieldData)
    
    def getFieldDescription(serializer_class, query=False, is_table=False):
        meta_data_dict = {
            'fields': [],
            'filters': [],
            'search': [],
            'ordering': [],
        }
        if is_table:
            meta_data_dict['total'] = []
        try:
            class_model = serializer_class.Meta.model
        except:
            class_model = None

        try:
            field_name_list = serializer_class.Meta.fields
        except:
            field_name_list = []
        try:
            filter_name_list = serializer_class.Meta.filter_fields
        except:
            filter_name_list = []
        try:
            search_name_list = serializer_class.Meta.search_fields
        except:
            search_name_list = []
        try:
            ordering_fields = serializer_class.Meta.ordering_fields
        except:
            ordering_fields = []
        try:
            fields_actions = serializer_class.Meta.fields_actions.copy()
        except:
            fields_actions = {}
        try:
            dynamic_fields = serializer_class.Meta.dynamic_fields
        except:
            dynamic_fields = []
        try:
            total_name_list = serializer_class.Meta.total_fields
        except:
            total_name_list = []
        try:
            fields_readonly = serializer_class.Meta.fields_readonly
        except:
            fields_readonly = []
        try:
            mandatory_fields = serializer_class.Meta.mandatory_fields
        except:
            mandatory_fields = []
        try:
            meta_data_dict['commands'] = serializer_class.Meta.commands.copy()
        except:
            meta_data_dict['commands'] = []        
            
        field_dict_list = []
        filter_dict_list = []
        total_dict_list = []
        
        order = 0
        for field_name in field_name_list:
            field_dict = GenericTotalField.getFieldData(field_name, fields_actions, serializer_class, class_model, order, dynamic_fields, fields_readonly, mandatory_fields=mandatory_fields)

            if field_dict is not None:
                field_dict_list.append(field_dict)

                if field_name in total_name_list:
                    total_dict = field_dict.copy()
                    total_dict_list.append(total_dict)

            order += 1
        
        order = 0
        for field_name in filter_name_list:
            filter_dict = GenericTotalField.getFieldData(field_name, fields_actions, serializer_class, class_model, order, dynamic_fields, fields_readonly, m2mtoforeignkey=True, datewithrange=True, mandatory_fields=mandatory_fields, is_filter=True)
            if filter_dict is not None:
                filter_dict_list.append(filter_dict)
            order += 1
            
        meta_data_dict['fields'] = field_dict_list
        meta_data_dict['filters'] = filter_dict_list
        meta_data_dict['search'] = search_name_list
            
        meta_data_dict['ordering'] = ordering_fields
        if is_table:
            meta_data_dict['total'] = total_dict_list

        return meta_data_dict

    getFieldDescription = staticmethod(getFieldDescription)

    def generate_field_parameters(field, field_name, verbose_name, order, dynamic_fields=None, field_action=None, fields_readonly=[], m2mtoforeignkey=False, datewithrange=False, mandatory_fields=[], is_filter=False):
        field_dict = {}
        fill_url = None
        field_dict['name'] = field_name
        field_dict['verbose_name'] = verbose_name
        field_dict['order'] = order
        field_dict['null_verbose'] = '' 
        field_dict['required'] = None
        field_dict['readonly'] = False
        field_dict['type'] = field.__class__.__name__
        field_dict['child'] = None
        many = False
        try:
            many = field.many
            if many:
                field_dict['child'] = GenericTotalField.getFieldDescription(field.child)
        except:
            pass
            
        if is_filter:
            field_dict['required'] = False

        try:
            field_dict['required'] = field.required
        except:
            pass
        
        if field_dict['required'] is None:
            try:
                field_dict['required'] = not field.null
            except:
                pass
    
        if field_dict['required'] is None:
            try:
                field_dict['required'] = not field.allow_blank
            except:
                pass
    
        if field_dict['required'] is None:
            try:
                field_dict['required'] = not field.blank
            except:
                pass
        
        if field_dict['required'] is None:
            field_dict['required'] = False
                
        try:
            field_dict['readonly'] = not field.editable
        except:
            pass
        
        try:
            field_dict['readonly'] = field.read_only
        except:
            pass
            
        if field_name in mandatory_fields:
            field_dict['required'] = True
            
        try:
            field_dict['max_length'] = field.max_length
        except:
            field_dict['max_length'] = None
        field_dict['re'] = None
        field_dict['hide'] = False
        field_dict['addhide'] = False
        field_dict['edithide'] = False
        field_dict['queryhide'] = False
        field_dict['filterrange'] = False
        field_dict['text_size'] = 'normal'
        field_dict['size'] = 'normal'
        field_dict['align'] = 'left'
        field_dict['text_align'] = 'left'
        field_dict['typping'] = 'any'
        field_dict['text_type'] = 'alphanumeric'
        field_dict['accents'] = True
        field_dict['description'] = None
        field_dict['help_text'] = None
        field_dict['autocomplete'] = False
        field_dict['global'] = False
        field_dict['radiobutton'] = False
        field_dict['maxrow'] = '5'
        field_dict['withdialog'] = False
        if field_dict['type'] in ['IntegerField']:
            field_dict['text_type'] = 'numbers'
        if field_dict['type'] in ['EmailField']:
            field_dict['accents'] = False
        is_table = False
        help_text_var_list = []
        try:
            if field.help_text is not None:
                help_text_var_list = field.help_text.split('|')
        except:
            pass
        for help_text_var in help_text_var_list:
            if help_text_var == 'readonly':
                field_dict['readonly'] = True
            if help_text_var == 'table':
                is_table = True
            if help_text_var == 'hidden':
                field_dict['hide'] = True
            if help_text_var == 'addhidden':
                field_dict['addhide'] = True
            if help_text_var == 'edithidden':
                field_dict['edithide'] = True
            if help_text_var == 'queryhidden':
                field_dict['queryhide'] = True

            if help_text_var in ['tiny', 'small', 'large', 'xlarge', 'xxlarge', 'xxxlarge', 'huge']:
                field_dict['size'] = help_text_var

            if help_text_var == 'center':
                field_dict['align'] = 'center'
            if help_text_var == 'right':
                field_dict['align'] = 'right'
            if help_text_var == 'text_center':
                field_dict['text_align'] = 'center'
            if help_text_var == 'text_right':
                field_dict['text_align'] = 'right'
            if help_text_var == 'filterrange':
                field_dict['filterrange'] = True
            if help_text_var == 'typping_lower':
                field_dict['typping'] = 'lower'
            if help_text_var == 'typping_capital':
                field_dict['typping'] = 'capital'
            if help_text_var == 'typping_any':
                field_dict['typping'] = 'any'
            if help_text_var == 'only_letter':
                field_dict['text_type'] = 'letters'
            if help_text_var == 'only_number':
                field_dict['text_type'] = 'numbers'
            if help_text_var == 'phone':
                field_dict['text_type'] = 'phone'
            if help_text_var == 'no_accents':
                field_dict['accents'] = False
            if help_text_var == 'chronometer':
                field_dict['chronometer'] = True
            if help_text_var == 'autocomplete':
                field_dict['autocomplete'] = True
            if help_text_var == 'withradio':
                field_dict['radiobutton'] = True
            if help_text_var == 'withdialog':
                field_dict['withdialog'] = True
            if help_text_var == 'global':
                field_dict['global'] = True
            if 'textsize=' in help_text_var:
                field_dict['text_size'] = help_text_var.replace('textsize=','')
            if help_text_var == 'uploadtemporalfile':
                field_dict['uploadtempfileurl'] = ResourceHost.getURL('BASE_URL') + '/filemanager/uploadtemporalfile/1/'
                field_dict['deletetempfileurl'] = ResourceHost.getURL('BASE_URL') + '/filemanager/deletetemporalfile/'
            if help_text_var == 'uploadtemporalcontent':
                field_dict['uploadtempfileurl'] = ResourceHost.getURL('BASE_URL') + '/filemanager/uploadtemporalfile/0/'
                field_dict['deletetempfileurl'] = ResourceHost.getURL('BASE_URL') + '/filemanager/deletetemporalfile/'
            if 're=' in help_text_var:
                field_dict['re'] = help_text_var.replace('re=','')
            if 'maxrow=' in help_text_var:
                field_dict['maxrow'] = help_text_var.replace('maxrow=','')
            if 'help=' in help_text_var:
                field_dict['help_text'] = help_text_var.replace('help=','')
            if 'description=' in help_text_var:
                field_dict['description'] = help_text_var.replace('description=','')
            if 'null' in help_text_var:
                null_tuple = help_text_var.split(':')
                try:
                    field_dict['null_verbose'] = null_tuple[1]
                except:
                    pass
            if 'confirm' in help_text_var:
                confirm_tuple = help_text_var.split(':')
                try:
                    field_dict['confirm_field'] = confirm_tuple[1]
                except:
                    pass
                
        if field_dict['name'] in fields_readonly:
            field_dict['readonly'] = True
    
        if field_action is not None:
            for key in field_action.keys():
                if key == 'fill_url':
                    fill_url = field_action[key]
                if key == 'valueaction':
                    if dynamic_fields is not None:
                        for value_key in field_action[key].keys():
                            try:
                                if 'show' in field_action[key][value_key].keys():
                                    field_action[key][value_key]['hide'] = list(set(dynamic_fields) - set(field_action[key][value_key]['show']))
                                elif 'hide' in field_action[key][value_key].keys():
                                    field_action[key][value_key]['show'] = list(set(dynamic_fields) - set(field_action[key][value_key]['hide']))
                                elif 'disable' in field_action[key][value_key].keys():
                                    field_action[key][value_key]['enable'] = list(set(dynamic_fields) - set(field_action[key][value_key]['disable']))
                                elif 'enable' in field_action[key][value_key].keys():
                                    field_action[key][value_key]['disable'] = list(set(dynamic_fields) - set(field_action[key][value_key]['enable']))
                            except:
                                pass
                
            field_dict['actions'] = field_action
        else:
            field_dict['actions'] = {}

        field_dict['type'] = field.__class__.__name__
        
        if field_name == 'password':
            field_dict['type'] = 'PasswordField'
        elif field.__class__.__name__ == 'TextField':
            if field_dict['size'] == 'normal':
                field_dict['size'] = 'xlarge'
        elif field.__class__.__name__ == 'SerializerMethodField':
            if field_dict['size'] == 'xlarge':
                field_dict['type'] = 'TextField'
            else:
                field_dict['type'] = 'CharField'
        elif field.__class__.__name__ == 'JSONField':
            field_dict['type'] = 'TextField'
            field_dict['description'] = 'Diccionario de la forma {"LLAVE": "VALOR"}'
            if field_dict['size'] == 'normal':
                field_dict['size'] = 'xlarge'
        elif field.__class__.__name__ == 'OneToOneRel':
            pass
        elif field.__class__.__name__ in ['ForeignKey', 'ManyToManyField']:
            if m2mtoforeignkey:
                field_dict['type'] = 'ForeignKey'
            if fill_url is None:
                field_dict['options'] = []
                for option in field.related_model.objects.all():
                    option_dict = {}
                    option_dict['id'] = option.id
                    option_dict['name'] = str(option)
                    field_dict['options'].append(option_dict)
        elif field.__class__.__name__ in ['NullBooleanField']:
            field_dict['type'] = 'ForeignKey'
            field_dict['options'] = [
                {'id': '', 'name': '----'},
                {'id': True, 'name': 'Verdadero'},
                {'id': False, 'name': 'Falso'},
            ]
        elif field.__class__.__name__ in ['ChoiceField']:
            field_dict['type'] = 'ForeignKey'
            if fill_url is None:
                field_dict['options'] = []
                for key in field.choices:
                    option_dict = {}
                    option_dict['id'] = key
                    option_dict['name'] = field.choices[key]
                    field_dict['options'].append(option_dict)
        elif field.__class__.__name__ in ['MultipleChoiceField']:
            field_dict['type'] = 'ManyToManyField'
            if m2mtoforeignkey:
                field_dict['type'] = 'ForeignKey'
            if fill_url is None:
                field_dict['options'] = []
                for key in field.choices:
                    option_dict = {}
                    option_dict['id'] = key
                    option_dict['name'] = field.choices[key]
                    field_dict['options'].append(option_dict)
        elif field.__class__.__name__ in ['DateField', 'DateTimeField']:
            if not datewithrange and field_dict['filterrange']:
                field_dict['filterrange'] = False
        if is_table:
            field_dict['type'] = 'TableSerializer'
        return field_dict

    generate_field_parameters = staticmethod(generate_field_parameters)

class GenericTableMetadata(BaseMetadata):
    """
    Don't include field and other information for `OPTIONS` requests.
    Just return the name and description.
    """

    def determine_metadata(self, request, view):
        try:
            serializer_class = view.serializer_class
        except:
            serializer_class = None
        try:
            class_model = serializer_class.Meta.model
        except:
            class_model = None
        try:
            meta_data_dict = GenericTotalField.getFieldDescription(serializer_class)
            meta_data_dict['type'] = 'TABLE'
            meta_data_dict['form_type'] = 'FORM'
            meta_data_dict['edit_form_type'] = 'FORM'
            if class_model:
                meta_data_dict['modelname'] = class_model.__name__.lower()
                meta_data_dict['name'] = class_model._meta.verbose_name.title()
                meta_data_dict['title'] = class_model._meta.verbose_name_plural.title()
                meta_data_dict['url'] = ResourceHost.getURL('BASE_URL') + '/' + class_model._meta.app_label + '/' + class_model.__name__.lower() + '/'
                meta_data_dict['delete_url'] = ResourceHost.getURL('BASE_URL') + '/' + class_model._meta.app_label + '/' + class_model.__name__.lower() + '/'
                meta_data_dict['actualurl'] = ResourceHost.getURL('BASE_URL') + '/' + class_model._meta.app_label + '/' + class_model.__name__.lower() + 'table/'
            else:
                meta_data_dict['name'] = serializer_class.Meta.verbose_name.title()
                meta_data_dict['title'] = serializer_class.Meta.verbose_name.title()

            try:
                detail_class = view.detail_class
                meta_data_dict['detailurl'] = ResourceHost.getURL('BASE_URL') + '/' + detail_class._meta.app_label + '/' + detail_class.__name__.lower() + 'table/'    
            except:
                meta_data_dict['detailurl'] = None
            try:
                master_class = view.master_class
                meta_data_dict['backurl'] = ResourceHost.getURL('BASE_URL') + '/' + master_class._meta.app_label + '/' + master_class.__name__.lower() + 'table/'
            except:
                meta_data_dict['backurl'] = None
            meta_data_dict['with_menu'] = True    
            meta_data_dict['can_query'] = True
            meta_data_dict['can_add'] = False
            meta_data_dict['can_delete'] = False
            meta_data_dict['can_update'] = False
            meta_data_dict['actions'] = []
            try:
                meta_data_dict['actions'] = serializer_class.Meta.actions.copy()
            except:
                meta_data_dict['actions'] = []
            try:
                meta_data_dict['add_actions'] = serializer_class.Meta.add_actions.copy()
            except:
                meta_data_dict['add_actions'] = []
            try:
                meta_data_dict['add_actions_condition'] = serializer_class.Meta.add_actions_condition.copy()
            except:
                meta_data_dict['add_actions_condition'] = []

        except:
            meta_data_dict = {}
        return meta_data_dict

class  GenericMetadata(BaseMetadata):
    """
    Don't include field and other information for `OPTIONS` requests.
    Just return the name and description.
    """

    def determine_metadata(self, request, view):

        query = True
        add_register = False
        edit_register = False
        if (request.POST and 'add' in request.POST.keys() and request.POST['add'] == 'TRUE') or (
                request.GET and 'add' in request.GET.keys() and request.GET['add'] == 'TRUE'):
            add_register = True
            query = False
        if (request.POST and 'edit' in request.POST.keys() and request.POST['edit'] == 'TRUE') or (
                request.GET and 'edit' in request.GET.keys() and request.GET['edit'] == 'TRUE'):
            edit_register = True
            query = False
        
        serializer_class = None
        try:
            serializer_class = view.serializer_class
        except:
            pass
        meta_data_dict = GenericTotalField.getFieldDescription(serializer_class, query)
        meta_data_dict['title'] = ''
        try:
            meta_data_dict['title'] = view.title
        except:
            pass 
        
        try:
            class_model = serializer_class.Meta.model
            meta_data_dict['modelname'] = class_model.__name__.lower()
            
            if meta_data_dict['title'] == '':
                meta_data_dict['title'] = class_model._meta.verbose_name.title()
            
            meta_data_dict['type'] = 'QUERY'
            if add_register:
                meta_data_dict['type'] = 'ADD'
            if edit_register:
                meta_data_dict['type'] = 'EDIT'
            if view.kwargs is not None and 'pk' in view.kwargs:
                try:
                    meta_data_dict['title'] += ' - ' + str(class_model.objects.get(pk=view.kwargs['pk']))
                except:
                    pass
        except:
            if meta_data_dict['title'] == '':
                try:
                    meta_data_dict['title'] = serializer_class.Meta.verbose_name
                except:
                    pass
            
            meta_data_dict['type'] = 'FORM'
        meta_data_dict['savebutton'] = 'Guardar'
        meta_data_dict['exitbutton'] = 'Cancelar'
        meta_data_dict['deletebutton'] = 'Eliminar'

        return meta_data_dict

class GenericQueryMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        
        meta_data_dict = {}
        try:
            meta_data_dict['title'] = view.title
        except:
            meta_data_dict['title'] = 'Consulta'
        meta_data_dict['type'] = 'QUERY'
        try:
            meta_data_dict['fields'] = view.options_fields
        except:
            meta_data_dict['fields'] = {}
        meta_data_dict['savebutton'] = None
        meta_data_dict['exitbutton'] = 'Cancelar'
        return meta_data_dict

class AboutMetadata(GenericQueryMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericQueryMetadata.determine_metadata(self, request, view)
        meta_data_dict['fields'] = [
                                        {  'name': 'sw_name', 'verbose_name': 'Plataforma' },
                                        {  'name': 'version', 'verbose_name': 'Versión Backend' },
                                        {  'name': 'frontend_version', 'verbose_name': 'Versión Frontend' },
                                        {  'name': 'environment', 'verbose_name': 'Entorno Backend' },
                                        {  'name': 'owner', 'verbose_name': 'Licenciado a' },
                                        {  'name': 'owner_email', 'verbose_name': 'Email Cliente' },
                                        {  'name': 'developer', 'verbose_name': "Desarrollador" },
                                        {  'name': 'developer_url', 'verbose_name': "URL Desarrollador" },
                                        {  'name': 'developer_email', 'verbose_name': "Email Desarrollador" },
                                        {  'name': 'developer_address', 'verbose_name': "Dirección Desarrollador" },
                                        {  'name': 'developer_phone', 'verbose_name': "Teléfono Desarrollador" },
                                        {  'name': 'developer_city', 'verbose_name': "Ciudad Desarrollador" },
            ]
        meta_data_dict['exitbutton'] = 'Cancelar'

        return meta_data_dict

class GenericMenuMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = {}
        try:
            meta_data_dict['title'] = view.title
        except:
            meta_data_dict['title'] = 'Menú'
        meta_data_dict['type'] = 'MENU'
        meta_data_dict['exitbutton'] = 'Cancelar'

        return meta_data_dict

class GenericMasterDetailMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        query = True
        add_register = False
        edit_register = False
        if (request.POST and 'add' in request.POST.keys() and request.POST['add'] == 'TRUE') or (
                        request.GET and 'add' in request.GET.keys() and request.GET['add'] == 'TRUE'):
            add_register = True
            query = False
        if (request.POST and 'edit' in request.POST.keys() and request.POST['edit'] == 'TRUE') or (
                        request.GET and 'edit' in request.GET.keys() and request.GET['edit'] == 'TRUE'):
            edit_register = True
            query = False

        serializer_class = view.serializer_class
        class_model = serializer_class.Meta.model
        meta_data_dict = GenericTotalField.getFieldDescription(serializer_class, query)
        meta_data_dict['modelname'] = class_model.__name__.lower()
        meta_data_dict['title'] = 'Creación de '
        meta_data_dict['type'] = 'MASTERDETAIL'
        meta_data_dict['savebutton'] = 'Guardar'
        meta_data_dict['exitbutton'] = 'Cancelar'
        meta_data_dict['deletebutton'] = 'Eliminar'
        if add_register:
            meta_data_dict['title'] = 'Creación de '
            meta_data_dict['type'] = 'ADD'
        if edit_register:
            meta_data_dict['title'] = 'Edición de '
            meta_data_dict['type'] = 'EDIT'

        meta_data_dict['title'] += class_model._meta.verbose_name.title()

        if view.kwargs is not None and 'pk' in view.kwargs:
            try:
                meta_data_dict['title'] += ' - ' + str(class_model.objects.get(pk=view.kwargs['pk']))
            except:
                pass
        try:
            detail_serializer_class = view.detail_serializer_class
            detail_class_model = detail_serializer_class.Meta.model
            meta_data_dict['detailmodelname'] = detail_class_model.__name__.lower()
            meta_detail_data_dict = GenericTotalField.getFieldDescription(detail_serializer_class, is_table=True)
            meta_detail_data_dict['url'] = ResourceHost.getURL('BASE_URL') + '/' + detail_class_model._meta.app_label + '/' + detail_class_model.__name__.lower() + '/'
            meta_detail_data_dict['actualurl'] = ResourceHost.getURL('BASE_URL') + '/' + detail_class_model._meta.app_label + '/' + detail_class_model.__name__.lower() + 'table/'
            meta_data_dict['detail'] = meta_detail_data_dict
        except:
            pass

        return meta_data_dict

class GenericReportMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        serializer_class = view.serializer_class

        meta_data_dict = GenericTotalField.getFieldDescription(serializer_class)
        meta_data_dict['title'] = 'Indicador - '
        meta_data_dict['type'] = 'REPORT'
        meta_data_dict['title'] += serializer_class.Meta.verbose_name.title()
        meta_data_dict['execute_get'] = True
        meta_data_dict['COLS'] = 2
        meta_data_dict['WIDGET_LIST'] = []
        return meta_data_dict

    def getWidgetChart(title, name, chart_type, x_field=None, serie_fields=None):
        widget_graph = {
            'type': 'GRAPH',
            'name': name,
            'title': title,
            'chartType': chart_type,
            'can_export': True,
        }
        if x_field is not None:
            widget_graph ['x_field'] = x_field
        if serie_fields is not None:
            widget_graph['serie_fields'] = serie_fields

        widget_graph['options'] = {}

        return widget_graph

    getWidgetChart = staticmethod(getWidgetChart)

    def getWidgetSummary(title, name):

        widget_availability_summary = {
            'name': name,
            'type': 'TABLE',
            'title': title,
            'export': None,
        }
        return widget_availability_summary

    getWidgetSummary = staticmethod(getWidgetSummary)

    def getWidgetDetail(serializer_class, name, title):
        widget_detail = GenericTotalField.getFieldDescription(serializer_class, query=False)
        widget_detail['name'] = name
        widget_detail['type'] = 'TABLE'
        widget_detail['title'] = title
        widget_detail['export'] = None

        try:
            widget_detail['ordering'] = serializer_class.Meta.ordering_fields
        except:
            widget_detail['ordering'] = []

        return widget_detail

    getWidgetDetail = staticmethod(getWidgetDetail)

class GenericPanelMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = {}
        meta_data_dict['title'] = ''
        meta_data_dict['type'] = 'PANEL'
        meta_data_dict['execute_get'] = True
        meta_data_dict['PANEL_LIST'] = []
        return meta_data_dict

    def getPanelChart(title, name, chart_type, x_field=None, serie_fields=None):
        panel_graph = {
            'type': 'GRAPH',
            'name': name,
            'title': title,
            'chartType': chart_type,
            'can_export': True,
        }
        if x_field is not None:
            panel_graph ['x_field'] = x_field
        if serie_fields is not None:
            panel_graph['serie_fields'] = serie_fields

        panel_graph['hide'] = False
        panel_graph['size'] = 'total'
        panel_graph['expanded'] = True
        panel_graph['options'] = {}

        return panel_graph

    getPanelChart = staticmethod(getPanelChart)

    def getPanelTable(serializer_class, name, title):
        panel_table = GenericTotalField.getFieldDescription(serializer_class, query=False)
        panel_table['name'] = name
        panel_table['type'] = 'TABLE'
        panel_table['title'] = title
        panel_table['hide'] = False
        panel_table['export'] = None
        panel_table['expanded'] = True
        panel_table['adddirect'] = False
        panel_table['editdirect'] = False
        panel_table['canadd'] = True
        panel_table['withpagination'] = False
        panel_table['size'] = 'total'

        try:
            panel_table['ordering'] = serializer_class.Meta.ordering_fields
        except:
            panel_table['ordering'] = []

        return panel_table

    getPanelTable = staticmethod(getPanelTable)

    def getPanelForm(serializer_class, name, title, fields_url=None):
        if serializer_class is not None:
            panel_detail = GenericTotalField.getFieldDescription(serializer_class, query=False)
        else:
            panel_detail = {}
        panel_detail['name'] = name
        panel_detail['type'] = 'FORM'
        panel_detail['size'] = 'total'
        panel_detail['cols'] = 4
        panel_detail['title'] = title
        panel_detail['hide'] = False
        panel_detail['canadd'] = False
        panel_detail['expanded'] = True
        panel_detail['savebutton'] = 'Guardar'
        panel_detail['exitbutton'] = 'Cancelar'
        panel_detail['deletebutton'] = 'Eliminar'
        if fields_url is not None:
            panel_detail['fields_url'] = fields_url

        return panel_detail

    getPanelForm = staticmethod(getPanelForm)

    def getPanelTabs(tabprocess, name, title, tabs_struct_url=None):
        panel_tabs = {}

        if tabprocess is not None:
            panel_tabs['tabprocess'] = tabprocess
            panel_tabs['url'] =  ResourceHost.getURL('BASE_URL') + '/tabs/start/process/',
        panel_tabs['name'] = name
        panel_tabs['type'] = 'TABS'
        panel_tabs['size'] = 'total'
        panel_tabs['cols'] = 4
        panel_tabs['title'] = title
        panel_tabs['hide'] = False
        panel_tabs['expanded'] = True
        panel_tabs['savebutton'] = 'Guardar'
        panel_tabs['exitbutton'] = 'Cancelar'
        panel_tabs['deletebutton'] = 'Eliminar'
        if tabs_struct_url is not None:
            panel_tabs['tabs_struct_url'] = tabs_struct_url

        return panel_tabs

    getPanelTabs = staticmethod(getPanelTabs)

    def getPanelWorkFLow(tabprocess, name, title, start_workflow_url=None):
        panel_tabs = {}

        if tabprocess is not None:
            panel_tabs['tabprocess'] = tabprocess
            panel_tabs['url'] =  ResourceHost.getURL('BASE_URL') + '/tabs/start/process/',
        panel_tabs['name'] = name
        panel_tabs['type'] = 'WORKFLOW'
        panel_tabs['size'] = 'total'
        panel_tabs['cols'] = 2
        panel_tabs['title'] = title
        panel_tabs['hide'] = False
        panel_tabs['expanded'] = True
        panel_tabs['savebutton'] = 'Guardar'
        panel_tabs['exitbutton'] = 'Cancelar'
        panel_tabs['deletebutton'] = 'Eliminar'
        if start_workflow_url is not None:
            panel_tabs['start_workflow_url'] = start_workflow_url

        return panel_tabs

    getPanelWorkFLow = staticmethod(getPanelWorkFLow)

class GenericWorkflowMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        serializer_class = view.serializer_class
        meta_data_dict = GenericTotalField.getFieldDescription(serializer_class)
        meta_data_dict['type'] = 'WORKFLOW'
        meta_data_dict['title'] = 'WorkFlow'
        meta_data_dict['step_title'] = serializer_class.Meta.verbose_name.title()
        return meta_data_dict
    
class GenericTabsMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        serializer_class = view.serializer_class
        meta_data_dict = GenericTotalField.getFieldDescription(serializer_class)
        meta_data_dict['type'] = 'TABS'
        meta_data_dict['title'] = 'Pestañas'
        meta_data_dict['step_title'] = serializer_class.Meta.verbose_name.title()
        meta_data_dict['tabsurl'] = ResourceHost.getURL('BASE_URL') + '/tabs/start/process/'
        meta_data_dict['savebutton'] = 'Guardar'
        meta_data_dict['exitbutton'] = 'Cancelar'
        meta_data_dict['deletebutton'] = 'Eliminar'

        return meta_data_dict

class GenericWidgetGraphMetadata(BaseMetadata):

    def determine_metadata(self, request, view, chart_type):
        try:
            name = view.name
        except:
            name = ''
        try:
            title = view.title
        except:
            title = 'Gráfico'
        try:
            x_field = view.x_field
        except:
            x_field = None
        try:
            serie_fields = view.serie_fields
        except:
            serie_fields = None

        widget_graph = GenericReportMetadata.getWidgetChart(title, name, chart_type, x_field, serie_fields)

        return widget_graph

class GenericWidgetBoxesMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        try:
            name = view.name
        except:
            name = ''
        try:
            title = view.title
        except:
            title = None

        widget_boxes = {
            'type': 'BOXES',
            'name': name,
            'title': title,
        }

        return widget_boxes

class GenericWidgetTableMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        try:
            name = view.name
        except:
            name = ''
        try:
            title = view.title
        except:
            title = 'Data'

        widget_table = {
            'type': 'TABLE',
            'name': name,
            'title': title,
        }

        return widget_table
