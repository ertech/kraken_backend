# -*- coding: utf-8 -*-

from django import template
from django.db.models import Q
from backend.library.utils import check_any_permission_required, \
    check_all_permission_required
from django.contrib.humanize.templatetags.humanize import intcomma
import datetime
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
def getlist(data_dict, key):
    return data_dict.getlist(key)

@register.filter
def has_any_perm(user, permissions):
    return check_any_permission_required(user, permissions)
has_any_perm.is_safe = True

@register.filter
def has_all_perm(user, permissions):
    return check_all_permission_required(user, permissions)
has_all_perm.is_safe = True

@register.filter
def customfloatformat(number):
    result = "%.2f" % number
    return result

customfloatformat.is_safe = True

@register.filter
def getCompleteName(person):
    result = person.first_lastname
    result += ' '
    if person.second_lastname is not None:
        result += person.second_lastname
        result += ' '
    result += person.first_name
    result += ' '
    if person.second_name is not None:
        result += person.second_name
    return result

getCompleteName.is_safe = True

@register.filter
def countOtherCreditCards(person):
#    other_credit_cards_list = person.creditcard_set.all()
#    return other_credit_cards_list.count()

    return person.count_other_credit_cards()
countOtherCreditCards.is_safe = True

@register.filter
def getFinancialInstitucionOtherCreditCards(person):
    other_credit_cards_list = person.creditcard_set.all()
    if other_credit_cards_list.count() > 0:
        return other_credit_cards_list[0].financial_institution
    return ''

getFinancialInstitucionOtherCreditCards.is_safe = True

@register.filter
def getScoringResult(product):
    scoring_list = product.scoring_set.all()
    if scoring_list.count() > 0:
        if scoring_list[0].result is not None:
            data = ""
            condition = scoring_list[0].result.name
            score = scoring_list[0].score
            if score > 0:
                data = str(score) 
            data += " (" + condition + ")"
            return data
    return ''

getScoringResult.is_safe = True

@register.filter
def getScoringResultID(product):
    scoring_list = product.scoring_set.all()
    if scoring_list.count() > 0:
        if scoring_list[0].result:
            return scoring_list[0].result.id
    return ''

getScoringResultID.is_safe = True


@register.filter
def getCheckListValue(process,check_list_type):
    if process is None:
        return None
    try:
        check_list_process = process.checklistprocess_set.get(type=check_list_type)
    except:
        return None
    if check_list_process.check == True:
        return "YES"
    elif check_list_process.check == False:
        return "NO"
    return "NONE"

getCheckListValue.is_safe = True

@register.filter
def getCheckListDateTime(process,check_list_type):
    if process is None:
        return ''
    try:
        check_list_process = process.checklistprocess_set.get(type=check_list_type)
    except:
        return ''
    return check_list_process.date_time.strftime("%Y-%m-%d %H:%M")

getCheckListValue.is_safe = True

@register.filter
def isInCSV(pk_id, csv_list, separator=","):
    id_array = csv_list.split(separator)
    return pk_id in id_array
isInCSV.is_safe = True

@register.filter
def currency(dollars):
    dollars = round(float(dollars), 2)
    return "$%s%s" % (intcomma(int(dollars)), ("%0.2f" % dollars)[-3:])
currency.is_safe = True

@register.filter
def get_groupingitem_list(grouping):
    return grouping.groupingitem_set.exclude(condition_id="D")
get_groupingitem_list.is_safe = True

@register.filter
def get_item_data(item):
    html = "Ruta: %s <br/>Valor declarado: %s <br/>Largo: %s <br/>Ancho: %s <br/>Alto: %s <br/>Peso: %s <br/>Condiciones de transporte: %s" \
           % (item.route.code,
              item.declared_value,
              item.length,
              item.width,
              item.height,
              item.weight,
              item.transport_condition,
              )
    return html

get_item_data.is_safe = True

@register.filter
def calculate_page(value, module):
    return (int(value) - 1)/int(module) + 1  # add 1 one for 1-based initial page
calculate_page.is_safe = True

@register.filter
def get_grouping_id(item):
    html = ""
    groupingitem_list = item.groupingitem_set.exclude(condition_id="D")
    count = groupingitem_list.count()
    if count == 0:
        html = "(No tiene agrupación asignada)"
    elif count == 1:
        html += """
                Agrupación: %d
                """ % (groupingitem_list[0].id)
    else:
        html = "(Se retornaron múltiples agrupaciones)"

    return html
get_grouping_id.is_safe = True

@register.filter
def get_grouping_data(item):
    html = ""
    groupingitem_list = item.groupingitem_set.exclude(condition_id="D")
    count = groupingitem_list.count()
    if count == 0:
        html = "<No tiene agrupación asignada>"
    elif count == 1:
        html += """
                Cabezote: %s<br/>Trailer: %s<br/>Ruta: %s<br/>Operador: %s<br/>Fecha inicial: %s<br/>Fecha final: %s
                """ % (groupingitem_list[0].grouping.truck,
                       groupingitem_list[0].grouping.trailer,
                       groupingitem_list[0].grouping.route,
                       groupingitem_list[0].grouping.transporter,
                       groupingitem_list[0].grouping.start_date.strftime("%Y-%m-%d"),
                       groupingitem_list[0].grouping.end_date.strftime("%Y-%m-%d"),
                       )
    else:
        html = "<Se retornaron múltiples agrupaciones>"

    return html
get_grouping_data.is_safe = True


@register.filter
def get_grouping_type_quantity(grouping_list, grouping_type):
    if grouping_type == "N":
        quantity = grouping_list.exclude(Q(load_weight=0)|Q(is_compensation=True)).count()
    elif grouping_type == "V":
        quantity = grouping_list.filter(load_weight=0).exclude(is_compensation=True).count()
    elif grouping_type == "C":
        quantity = grouping_list.filter(is_compensation=True).count()
    else:
        quantity = "none"

    return quantity
get_grouping_data.is_safe = True


@register.filter
def residue(value, reference):
    return value % reference
residue.is_safe = True

@register.filter
def isTimeNow(diary_parameter, diary_interval):
    time_now = datetime.datetime.now().time()
    if time_now > diary_interval:
        next_interval = diary_parameter.getNextInterval(diary_interval)
        if time_now <= next_interval:
            return time_now.strftime('%H:%M')
    return ''

isTimeNow.is_safe = True    

@register.filter
def canAssign(medical_schedule_detail):
    return medical_schedule_detail.canAssign()

canAssign.is_safe = True    

@register.filter
def canEdit(medical_schedule_detail):
    return medical_schedule_detail.canEdit()

canEdit.is_safe = True

@register.filter
def canCancel(medical_schedule_detail):
    return medical_schedule_detail.canCancel()

canCancel.is_safe = True

@register.filter
def canConfirm(medical_schedule_detail):
    return medical_schedule_detail.canConfirm()

canConfirm.is_safe = True

@register.filter
def canAttend(medical_schedule_detail):
    return medical_schedule_detail.canAttend()

canConfirm.is_safe = True

def formatNumber (  n ):
 
    ltz = 1
    if n < 0:
        ltz = -1
    n = -1 * n
 
    ns  = str(n)
    nsl = ns.split(".")
    nd  = ""
 
    ni = nsl[0]
    if len(nsl) == 2:
        nd=nsl[1]
 
    if len(ni) < 4:
        return ns
 
    leng    = len(ni)
    nifinal = ""
 
    for i in range(leng):
        ind = -1 * (i+1)
        nifinal = ni[ind] + nifinal
 
        if ( i + 1 ) % 3 == 0:
            if i + 1 != leng:
                nifinal = "," + nifinal
 
    if nd != "":
        nd = "." + nd
 
    if ltz == -1:
        nifinal = "-" + nifinal
 
    return nifinal + nd
 
register.simple_tag ( formatNumber )
 
def formatDateTime ( d ):
    return str(d).split ( "." ) [ 0 ]
 
register.simple_tag ( formatDateTime )
 
def getJSArrays(data_list, name=""):
    arr = "var " + name + "Types = new Array ();\n"
    for data in data_list:
        arr += name + 'Types["%s"] = new Array();\n' % (data['id'])
        for element in data['list']:
            arr += name + 'Types["%s"]["%s"] = "%s";\n' % (data['id'],element.id,str(element))
    return arr
         
register.simple_tag ( getJSArrays )
 
def getJSVector(data_list, name=""):
    arr = "var " + name + "Types = new Array ();\n"
    for data in data_list:
        arr += name + 'Types["%s"] = "%s";\n' % (data['id'], data['name'])
    return arr
 
register.simple_tag ( getJSVector )
 
def getJSQuery(data_query, name=""):
    arr = "var " + name + " = new Array ();\n"
    for data in data_query:
        arr += name + '["%s"] = "%s";\n' % (data.id, data.name)
    return arr
 
register.simple_tag ( getJSQuery )
 
def getJSQueryGeneric(data_query, name="", keys_csv=""):
    arr = "var " + name + " = new Array ();\n"
    keys = keys_csv.split(',')
    for data in data_query:
        arr += name + '["%s"] = "%s";\n' % (str(data[keys[0]]), data[keys[1]])
    return arr
 
register.simple_tag ( getJSQueryGeneric )
 
def getFeesValue (arrears_value, arrears_days):
    result = float(arrears_value)
    if int(arrears_days) <= 90:
        result += result * 0.10
    else:
        result += result * 0.15
    result += 10000
    return str(result)
     
register.simple_tag ( getFeesValue )
 
def checkIfExistsThenSelect ( element, mylist ):
    if element in mylist:
        return 'selected="selected"'
    return '' 
         
register.simple_tag ( checkIfExistsThenSelect )
 
@register.filter
def divide(value, arg):
    if arg and float(arg) != 0:
        return float(float(value) / float(arg))
    return None
divide.is_safe = True
 
@register.filter
def multiply(value, arg):
    if arg and value:
        return float(float(value) * float(arg))
    return 0
multiply.is_safe = True
 
@register.filter
def minus(value, arg):
    return float(value) - float(arg)
minus.is_safe = True
 
@register.filter
def addition(value, arg):
    return float(value) + float(arg)
addition.is_safe = True
 
@register.filter
def increment(value, arg=1):
    return value + arg
addition.is_safe = True
 
@register.filter
def decrement(value, arg=1):
    return value - arg
decrement.is_safe = True
 
@register.filter
def calcmodule(value, arg=1):
    return int(value) % arg
calcmodule.is_safe = True

@register.filter
def hmsformating(value, timeformat='%H:%M:%S'):
    hours = value / 3600
    value -= hours * 3600
    minutes = value / 60
    value -= minutes * 60
    seconds = value
    if timeformat == '%H:%M:%S':
        return str(hours) + ":" + str(minutes).zfill(2) + ":" + str(seconds).zfill(2)
    if timeformat == '%H:%M':
        return str(hours) + ":" + str(minutes).zfill(2)
hmsformating.is_safe = True

@register.filter
def minutesformating(value):
    minutes = value / 60
    value -= minutes * 60
    seconds = value
    return str(minutes).zfill(2) + ":" + str(seconds).zfill(2)

minutesformating.is_safe = True
 
@register.filter
def timeformating(value):
    hour = int(float(value))
    minute = int((float(value) - float(hour)) * 60.0 )
    minute_str = str(minute)
    if len(minute_str) == 1:
        minute_str = '0' + minute_str
    return str(hour) + ':' + minute_str
timeformating.is_safe = True
 
@register.filter
def div1000(value):
    return int(value / 1000)
div1000.is_safe = True
 
@register.filter
def strtimeformatting(time, timeformat="%H:%M"):
    return time.strftime(timeformat)
strtimeformatting.is_safe = True
 
@register.filter
def dateformatting(date, timeformat="%Y-%m-%d %H:%M"):
    if date is not None:
        return date.strftime(timeformat)
    return ''
dateformatting.is_safe = True
 
@register.filter
def getPeriod(date):
    return date.strftime('%Y%m')
getPeriod.is_safe = True
 
@register.filter
def getVerbosePeriod(period_str):
    year = int(period_str[0:4])
    month = int(period_str[4:6])
    return dateMonthName(datetime.date(year, month, 1)) + " de " + str(year) 
getVerbosePeriod.is_save = True
 
@register.filter
def dateMonthName(date):
    month = date.month
    if month == 1:
        return 'Enero'
    if month == 2:
        return 'Febrero'
    if month == 3:
        return 'Marzo'
    if month == 4:
        return 'Abril'
    if month == 5:
        return 'Mayo'
    if month == 6:
        return 'Junio'
    if month == 7:
        return 'Julio'
    if month == 8:
        return 'Agosto'
    if month == 9:
        return 'Septiembre'
    if month == 10:
        return 'Octubre'
    if month == 11:
        return 'Noviembre'
    if month == 12:
        return 'Diciembre'
dateformatting.is_safe = True
     
@register.filter
@stringfilter
def formating(value, arg=2):
    if value != 'None':
        if value and arg and len(str(value)) > 0 and len(str(arg)) > 0:
            value_list = str(value).split('.')
            string = value_list[0]
            if len(value_list) > 1:
                string += '.'
                string += value_list[1][:arg]
            return string
    return ''
formating.is_safe = True
 
@register.filter
def try_or_incomma(value):
    if value.replace(',','').replace('.','').isdigit():
        try:
            value = formatting(value,2)
            return intcomma(value)
        except:
            return value
    else:
        return value
 
try_or_incomma.is_safe = True
 
@register.filter
@stringfilter
def formatting(value, arg=2):
    if value != 'None':
        if value and arg and len(str(value)) > 0 and len(str(arg)) > 0:
            value_list = str(value).split('.')
            string = value_list[0]
            if len(value_list) > 1:
                string += '.'
                string += value_list[1][:arg]
            return string
    return ''
formatting.is_safe = True
 
@register.filter
def getDictData(data_dict, key):
    try:
        result = data_dict[key]
        if result is not None:
            return result
        else:
            return '' 
    except:
        return ''
 
getDictData.is_save = True

def getHtmFieldCodeQuery (field, gestion):
    return field.getHtmlCode(value_dict=gestion.other_data)

register.simple_tag ( getHtmFieldCodeQuery )