import datetime
from django.conf import settings

def generic_variables(request):
    return {    
                'CURRENT_YEAR': datetime.date.today().strftime("%Y"),
                'APP_NAME': settings.APP_NAME,
                'COMERCIAL_APP_NAME': settings.COMERCIAL_APP_NAME,
                'APP_URL': settings.APP_URL,
                'USER_NAME': request.user.username,
                'STATIC_URL': settings.STATIC_URL,
            }
