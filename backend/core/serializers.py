# -*- coding: utf-8 -*-
from rest_framework import serializers

class GenericQuerySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_alternate_name')
     
    def get_alternate_name(self, obj):
        return '%s' % str(obj)
    