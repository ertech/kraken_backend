# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from django.conf import settings
from rest_framework import viewsets
from backend.core.metadata import GenericTableMetadata,\
    GenericMetadata
from backend.auth.license.models import ResourceHost
from django.views.generic.base import RedirectView

favicon_view = RedirectView.as_view(url='/media/favicon.ico', permanent=True)

class HomeView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        data = {}
#         data['app_name'] = settings.COMERCIAL_APP_NAME
#         data['backend_version'] = settings.VERSION
#         data['developer'] = "ER Technology LTDA."
#         data['developer_url'] = "http://www.er-technology.net"
#         data['developer_email'] = "info@er-technology.net"
#         data['year'] = datetime.date.today().strftime('%Y')
#         if account is not None:
#             owner = account.getOwner()
#             if owner:
#                 data['account'] = account.username
#                 data['owner'] = owner.name.upper()
#                 if owner.email is not None:
#                     data['owner_email'] = owner.email
#                 else:
#                     data['owner_email'] = ''
#         data['ENVIRONMENT'] = ResourceHost.getEnvironment(account)

        return Response(data)

class SchemeView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        data = {}
        base_url = ResourceHost.getURL('BASE_URL')

        for key in settings.COMPONENT_LIST.keys():
            url = base_url + '/'
            url += settings.COMPONENT_LIST[key]["COMPONENT_PATH"]
            url += '/'
            data_url = {}
            data_url['NAME'] = settings.COMPONENT_LIST[key]["NAME"]
            data_url['URL'] = url
            data[key] = data_url

        return Response(data)

class GenericTablenoOwnerViewSet(viewsets.ReadOnlyModelViewSet):
    metadata_class = GenericTableMetadata

    def get_queryset(self):
        try:
            self.filter_fields = self.serializer_class.Meta.filter_fields
        except:
            pass
        try:
            self.search_fields = self.serializer_class.Meta.search_fields
        except:
            pass
        try:
            self.ordering_fields = self.serializer_class.Meta.ordering_fields
        except:
            pass
        return self.queryset

class GenericTableViewSet(viewsets.ReadOnlyModelViewSet):
    metadata_class = GenericTableMetadata

    def get_queryset(self):
        try:
            self.filter_fields = self.serializer_class.Meta.filter_fields
        except:
            pass
        try:
            self.search_fields = self.serializer_class.Meta.search_fields
        except:
            pass
        try:
            self.ordering_fields = self.serializer_class.Meta.ordering_fields
        except:
            pass
        
        return self.queryset

class GenericQuerynoOwnerViewSet(GenericTablenoOwnerViewSet):

    def paginate_queryset(self, queryset, view=None):
        return None

class GenericQueryViewSet(GenericTableViewSet):

    def paginate_queryset(self, queryset, view=None):
        return None

class GenericModelViewSet(viewsets.ModelViewSet):
    metadata_class = GenericMetadata

    def paginate_queryset(self, queryset, view=None):
        return None
 
    def get_queryset(self):
        try:
            self.filter_fields = self.serializer_class.Meta.filter_fields
        except:
            pass
        try:
            self.search_fields = self.serializer_class.Meta.search_fields
        except:
            pass
        try:
            self.ordering_fields = self.serializer_class.Meta.ordering_fields
        except:
            pass
        
        return self.queryset

    def perform_destroy(self, instance):
        instance.delete()
    
class GenericModelnoOwnerViewSet(viewsets.ModelViewSet):
    metadata_class = GenericMetadata

    def paginate_queryset(self, queryset, view=None):
        return None
 
    def get_queryset(self):
        queryset = self.queryset
        return queryset

    def perform_destroy(self, instance):
        old_serializer_data = self.serializer_class(instance).data
        instance.delete()
    
    
