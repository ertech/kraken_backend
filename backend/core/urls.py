from django.conf.urls import url, include
from backend.core.base_urls import urlpatterns 

urlpatterns += [
    url(r'^disanconn/', include('backend.apps.disanconn.urls')),
    url(r'^operative/', include('backend.apps.operative.urls')),
] 

